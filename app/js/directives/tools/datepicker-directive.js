/******************************************************************************************************************
 Datepicker Directive
 - 
******************************************************************************************************************/

reddonor.directive('datepicker', function() {
	return {
		restrict: 'A',
		require : 'ngModel',
		link : function (scope, element, attrs, ngModelCtrl) {
			$(function(){
				element.datepicker({
					dateFormat:'mm/dd/yy',
					changeMonth: true,
					changeYear: true,
					onSelect:function (date) {
						ngModelCtrl.$setViewValue(date);
						scope.$apply();
					}
				});
			});
		}
	};
});