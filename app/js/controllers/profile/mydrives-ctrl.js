/******************************************************************************************************************
 My Drives Controller
 - Get user profile info and created drives
******************************************************************************************************************/

reddonor.controller('MyDrivesCtrl', ['$scope', '$http',
	function($scope, $http) {
		$http.get('/api/profile-info/').
    success(function(data) {
      $scope.profiles = data;
    });
  $http.get('/api/created-drives/').
      success(function(data) {
        $scope.drives = data;
      });
	}]);