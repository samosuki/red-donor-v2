###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###******************************************************************************************************************
LOCATION LIST FOR USER [LOGGED IN MENU]
Usage: Displays list of Donation Centres with the user's country
******************************************************************************************************************###
exports.userlocations = (req, res) ->
  connection = database.getConnection() 
  # returns the location (donation centre) name and id from the user's country
  userLocation = req.session.user.user_country
  q1 = "SELECT l.location_name, l.location_title, l.location_id FROM location l WHERE l.location_country = ?;"
  connection.connect()
  connection.query q1, [userLocation], (err, results) ->
    if err
      console.log err
    else
      res.json results
    connection.end()

###******************************************************************************************************************
GET LOCATION LIST [DRIVE DETAILS PAGE]
Usage: Displays list of Donation Centres and their locations for the selected Blood Drive
******************************************************************************************************************###
exports.locations = (req, res) ->
  connection = database.getConnection() 
  # returns the country id from a selected drive
  driveID = req.params.did
  q1 = "SELECT d.drive_location FROM location l, drive d WHERE d.drive_location=l.location_id AND d.drive_id = ?;"
  connection.connect()
  connection.query q1, [driveID], (err, results) ->
    if err
      console.log err
      connection.end()
    else
      if results.length > 0
        location = results[0].drive_location
        # uses the country id to return the locations data for the selected drive
        q2 = "SELECT * FROM location l, country c WHERE l.location_id = ? AND l.location_country=c.country_id;"
        connection.query q2, [location], (err, results) ->
          if err
            console.log err
          else
            res.json results
          connection.end()
      else
        q3 = "SELECT c.`country_id` FROM drive d, country c WHERE d.`drive_id` = ? AND d.drive_country=c.country_id;"
        connection.query q3, [driveID], (err, results) ->
          if err
            console.log err
            connection.end()
          else
            country = results[0].country_id
            # uses the country id to return the locations data for the selected drive
            q4 = "SELECT * FROM location l, country c WHERE l.location_country=c.country_id AND c.`country_id` = ?;"
            connection.query q4, [country], (err, results) ->
              if err
                console.log err
              else
                res.json results
              connection.end()

