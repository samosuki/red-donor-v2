###
Module dependencies.
###

#require('newrelic')
require("coffee-script")

express = require("express")
http = require("http")
path = require('path')
_ = require("underscore") # Underscore
cmysql = require('mysql').createPool( user: 'rev3', password: 'mOg33S4m_RI', database: 'samosuki_donor', host: 'localhost' )
MySQLStore = require('connect-mysql')(express)
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')

routes = require("./routes")
APIappt = require("./routes/api/api-appointments") # Appointments API
APIauth = require("./routes/api/api-auth") # Authentication API
APIblood = require("./routes/api/api-blood") # Blood API
APIcentres = require("./routes/api/api-centres") # Donation Centres API
APIcharts = require("./routes/api/api-charts") # Charts API
APIcountry = require("./routes/api/api-country") # Country API
APIdrives = require("./routes/api/api-drives") # Drives API
APIlocs = require("./routes/api/api-locations") # Locations API
APIorgs = require("./routes/api/api-organisations") # Organisations API
APIprofile = require("./routes/api/api-profiles") # Profiles API
APIpages = require("./routes/api/api-pages") # Pages API

AUTHform = require("./routes/auth/auth-forms") # Authentication Forms
AUTHdrive = require("./routes/auth/auth-drives") # Authentication Drives

app = module.exports = express()

###
Configuration
###

# all environments
app.set "port", process.env.PORT || 25340
app.set "views", __dirname + "/views"
app.set "view engine", "jade"
#app.use express.logger('dev')
app.use express.cookieParser()
app.use express.session(
	secret: "linksawakeningdx"
	store: new MySQLStore(client: cmysql)
	cookie:
		maxAge: 1000 * 60 * 60 * 24
	)

app.use (req, res, next) ->
  if req.session
    res.locals.session = req.session.user
    if req.session.user
      res.locals.userid = req.session.user.users_id
      res.locals.user_name = req.session.user.user_name
      res.locals.bloodtype = req.session.user.user_bloodtype
    next()

app.use express.bodyParser()
app.use express.methodOverride()
app.use express.static(path.join(__dirname, '/app'))
app.use app.router

app.locals.pretty = true # outputs jade to html readable

# development only
app.use express.errorHandler() if app.get("env") is "development"

# production only
app.get("env") is "production"


###
ROUTES
###

# Get Page Layout Template
app.get "/", routes.index
# Get Pages Not Requiring Login
app.get "/partials/:name", routes.partials
# Get Pages Requiring Login
app.get "/sessions/:name", AUTHform.requireLogin, routes.sessions
# Get Session Login Page
app.get "/partials/login", routes.login

# Session Logout
# Uses MySQL-Connect to find session and delete
app.get "/session/logout", (req, res) ->

  # didn't work unless I put delete
  req.session.destroy((err) ->
    console.log "Logout User..."
    #app.locals.user_name = ""
    res.redirect "/partials/login"
  )

###***********************************************************
API - Pages
***********************************************************###

# Get Welcome Page Info
app.get "/api/drives-welcome/", APIpages.welcome

# Get Most Recent Drives  Info
app.get "/api/drives-mostrecent/", APIpages.recent
app.get "/api/drives-mostrecent/:page", APIpages.recent

# Get Ending Soon Drives Pages Info
app.get "/api/drives-endingsoon/", APIpages.ending
app.get "/api/drives-endingsoon/:page", APIpages.ending

###***********************************************************
API - Appointments
***********************************************************###

# Check for User Scheduled Already
app.get "/api/schedule-check", APIappt.schedulescheck

# Get Appointments Schedules
app.post "/api/appointments/:loc", APIappt.getapptschedules

# Add Appointment Schedule
app.post "/api/add-appointment/:loc", APIappt.addscheduleappt

###***********************************************************
API - Authentication
***********************************************************###

# User Login and Authentication | calls authenticateLogin function and if user returns add data to new session
app.post "/api/login/", APIauth.userlogin

#  User Sign Up & Authenticaion | calls existingEmail function to check if email address exists already then authenticateSignup function and
#  if user returns add data to new session
app.post "/api/signup/", APIauth.usersignup

#  Guest Donor Sign Up & Authenticaion | calls authenticateGuestSignup function
app.post "/api/guest-signup/", APIauth.guestusersignup

#  Donor Sign Up & Authenticaion | calls existingEmail function to check if email address exists already then authenticateSignup function
app.post "/api/donor-signup/", APIauth.donorsignup

#  Create New Blood Drive | calls checkDate function to check if the deadline date has already passed, if not,
#  call autheticateDrive to create a new blood drive
app.post "/api/create-drive/", APIauth.createblooddrive

#  Create New Blood Drive Without Login | calls guestSignup function to check user id
# calls checkDate function to check if the deadline date has already passed, if not,
#  call autheticateDrive to create a new blood drive
app.post "/api/create-new-drive/", APIauth.createnewdrive

# Get Drive Detail
app.get "/api/drivecheck/:did", APIauth.drivecheck

# Pledge Donation to Drive | if no errors are found, pledge donation to drive
app.post "/api/pledge-donation/:did/:loc", APIauth.pledgetodrive

# Unpledge Donation | calls functions to unpledge donation and substract from pledge count
app.post "/api/unpledge-donation/:did", APIauth.unpledeblooddonation

# Authenticate User Token | Checks if right token exists and if token was created within past 2 hrs
app.get "/api/reset-password/:token", APIauth.authenticatetoken

# Checks User Logged In | Checks to see if user exists and if a session is active
app.get "/api/check-logged/", APIauth.checkLoggedMenu

###***********************************************************
API - Blood
***********************************************************###

# Get Bloodtype List |
app.get "/api/bloodtype", APIblood.bloodtype

# Get Compatible BloodTypes | Displays All the Compatible Blood Types for User
app.get "/api/blood-compat/", APIblood.headingcompatlist

###***********************************************************
API - Centres
***********************************************************###

# Get All Donation Centres | Displays ALL Donation Centres
app.get "/api/centres", APIcentres.donationcentres
app.get "/api/centres/:loc", APIcentres.donationcentres

# Get Blood Bank Info
app.get "/api/bloodbank-profile/:bid", APIcentres.bloodbankinfo

# Get Blood Levels for Blood Bank
app.get "/api/bloodbank-levels/:bid", APIcentres.bloodbanklevels

###***********************************************************
API - Charts
***********************************************************###

# Get Chart Data for Total Pledges By Gender For Location
app.get "/api/loc-gender-chart/:bid", APIcharts.chartgenderlocation

# Get Chart Data for Total Pledges By Blood Type For Location
app.get "/api/loc-bloodtype-chart/:bid", APIcharts.chartbloodtypelocation

# Get Chart Data for Total Pledges By Causes For Location
app.get "/api/loc-causes-chart/:bid", APIcharts.chartcauseslocation

# Get Chart Data for Blood Levels For Location
app.get "/api/loc-levels-chart/:bid", APIcharts.chartlevelslocation

# Get Chart Data for Pleges by Gender For Organisation
app.get "/api/org-gender-chart/:oid", APIcharts.chartgenderorg

# Get Chart Data for Drives Created by Cause for Organisation
app.get "/api/org-drives-cause-chart/:oid", APIcharts.orgdrivescauses

# Get Chart Data for Pledges by Cause for Organisation
app.get "/api/org-pledges-cause-chart/:oid", APIcharts.orgpledgescauses

# Get Chart Data for Drives Created by Blood Type for Organisation
app.get "/api/org-drives-bloodtype-chart/:oid", APIcharts.orgdrivesbloodtype

# Get Chart Data for Pledges by Blood Type for Organisation
app.get "/api/org-pledges-bloodtype-chart/:oid", APIcharts.orgpledgesbloodtype

###***********************************************************
API - Country
***********************************************************###

# Get Country List | Populates dropdown list for with all countries user to select a Country
app.get "/api/countries", APIcountry.countries

# Get Country | Populates dropdown list for user to select the country drive takes place in
app.get "/api/country", APIcountry.country

###***********************************************************
API - Drives
***********************************************************###

# Get Ending Soon of Blood Drives
app.get "/api/drives-end", APIdrives.drivesending
app.get "/api/drives-end/:page", APIdrives.drivesending

# Get Uses List | Populates dropdown list for user to select the reason (use) behind the blood drive.
app.get "/api/uses", APIdrives.uses

# Get All Active Blood Drives | Displays All the active blood drives
app.get "/api/drives-active", APIdrives.drivesactive
app.get "/api/drives-active/:page", APIdrives.drivesactive

# Get All Blood Drives Ended | Displays All the ended blood drives
app.get "/api/drives-nonactive", APIdrives.drivesnonactive
app.get "/api/drives-nonactive/:page", APIdrives.drivesnonactive

# Get Compatible Blood Drives
app.get "/api/drives-compat", APIdrives.drivescompat
app.get "/api/drives-compat/:page", APIdrives.drivescompat

# Get Compatible BloodTypes / Get Date Difference | Displays Compatible Blood Types for Selected Blood Drive
app.get "/api/blood-info/:did", APIdrives.drivedetailinfo

# Get Drive Detail
app.get "/api/drivelist/:did", APIdrives.drivedetail

# Get Blood Drives by Bloodtype
app.get "/api/drives/:cat", APIdrives.drivesbybloodtype

# Get Drive By Bloodtype Count & Breadcrumb | Gets the Count of Total Blood Drives by Bloodtyoe & Bloodtype Name
app.get "/api/drives-count-crumb/:cat", APIdrives.drivesbybloodtypecountcrumb

# Get Most Recent of Blood Drives
app.get "/api/drives-recent/", APIdrives.drivesrecent
app.get "/api/drives-recent/:page", APIdrives.drivesrecent

# Get Blood Drives by Location
app.get "/api/drives-location/:loc", APIdrives.drivesbylocation

# Get Drive By Location Count & Breadcrumb | Gets the Count of Total Blood Drives by Location & Country Name
app.get "/api/drives-location-count-crumb/:loc", APIdrives.drivesbylocationcountcrumb

# Get Drive Info For Edit
app.get "/api/edit-drive/:did", APIdrives.driveedit

# Update Drive Info for Edit
app.put "/api/save-drive/:did", APIdrives.savedrive

###***********************************************************
API - Locations
***********************************************************###

# Get Location List | Displays Donation Centres using the User's Country
app.get "/api/centre-locations", APIlocs.userlocations

# Get Location List | Displays list of Donation Centres and their locations for the selected blood drive
app.get "/api/locations/:did", APIlocs.locations

###***********************************************************
API - Organisations
***********************************************************###

# Get Organisation Info
app.get "/api/org-profile-info/:oid", APIorgs.orgprofileinfo

###***********************************************************
API - Profiles
***********************************************************###

# Get Username
app.get "/api/username/", APIprofile.username

# Get Profile Info
app.get "/api/profile-info/", APIprofile.profileinfo

# Get Donation Pledges Info
app.get "/api/donation-pledges/", APIprofile.donationpledges

# Get Appointments Info
app.get "/api/appointment-schedules/", APIprofile.getapptinfo

# Get Created Drives
app.get "/api/created-drives/", APIprofile.createddrives

# Get User Account Setting Info
app.get "/api/user-settings/", APIprofile.usersettings

# Update User Account Setting Info
app.put "/api/save-account/", APIprofile.saveaccount

# Send Forgot Password Email | Checks if email address exists, if so sent password reset email
app.post "/api/forgot-password/", APIprofile.forgotpassword

# Reset Password | Updates user password where it matches user token and removes the user token data and time
app.post "/api/reset-password/:token", APIprofile.resetpassword

# Get Appointments Info
app.get "/api/appointment-schedules/", APIprofile.getapptinfo

# Check & Update User Password Setting // Update Check Current Password For Settings
app.put "/api/save-password/", APIprofile.updatepassword


app.get "*", routes.index

###***************************************************************************************************************
Start server
***************************************************************************************************************###
http.createServer(app).listen app.get("port"), ->
	console.log "Express server listening on port " + app.get("port") + " - Launch Red Donor Webapp"
