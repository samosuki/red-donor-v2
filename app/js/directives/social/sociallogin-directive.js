/******************************************************************************************************************
 Social Login Directive
 - Authenticate Login with Facebook & Twitter
******************************************************************************************************************/

reddonor.directive('sociallogin', ['$location', function ($location) {
    var socialloginFn;

    socialloginFn = function(scope, element, attrs) {
      
      scope.onTwitterLogin = function() {
        // to overcome Angular intercepting function call
        window.location = "../auth/twitter";
      };

      scope.onFacebookLogin = function() {
        // to overcome Angular intercepting function call
        window.location = "../auth/facebook";
    };
  };
  return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/social-login.jade',
      link: socialloginFn

    };
}]);