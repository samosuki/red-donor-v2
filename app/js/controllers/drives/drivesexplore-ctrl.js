/******************************************************************************************************************
 Explore Drives Controller (Paged)
 - Display blood drives, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('ListDrivePagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window',
	function($scope, $http, $routeParams, $location, $window) {

		$scope.page.setTitle("Explore Drives");
		$scope.content = [];
		$scope.empty = true;
		$scope.emptydrive = false;
		$scope.currentPage = 0;
		$scope.pageSize = 8;

		// Get Previous Explore Drive Page
		$scope.prevPage = function () {
			if ($scope.currentPage > 0) {
				$location.url('../drives/list?page=' + $scope.currentPage);
				//$window.location = '../drives/list?page=' + $scope.currentPage;
			}
		};
		// Get Next Explore Drive Page
		$scope.nextPage = function () {
			if ($scope.currentPage <  $scope.numPages-1) {
				$scope.nextPage = $scope.currentPage + 2;
				$location.url('../drives/list?page=' + $scope.nextPage);
				//$window.location = '../drives/list?page=' + $scope.nextPage;
			}
		};
			// Get blood drive information
	$scope.getDrives = function() {
		console.log("--routeParams--");
		console.log($routeParams);
		$http.get('/api/drives-active/' + $routeParams.page).
				success(function(data) {
					$scope.drivesactive = data;
					$scope.empty = false;
					$scope.limit = 8;

					$scope.totalItems = data[0].Result_Count;
					$scope.pageSize = 8;
					$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
					for(var i = 0; i<$scope.numPages; i++) {
						$scope.content.push(i);
					}
					
					if(($routeParams.page !== null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}
					if($scope.currentPage >= 3) {
						$scope.showCurrent = false;
						$scope.hideCurrent = false;
					} else {
						$scope.showCurrent = true;
						$scope.hideCurrent = true;
					}

					if($scope.drives === "") {
						$scope.text = "No New Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			// Get Soon Ending Drives
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getDrives();
		}
	}]);