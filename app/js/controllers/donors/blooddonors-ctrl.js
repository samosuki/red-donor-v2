/******************************************************************************************************************
 Blood Donor Controller
 - Sign Up form receiving information for emergency blood donors
******************************************************************************************************************/

reddonor.controller('DonorSignUpCtrl', ['$scope', '$http', '$location', '$routeParams',
	function($scope, $http, $location, $routeParams) {
					$scope.donorsuccess = false;

			//$scope.showSuccess = function() {
			//	$scope.donorsuccess = true;
			//}

			$scope.onSubmitSignup = function() {
				$http.post('/api/donor-signup/', $scope.user).
					success(function(data) {
						//console.log(data);
						//console.log(scope.user);

						if(data.error) {
							$scope.error = "Email Already Exists";
							//window.location = "/login";
						}
						if(data.success) {
							$scope.error = "";
							//$scope.showSuccess();
							//console.log("showSuccess called?");
							$scope.donorsuccess = true;
							//console.log("$scope.donorsuccess = true");
							//window.location = "/";
							//console.log("Button clicked!");
						}
				});
			};
			
			$http.get('/api/countries').
				success(function(data) {
					$scope.country = data;
					//console.log(data);
					return;
				});

			$scope.bloodtypes = [
				{ id: 1, name:"O+" },
				{ id: 2, name:"O-" },
				{ id: 3, name:"A+" },
				{ id: 4, name:"A-" },
				{ id: 5, name:"B+" },
				{ id: 6, name:"B-" },
				{ id: 7, name:"AB+"},
				{ id: 8, name:"AB-"},
				{ id: 9, name:"Unknown"}
			];

			$scope.gender = [
				{ id: 1, name:"Male" },
				{ id: 2, name:"Female" }
			];

			$scope.telephone = /^\d\d\d\d\d\d\d$/;
	}]);