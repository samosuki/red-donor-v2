/******************************************************************************************************************
 Donation History Controller
 - Get user profile info and donation pledges
******************************************************************************************************************/
reddonor.controller('HistoryCtrl', ['$scope', '$http',
	function($scope, $http) {
		$http.get('/api/profile-info/').
    success(function(data) {
      $scope.profiles = data;
    });
  $http.get('/api/donation-pledges/').
      success(function(data) {
        $scope.pledges = data;
      });
}]);