mysql = require("mysql")

#var CONFIG = require(__dirname + "/configuration");
module.exports.getConnection = ->

  # Test connection health before returning it to caller.
  return module.exports.connection  if (module.exports.connection) and (module.exports.connection._socket) and (module.exports.connection._socket.readable) and (module.exports.connection._socket.writable)
  console.log ((if (module.exports.connection) then "UNHEALTHY SQL CONNECTION; RE" else "")) + "CONNECTING TO SQL..."
  connection = mysql.createConnection(
    host: "localhost"
    user: "rev3"
    password: "mOg33S4m_RI"
    database: "samosuki_donor"
  )

#debug  : 'true'
#port     : CONFIG.db.port
