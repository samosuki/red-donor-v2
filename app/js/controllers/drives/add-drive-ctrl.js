/******************************************************************************************************************
 Add Drive Controller
 - Get country list, uses list, blood types list and errors
******************************************************************************************************************/
/* ---------- Add Drive Controller ---------- */
reddonor.controller('AddDriveCtrl', ['$scope', '$http', '$location',
	function($scope, $http, $location) {
				// Get Country List
		$http.get('/api/country').
				success(function(data) {
					$scope.country = data;
					//console.log(data);
					return;
		});

		// Get Uses List
		$http.get('/api/uses').
				success(function(data) {
					$scope.uses = data;
					//console.log(data);
					return;
		});

		$scope.showForgot = function () {
			 if($scope.forgotpwd === true) {
					$scope.forgotpwd = false;
				} else {
					$scope.forgotpwd = true;
				}
		};

		// Get Locations Info (ALL)
		$http.get('/api/centres/').
			success(function(data) {
				$scope.location = data;
				$scope.filteredLocations = data;
				return;
		});

		$scope.countryChange = function() {
			$scope.filteredLocations = [];
			angular.forEach($scope.location, function(val, key){
				if(val.location_country == $scope.recipient.driveCountry){
					this.push(val);
				}
				else if($scope.recipient.driveCountry === undefined) {
					this.push(val);
				}
			},$scope.filteredLocations);
		};


		// Blood Types for List
		$scope.bloodTypes = [
				{ id: 1, name:"O+" },
				{ id: 2, name:"O-" },
				{ id: 3, name:"A+" },
				{ id: 4, name:"A-" },
				{ id: 5, name:"B+" },
				{ id: 6, name:"B-" },
				{ id: 7, name:"AB+"},
				{ id: 8, name:"AB-"},
				{ id: 9, name:"Unknown"}
			];

			$scope.poperr = function() {
				toaster.pop('error', "", $scope.error, 5000, true);
			};

			$scope.popsuccess = function() {
				toaster.pop('success', "", $scope.success, 5000, true);
			};

			$scope.onSubmitAddDrive = function() {
				$http.post('/api/create-drive/', $scope.recipient).
					success(function(data) {
						if(data.aerror) {
							$scope.aerror = true;
							$scope.error = "Sorry, that Deadline Date has passed";
							$scope.poperr();
						}
						if(data.berror) {
							$scope.error = "An error occurred. Your Drive was not created :(";
							$scope.poperr();
						}
						if(data.success) {
							$scope.error = "";
							window.location = "/";
						}
					}); 
				};
	}]);