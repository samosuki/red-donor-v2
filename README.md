# Red Donor v2

## About
The web application which accompanied [Red Donor](http://reddonor.com/).

The project was built in 2013 with: 

* AngularJS (Frontend)
* NodeJS (Backend)
* MySQL 
* CoffeeScript
* GruntJS
* HTML + CSS
* Bootstrap 