 /******************************************************************************************************************
	Check Logged In Controller 
 - 
******************************************************************************************************************/

 reddonor.controller('CheckingMenuCtrl', ['$scope', '$http', function($scope, $http) {
	// Get Session Info

	$scope.init = function () {
		$http.get('/api/check-logged').
		success(function(data) {
			console.log(data);
			if(data.loggedin) {
				$scope.loggedin = true;
			}
			if(data.notloggedin) {
				$scope.loggedin = false;
			}  
		});
	}		
	$scope.init();
		
 }]);