
###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore")
moment = require("moment")
bcrypt = require("bcrypt") # Encryption 
nodemailer = require("nodemailer") # Email
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
apiprofile = require("./api-profiles") # Profiles API
authform = require("../auth/auth-forms") # Authentication Forms
authdrive = require("../auth/auth-drives") # Authentication Drives
#connection = database.getConnection()
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')

###***********************************************************
Get Username
***********************************************************###
exports.username = (req, res) ->
  connection = database.getConnection()
  userid = req.session.user.users_id
  q = "SELECT u.`user_name` FROM users u, bloodtype b, country c WHERE u.user_bloodtype=b.bloodtype_id AND u.user_country=c.country_id AND u.`users_id` = ?;"
  params = [userid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Username Loaded ---"
      res.send results
    connection.end()

###***********************************************************
Get Profile Info
***********************************************************###
exports.profileinfo = (req, res) ->
  connection = database.getConnection()
  userid = req.session.user.users_id
  q = "SELECT u.`user_name`, b.`bloodtype_name`, c.`country_name`, b.`blood_compatibility`, u.`date_joined` FROM users u, bloodtype b, country c WHERE u.user_bloodtype=b.bloodtype_id AND u.user_country=c.country_id AND u.`users_id` = ?;"
  params = [userid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Profile Info Loaded ---"
      res.send results
  connection.end()

###***********************************************************
Get Donation Pledges
***********************************************************###
exports.donationpledges = (req, res) ->
  connection = database.getConnection()
  userid = req.session.user.users_id
  q = "SELECT p.`date_pledged`, d.`drive_name`, d.`drive_title`, b.`bloodtype_name`, d.`pledge_count`, d.`drive_id`, d.`drive_enddate` FROM drive d LEFT OUTER JOIN pledge_users p ON p.drive_id=d.drive_id, bloodtype b WHERE d.drive_bloodtype=b.bloodtype_id AND p.users_id = ?;"
  params = [userid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Donation Pledges Loaded ---"
      res.send results
    connection.end()

###***********************************************************
Get Created Drives
***********************************************************###
exports.createddrives = (req, res) ->
  connection = database.getConnection()
  userid = req.session.user.users_id
  q = "SELECT d.`drive_name`, b.`bloodtype_name`, d.`drive_title`, d.`pledge_count`,  d.`date_created`, d.`drive_enddate`, d.`drive_id` FROM drive d, bloodtype b, users u WHERE d.drive_bloodtype=b.bloodtype_id AND d.created_by_user=u.users_id AND u.`users_id` = ? ORDER BY d.`date_created` DESC;"
  params = [userid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Created Drives Loaded ---"
      res.send results
    connection.end()

###***********************************************************
Retrieve User Information For Settings
***********************************************************###
exports.usersettings = (req, res) ->
  connection = database.getConnection()
  userID = req.session.user.users_id
  q = "SELECT u.`users_id`, u.`user_name`, u.`user_email`, u.`user_country` FROM users u WHERE u.`users_id` = ?;"
  connection.connect()
  connection.query q, [userID], (err, results) ->
    if err
      console.log err
    else
      console.log "--- User Setting Loaded ---"
      res.json _.first(results)
    connection.end()

###***********************************************************
Update User Info For Settings
***********************************************************###
exports.saveaccount = (req, res) ->
  connection = database.getConnection()
  userID = req.session.user.users_id
  name = req.body.user_name
  email = req.body.user_email
  country = req.body.user_country
  q = "UPDATE users SET user_name = ?, user_email = ?, user_country = ? WHERE users_id = ?;"
  params = [
    name
    email
    country
    userID
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      req.session.user.user_name = req.body.user_name
      res.send req.body
    connection.end()

###***********************************************************
Update New Password For Settings
***********************************************************###

exports.savenewpassword = (newPassword, userID, callback) ->
  connection = database.getConnection()
  salt = bcrypt.genSaltSync(12, 32)
  pass = bcrypt.hashSync(newPassword, salt)
  q = "UPDATE users SET user_password = ? WHERE users_id = ?"
  params = [
    pass
    userID
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback null
      return
    else
      console.log "Changing user password..."
      success = true      
      callback success
  connection.end()


###***********************************************************
FORGOT PASSWORD
Usage:  Checks if email address exists, if so sent password reset email
***********************************************************###
exports.forgotpassword = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  authform.emailExists req.body.forgotEmail, (exists) ->
    if exists
      #token = undefined
      authform.createtoken req.body.forgotEmail, (token) ->
        if token
          userName = exists.user_name
          message =
            html: "<html>" + "<title>Red Donor - Forgot Password</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Forgot your password, " + userName + "?</h3>" + "<p style='font-size: 14px;'>Red Donor received a request to reset the password for your Red Donor account.</p>" + "<p style='font-size: 14px;'>To reset your password, click on the link below (or copy and paste the URL into your browser):<br>" + "<p style='ont-size: 12px; font-weight:bold'><a style='color: #d94d48; text-decoration: underline;' href='http://my.reddonor.com/reset-password?token=" + token + "'>http://reddonor.com/reset-password?token=" + token + "</a></p>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Support</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
            text: "Forgot your password, Samora Reid? Red Donor received a request to reset the password for your Red Donor account. To reset your password,  click on the link below (or copy and paste the URL into your browser): http://reddonor.com/reset-password?token=" + token + " - Red Donor Support"
            subject: "Reset your Red Donor password"
            from_email: "support@reddonor.com"
            from_name: "Red Donor"
            to: [
              email: req.body.forgotEmail
              name: userName
              type: "to"
            ]
            headers:
              "Reply-To": "support@reddonor.com"

            important: false
            track_opens: null
            track_clicks: null
            auto_text: null
            auto_html: null
            inline_css: true
            url_strip_qs: null
            preserve_recipients: null
            view_content_link: null
            tracking_domain: null
            signing_domain: null
            return_path_domain: null
            merge: true

          async = false
          mandrill_client.messages.send
            message: message
          , ((result) ->
            console.log "Message sent"
            #console.log result
            #res.redirect "/login"
          ), (e) ->
            console.log "A mandrill error occurred: " + e.name + " - " + e.message

    else
      res.send noexist: "Email does not exist..."
      console.log "Email does not exist"
  connection.end()

###***********************************************************
RESET PASSWORD
Usage:  i. updates user password where it matches user token 
        ii. removes the user token data and time
***********************************************************###
exports.resetpassword = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  newPassword = req.body.resetPassword
  token = req.params.token
  salt = bcrypt.genSaltSync(12, 32)
  pass = bcrypt.hashSync(newPassword, salt)
  async.series [
    (callback) ->
      
      # i. Update user password where recover token matches
      q = "UPDATE users SET user_password = ? WHERE recover_token = ?"
      params = [
        pass
        token
      ]
      connection.query q, params, (err, results) ->
        if err
          console.log err
          callback err
        else
          console.log "Password Reset"
          res.send successful: "Password Reset"
          callback()
   
    # ii. Delete recover token and recover dateime data for user
    (callback) ->
      q2 = "UPDATE users SET recover_token = '', recover_datetime = '' WHERE recover_token = ?"
      params2 = [token]
      connection.query q2, params2, (err, results) ->
        if err
          console.log err
          callback err
        else
          console.log "Token Deleted"
          callback()

  ], (err) ->
    next err  if err
  connection.end()

###***********************************************************
RESET PASSWORD
Usage:  updates user password where it matches user token and removes the user token data and time
***********************************************************###
exports.updatepassword = (req, res) ->
  connection = database.getConnection()
  userID = req.session.user.users_id
  currentPassword = req.body.settingsCurrentPassword
  newPassword = req.body.settingsNewPassword
  salt = bcrypt.genSaltSync(12, 32)
  q = "SELECT u.`user_password` FROM users u WHERE u.`users_id` = ?"
  params = [userID]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
      #connection.end()
    else
      if results.length > 0
        if bcrypt.compareSync(currentPassword, results[0].user_password)
          console.log "Current Password matches"
          apiprofile.savenewpassword newPassword, userID, (success) ->
            if success
              console.log "Password Changed."
              res.send successful: "Password Changed"
            else
              console.log "Password not changed."
            #connection.end()

        else
          res.send incorrect: "Current Password is incorrect..."
          console.log "Incorrect Password"
        #connection.end()
  connection.end()


###***********************************************************
GET USER APPOINTMENT INFORMATION [PROFILE PAGES]
Usage: i. Get user appointment info
***********************************************************###
exports.getapptinfo = (req, res) ->
  connection = database.getConnection()
  apptUser = req.session.user.users_id
  q = "SELECT a.appt_date, t.times_name, l.location_name, d.donor_type_name, s.status_name, u.user_name, a.appt_id FROM appointments a, times t, location l, statuses s, donor_type d, users u WHERE a.appt_time=t.times_id AND a.appt_location=l.location_id AND a.appt_status=s.status_id AND a.appt_type=d.donor_type_id AND a.appt_user=u.users_id AND a.appt_user = ? ORDER BY a.appt_date DESC;"
  params = [apptUser]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      #console.log results
      res.send results
    connection.end()