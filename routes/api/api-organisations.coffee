###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###***********************************************************
Get Organisation Info
Usage: Displays All the Organisation Info
***********************************************************###
exports.orgprofileinfo = (req, res) ->
  connection = database.getConnection()
  # returns organisation info from organisation id
  oid = req.params.oid
  q = "SELECT (SELECT COUNT(org_id) FROM pledge_users p, org o WHERE p.pledge_org=o.org_id AND o.org_id = ?) AS 'org_pledges', (SELECT COUNT(drive_id) FROM drive d, org o WHERE d.created_by_org=o.org_id AND o.org_id = ?) AS 'org_drives', o.org_name, o.org_description, o.org_address, c.country_name, o.org_tel, o.org_email, o.org_site, o.org_map, o.org_banner, c.ITU_calling AS 'Area_Code' FROM org o, country c, country c1 WHERE o.org_country=c.country_id AND o.org_id = ? AND o.org_country=c1.country_id LIMIT 1;"
  params = [
    oid
    oid
    oid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Org Profile Info Loaded ---"
      res.send results
    connection.end()