/******************************************************************************************************************
 Most Recent Drives Controller (Paged)
 - Display most recent drives, pagination and no blood drives (empty) set
******************************************************************************************************************/

reddonor.controller('MostRecentPagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window',
	function($scope, $http, $routeParams, $location, $window) {
		$scope.drives = [];
		$scope.content = [];
		$scope.emptydrive = false;
		$scope.empty = true;
		$scope.currentPage = 0;
		$scope.pageSize = 8;

	// Get Most Recent Drives Previous Page
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$location.url('../drives/most-recent?page=' + $scope.currentPage);
			//$window.location = '../drives/most-recent?page=' + $scope.currentPage;
		}
	};
	// Get Most Recent Drives Next Page
	$scope.nextPage = function () {
		if ($scope.currentPage <  $scope.numPages-1) {
			$scope.nextPage = $scope.currentPage + 2;
			$location.url('../drives/most-recent?page=' + $scope.nextPage);
			//$window.location = '../drives/most-recent?page=' + $scope.nextPage;
		}
	};
	$scope.getDrives = function() {
		$http.get('/api/drives-mostrecent/' + $routeParams.page).
				success(function(data) {
					$scope.drives = data[0];
					$scope.empty = false;
					$scope.limit = 8;
					$scope.totalItems;
					console.log($scope.drives);
					console.log($scope.drives.length);

					if(data[0][0]) 
						$scope.totalItems = data[0][0].Result_Count;
					$scope.pageSize = 8;
					$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
					for(var i = 0; i<$scope.numPages; i++) {
						$scope.content.push(i);
					}
					
					if(($routeParams.page != null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}
					
					if($scope.currentPage >= 3) {
						$scope.showCurrent = false;
						$scope.hideCurrent = false;
					} else {
						$scope.showCurrent = true;
						$scope.hideCurrent = true;
					}

					if($scope.drives == "") {
						$scope.text = "No New Recent Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getDrives();
	}
}]);