/******************************************************************************************************************
 Reset Password Controller 
 - Get reset token, send success email, check errors and change password
******************************************************************************************************************/
reddonor.controller('ResetCtrl', ['$scope', '$http', '$location', '$routeParams', 'toaster',
	function($scope, $http, $location, $routeParams, toaster) {
		// set reset password form to hidden
		$scope.validUser = false;
		// set forgot password form to hidden
		$scope.forgotpwd = false;

		$scope.poperr = function() {
			toaster.pop('error', "", $scope.error, 5000, true);
		};

		$scope.popsuccess = function() {
			toaster.pop('success', "", $scope.success, 5000, true);
		};

		$http.get('/api/reset-password/' + $routeParams.token).
				success(function(data, status) {
					if(data.success) {
						$scope.success = "This token has been authenticated. Please continue.";
						$scope.validUser = true;
						$scope.popsuccess();
					}
					if(data.expired) {
						$scope.error = "Sorry, this request token has expired. Please request a password reset again.";
						$scope.forgotpwd = true;
						$scope.poperr();
					}
					if(data.error) {
						$scope.error = "Sorry, this request token is not valid. Please request a password reset again.";
						$scope.forgotpwd = true;
						$scope.poperr();
					}
		});
	$scope.onSubmitsendEmail = function() {


				$http.post('/api/forgot-password/', $scope.forgot).
					success(function(data) {
						if(data.noexist) {
							$scope.noexist = "Sorry, this email address was not found";
							$scope.error = "Sorry, this email address was not found";
							$scope.poperr();
						} else {
							$scope.noexist = "";
							$scope.success = "A Reset Password Email has been sent to your address";
							$scope.msg = "A Reset Password Email has been sent to your address";
							$scope.forgotpwd = false;
							$scope.popsuccess();
						}
				}).
				error(function(data) {
						$scope.error = "Something went wrong :(";
						$scope.msg  = "Something went wrong :(";
						$scope.poperr();
				});
		};


		$scope.onSubmitNewPassword = function() {
				$http.post('/api/reset-password/' + $routeParams.token, $scope.resetpassword).
					success(function(data) {
							$location.url('/login');
							window.location = "/login";
						});
				};
}]);