/******************************************************************************************************************
 Log In Controller
 - Display 'forgot password' form, success message, errors and send reset password to email address
******************************************************************************************************************/
reddonor.controller('LoginCtrl', ['$scope', '$http', '$location', 'toaster',
	function($scope, $http, $location, toaster) {

		$scope.forgotpwd = false;

		$scope.showForgot = function () {
			if($scope.forgotpwd === true) {
				$scope.forgotpwd = false;
			} else {
				$scope.forgotpwd = true;
			}
		};

			$scope.poperr = function() {
				toaster.pop('error', "", $scope.error, 5000, true);
			};

			$scope.popsuccess = function() {
				toaster.pop('success', "", $scope.success, 5000, true);
			};

		$scope.onSubmitsendEmail = function() {

					$http.post('/api/forgot-password/', $scope.forgot).
						success(function(data) {
							if(data.noexist) {
								$scope.noexist = "Sorry, this email address was not found";
								$scope.error = "Sorry, this email address was not found";
								$scope.poperr();
							} else {
								$scope.noexist = "";
								$scope.success = "A Reset Password Email has been sent to your address";
								$scope.forgotpwd = false;
								$scope.popsuccess();
							}
					}).
					error(function(data) {
							$scope.error = "Something went wrong :(";
							$scope.poperr();
					});
			};
}]);