/******************************************************************************************************************
 Organisation Profile Controller
 - Get Organisation Info, Charts for Causes, Pledges, Gender and Blood Type
******************************************************************************************************************/

reddonor.controller('OrgProfilePages', ['$scope', '$http', '$location', '$routeParams', 
	function($scope, $http, $location, $routeParams) {
		$scope.title = "Divsion of Causes";
		$scope.number = "Pledges";
		$scope.data = [];
		$scope.bdata = [];
		$scope.charts = ['BarChart', 'PieChart', 'ColumnChart', 'Table', 'LineChart', 'ComboChart', 'AreaChart', 'ScatterChart'];
		$scope.selectedChart = $scope.charts[1];

	$http.get('/api/org-profile-info/' + $routeParams.oid).
		success(function(data) {
			$scope.orgprofile = data;
		 });


	$http.get('/api/org-drives-cause-chart/' + $routeParams.oid).
		success(function(data) {
			$scope.pieData = data;
				for(var i=0; i<$scope.pieData.length; i++) {
					//console.log($scope.pieData[i]);
					for(var prop in $scope.pieData[i]) {
						//console.log(prop);
						$scope.data.push([prop, +$scope.pieData[i][prop]]);
					} 
				}
		 });   

	$http.get('/api/org-pledges-cause-chart/' + $routeParams.oid).
		success(function(bdata) {
			$scope.pieData = bdata;
				for(var i=0; i<$scope.pieData.length; i++) {
					//console.log($scope.pieData[i]);
					for(var prop in $scope.pieData[i]) {
						//console.log(prop);
						$scope.bdata.push([prop, +$scope.pieData[i][prop]]);
					} 
				}
				//console.log($scope.bdata);
		 });

	 $scope.cdata = [];

	 $http.get('/api/org-gender-chart/' + $routeParams.oid).
		success(function(cdata) {
			$scope.chartData = cdata;
				for(var i=0; i<$scope.chartData.length; i++) {
					//console.log($scope.chartData[i]);
					for(var prop in $scope.chartData[i]) {
						//console.log(prop);
						$scope.cdata.push([prop, +$scope.chartData[i][prop]]);
					} 
				}
		 });

	 $scope.ddata = [];

	 $http.get('/api/org-drives-bloodtype-chart/' + $routeParams.oid).
		success(function(ddata) {
			$scope.chartData = ddata;
				for(var i=0; i<$scope.chartData.length; i++) {
					//console.log($scope.chartData[i]);
					for(var prop in $scope.chartData[i]) {
						//console.log(prop);
						$scope.ddata.push([prop, +$scope.chartData[i][prop]]);
					} 
				}
		 });

		$scope.edata = [];

	 $http.get('/api/org-pledges-bloodtype-chart/' + $routeParams.oid).
		success(function(edata) {
			$scope.chartData = edata;
				for(var i=0; i<$scope.chartData.length; i++) {
					//console.log($scope.chartData[i]);
					for(var prop in $scope.chartData[i]) {
						//console.log(prop);
						$scope.edata.push([prop, +$scope.chartData[i][prop]]);
					} 
				}
		 });
	}]);