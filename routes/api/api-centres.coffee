###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###******************************************************************************************************************
GET DONATION CENTRE LIST
Usage: Displays list of Donation Centres (ALL & By Country)
******************************************************************************************************************###
exports.donationcentres = (req, res) ->
  connection = database.getConnection()  
  countryURL = req.params.loc
  if countryURL    
    # returns donation centres information
    q1 = "SELECT l.location_id, l.location_name, l.location_title, l.location_description, l.location_address, l.location_tel, l.location_email, l.location_site, l.location_hours, l.location_map, l.location_banner, c.country_name, c.ITU_calling, l.location_country FROM location l, country c WHERE l.location_country=c.country_id AND c.country_url = ?;"
    params = [countryURL]
    connection.connect()
    connection.query q1, params, (err, results) ->
      if err
        console.log err
      else        
        res.json results
      connection.end()
  else    
    # returns donation centres information
    q2 = "SELECT l.location_id, l.location_name, l.location_title, l.location_description, l.location_address, l.location_tel, l.location_email, l.location_site, l.location_hours, l.location_map, l.location_banner, c.country_name, c.ITU_calling, l.location_country FROM location l, country c WHERE l.location_country=c.country_id;"
    connection.connect()
    connection.query q2, (err, results, fields) ->
      if err
        console.log err
      else
        res.json results
      connection.end()

###***********************************************************
Get Blood Bank Info
Usage: Displays All the Blood Donation Centre Info
***********************************************************###
exports.bloodbankinfo = (req, res) ->
  connection = database.getConnection()
  # returns blood donation centre info from donation centre id
  bid = req.params.bid
  q = "SELECT (SELECT COUNT(l.location_id) FROM pledge_users p, location l WHERE p.pledge_location=l.location_id AND l.location_id = ?) AS 'location_pledges', l.location_name, l.location_description, l.location_address, l.location_tel, l.location_map, l.location_email, l.location_lat, l.location_lng, l.location_site, l.location_hours, l.location_map, c.country_name, c.ITU_calling AS 'Area_Code' FROM location l, country c, country c1 WHERE l.location_country=c.country_id AND l.location_id = ? AND l.location_country=c1.country_id;"
  params = [
    bid
    bid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Blood Bank Profile Info Loaded ---"
      res.send results
    connection.end()

###***********************************************************
GET BLOOD LEVELS BY DONATION CENTRES
Usage: Displays latest blood levels of a donation centre
***********************************************************###
exports.bloodbanklevels = (req, res) ->
  connection = database.getConnection()
  # returns blood levels for donation centre from donation centre id
  bid = req.params.bid
  q = "SELECT b.oposlevel, b.oneglevel, b.aposlevel, b.aneglevel, b.bposlevel, b.bneglevel, b.abposlevel, b.abneglevel, l.location_name, b.date_reached FROM blood_levels b, location l WHERE b.location_id=l.location_id AND b.location_id = ? ORDER BY b.date_reached DESC LIMIT 1;"
  params = [bid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Blood Bank Blood Levels Loaded ---"
      res.send results
    connection.end()

