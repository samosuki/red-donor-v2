'use strict';

/* Filters */

reddonor.filter('interpolate', ['version', function(version) {
		return function(text) {
			return String(text).replace(/\%VERSION\%/mg, version);
		};
	}])
	.filter('fromNow', function() {
		return function(dateString) {
			return moment(new Date(dateString)).fromNow();
		};
	})
	.filter('startFrom', function() {
		return function(input, start) {
			if (!_.isArray(input) && !_.isString(input)) {
				return input;
			}
			start = +start || 0;
			return input.slice(start);
		};
	})
	.filter('isCentre', function() {
			return function(input, centre) {
					if (typeof centre == 'undefined' || centre == null) {
							return input;
					} else {
							var out = [];
							angular.forEach($scope.countries, function(val, key){
								if(val !== $scope.selectedCountry){
									this.push(val);
									}
								},filteredCountries);
							console.log(input);
							console.log("Centre " + centre);
							return out;
					}
			};
	});


