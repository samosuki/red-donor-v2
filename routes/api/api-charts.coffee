###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###******************************************************************************************************************
 Get Chart Data for Total Pledges By Gender For Location
Usage: Displays Chart Data for Total Pledges By Gender For Location
******************************************************************************************************************###
exports.chartgenderlocation = (req, res) ->
  connection = database.getConnection()
  bid = req.params.bid
  q = "SELECT (SELECT COUNT(users_id) FROM pledge_users p, location l, gender g WHERE p.pledge_location=l.location_id AND l.location_id = ? AND p.pledge_gender=g.gender_id AND g.gender_id = 1) AS 'Male', (SELECT COUNT(users_id) FROM pledge_users p, location l, gender g WHERE p.pledge_location=l.location_id AND l.location_id = ? AND p.pledge_gender=g.gender_id AND g.gender_id = 2) AS 'Female'"
  params = [
    bid
    bid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Chart For Total Pledges By Gender For Location Loaded ---"
      res.send results
    connection.end()

# Get Chart Data for Total Pledges By Blood Type For Location
exports.chartbloodtypelocation = (req, res) ->
  connection = database.getConnection()
  bid = req.params.bid
  q = "SELECT (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 1) AS 'Pledged O+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 2) AS 'Pledged O-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 3) AS 'Pledged A+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 4) AS 'Pledged A-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 5) AS 'Pledged B+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 6) AS 'Pledged B-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 7) AS 'Pledged AB+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, users u, location l, bloodtype b WHERE p.users_id=u.users_id AND p.pledge_location=l.location_id AND l.location_id = ? AND u.user_bloodtype=b.bloodtype_id AND b.bloodtype_id = 8) AS 'Pledged AB-'"
  params = [
    bid
    bid
    bid
    bid
    bid
    bid
    bid
    bid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Chart For Total Pledges By Blood Type For Location Loaded ---"
      res.send results
    connection.end()

# Get Chart Data for Total Pledges By Causes For Location
exports.chartcauseslocation = (req, res) ->
  connection = database.getConnection()
  # Need to add new causes
  bid = req.params.bid
  q = "SELECT (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 1) AS 'General Surgery', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 2) AS 'Sickle Cell', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 3) AS 'Heart Surgery', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 4) AS 'Dengue', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 5) AS 'Trauma & Accident ', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 6) AS 'Organ Transplant', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 7) AS 'Blood Cancer', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 8) AS 'Orthopadedic', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 9) AS 'Severe Burns', (SELECT COUNT(d.uses_id) FROM pledge_users p, drive_uses d, location l WHERE p.pledge_use=d.uses_id AND p.pledge_location=l.location_id AND l.location_id = ? AND d.uses_id = 10) AS 'Obstetrics'"
  params = [
    bid
    bid
    bid
    bid
    bid
    bid
    bid
    bid
    bid
    bid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Chart For Total Pledges By Causes For Location Loaded ---"
      res.send results
    connection.end()

# Get Chart Data for Blood Levels For Location
exports.chartlevelslocation = (req, res) ->
  connection = database.getConnection()
  bid = req.params.bid
  q = "SELECT `level_o-pos` AS `O+`, `level_o-neg` AS `O-`, `level_a-pos` AS 'A+', `level_a-neg` AS 'A-', `level_b-pos` AS 'B+', `level_b-neg` AS 'B-', `level_ab-pos` AS 'AB+', `level_ab-neg` AS 'AB-' FROM `location` WHERE `location_id` = ?;"
  params = [bid]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Chart For Blood Levels For Location Loaded ---"
      res.send results
    connection.end()


# Get Chart Data for Total Pledges By Gender For Organisation
exports.chartgenderorg = (req, res) ->
  connection = database.getConnection()
  bid = req.params.oid
  q = "SELECT (SELECT COUNT(p.pledge_gender) FROM pledge_users p, gender g, org o WHERE p.pledge_gender=g.gender_id AND p.pledge_org=o.org_id AND o.org_id = ? AND g.gender_id = 1) AS 'Male', (SELECT COUNT(p.pledge_gender) FROM pledge_users p, gender g, org o WHERE p.pledge_gender=g.gender_id AND p.pledge_org=o.org_id AND o.org_id = ? AND g.gender_id = 2) AS 'Female'"
  params = [
    bid
    bid
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "--- Chart For Total Pledges By Gender For Organisation Loaded ---"
      res.send results
    connection.end()



# Get Chart Data for Drives Created by Cause for Organisation
exports.orgdrivescauses = (req, res) ->
  connection = database.getConnection()
  oid = req.params.oid
  q = "SELECT (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 1 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'General Surgery', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 2 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Sickle Cell', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 3 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Heart Surgery', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 4 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Dengue', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 5 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Trauma & Accident', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 6 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Organ Transplant', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 7 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Blood Cancer', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 8 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Orthopadedic', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 9 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Severe Burns', (SELECT COUNT(d.drive_id) FROM drive d, drive_uses du, org o WHERE d.drive_use=du.uses_id AND du.uses_id = 10 AND d.created_by_org=o.org_id AND o.org_id = ?) AS 'Obstetrics' FROM drive d, drive_uses du WHERE d.drive_use=du.uses_id LIMIT 1;"
  params = [
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
  ]
  connection.connect()
  connection.query q, params, (err, results, fields) ->
    if err
      console.log err
    else
      console.log "--- Chart For Drives Created By Causes For Organisation Loaded ---"
      res.send results
    connection.end()



# Get Chart Data for Pledges by Cause for Organisation
exports.orgpledgescauses = (req, res) ->
  connection = database.getConnection()
  # Need to add new pledges
  oid = req.params.oid
  q = "SELECT (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 1 AND p.pledge_org = ?) AS 'General Surgery', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 2 AND p.pledge_org = ?) AS 'Sickle Cell', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 3 AND p.pledge_org = ?) AS 'Heart Surgery', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 4 AND p.pledge_org = ?) AS 'Dengue', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 5 AND p.pledge_org = ?) AS 'Accident', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 6 AND p.pledge_org = ?) AS 'Organ Transplant', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 7 AND p.pledge_org = ?) AS 'Blood Cancer', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 8 AND p.pledge_org = ?) AS 'Orthopadedic', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 9 AND p.pledge_org = ?) AS 'Severe Burns', (SELECT COUNT(pledge_use) FROM pledge_users p, drive_uses d WHERE p.pledge_use=d.uses_id AND p.pledge_use = 10 AND p.pledge_org = ?) AS 'Obstetrics'"
  params = [
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
  ]
  connection.connect()
  connection.query q, params, (err, results, fields) ->
    if err
      console.log err
    else
      console.log "--- Chart For Pledges by Causes For Organisation Loaded ---"
      res.send results
    connection.end()

# Get Chart Data for Drives by Blood Type for Organisation
exports.orgdrivesbloodtype = (req, res) ->
  connection = database.getConnection()
  oid = req.params.oid
  q = "SELECT (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 1) AS 'O+', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 2) AS 'O-', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 3) AS 'A+', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 4) AS 'A-', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 5) AS 'B+', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 6) AS 'B-', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 7) AS 'AB+', (SELECT COUNT(b.bloodtype_id) FROM drive d, org o, bloodtype b WHERE d.created_by_org=o.org_id AND o.org_id = ? AND d.drive_bloodtype=b.bloodtype_id AND b.bloodtype_id = 8) AS 'AB-'"
  params = [
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
  ]
  connection.connect()
  connection.query q, params, (err, results, fields) ->
    if err
      console.log err
    else
      console.log "--- Chart For Drives by Blood Type For Organisation Loaded ---"
      res.send results
    connection.end()


# Get Chart Data for Pledges by Blood Type for Organisation
exports.orgpledgesbloodtype = (req, res) ->
  connection = database.getConnection()
  oid = req.params.oid
  q = "SELECT (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 1) AS 'Pledged O+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 2) AS 'Pledged O-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 3) AS 'Pledged A+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 4) AS 'Pledged A-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 5) AS 'Pledged B+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 6) AS 'Pledged B-', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 7) AS 'Pledged AB+', (SELECT COUNT(b.bloodtype_id) FROM pledge_users p, org o, users u, bloodtype b WHERE p.pledge_org=o.org_id AND p.users_id=u.users_id AND u.user_bloodtype=b.bloodtype_id AND o.org_id = ? AND b.bloodtype_id = 8) AS 'Pledged AB-'"
  params = [
    oid
    oid
    oid
    oid
    oid
    oid
    oid
    oid
  ]
  connection.connect()
  connection.query q, params, (err, results, fields) ->
    if err
      console.log err
    else
      console.log "--- Chart For Pledges By Blood Type For Organisation Loaded ---"
      res.send results
    connection.end()