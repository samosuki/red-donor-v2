/******************************************************************************************************************
 Account Settings Directive
 - Save account info, change password, flash notification, country list and user settings
******************************************************************************************************************/

reddonor.directive('settingsform', ['$http', '$location', '$window', 'toaster', function ($http, $location, $window, toaster) {
    var settingsformFn;
    settingsformFn = function(scope, element, attrs) {
      scope.onSubmitSaveAccount = function() {

      $http.put('/api/save-account/', scope.settings).
          success(function(data) {
            if(data.error) {
              scope.error = ":( An error occurred. Changes were not saved";
              scope.poperr();
            } else {
              scope.error = "";
              scope.success = "Your changes have been saved to your account";
              scope.popsuccess();
              //$window.location = "/profile";
              //$location.url('/profile');
            }
        });
      };

      scope.popsuccess = function() {
        toaster.pop('success', "", scope.success, 5000, true);
        console.log("success");
      };
      scope.poperr = function() {
        toaster.pop('error', "", scope.error, 5000, true);
      };


      scope.onSubmitChangePassword = function() {
        $http.put('/api/save-password/', scope.pwd).
          success(function(data) {
            if(data.incorrect) {
              scope.error = "Current password is incorrect";
              scope.poperr();
            }
						if(data.successful) {
              scope.incorrect = "";
              scope.success = "Your password has been changed";
              scope.popsuccess();
							//$location.url('/profile');
              //$window.location = "/profile";
						}
        });
      };

			$http.get('/api/countries/').
				success(function(data) {
					scope.country = data;
					//console.log(data);
					return;
				});

			$http.get('/api/user-settings/').
        success(function(data, status) {
          scope.settings = data;
          scope.status = status;
          return;
    }).
      error(function(data, status) {
        scope.settings = data || "Request failed";
        scope.status = status;
      });
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/settings-form.jade',
      link: settingsformFn

    };
  }]);