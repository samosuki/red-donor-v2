###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore")
bcrypt = require("bcrypt")
moment = require("moment")
nodemailer = require("nodemailer")
async = require("async") # Async Functions
apidrives = require("../api/api-drives") # Drives API
apipages = require("../api/api-pages") # Pages API
database = require(path.join(__dirname, "../database"))
authform = require("./auth-forms") # Authentication Forms
authdrive = require("./auth-drives") # Authentication Drives
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')


###***********************************************************
Check for Valid Date for New Blood Drive
Usage:  calls checkDate function to check if the deadline date has already passed, if not, call autheticateDrive to create a new blood drive
***********************************************************###
exports.checkDate = (deadlineDate, callback) ->
  deadline = moment(deadlineDate)
  currentDate = moment(Date())
  console.log "Deadline Date " + deadline
  console.log "Current Date " + currentDate
  if (deadline < currentDate)
    console.log "No time travel..."
    passed = true
    callback passed
    return
  else
    callback false
    console.log "Good to go..."
    return

###***********************************************************
Check for Guest Account
Usage:  calls checkAge function to check if the user is over 16
***********************************************************###
exports.checkguest = (DriveID, callback) ->
  connection = database.getConnection()
  connection.connect()
  q = "SELECT d.created_by_user FROM drive d WHERE d.drive_id = ?;"
  q1 = "SELECT u.user_bloodtype FROM users u WHERE u.users_id = ?;"
  UserID = undefined
  params = [DriveID]
  
  connection.query q, params, (err, results) ->
    if err
      console.log err
      console.log params
    else 
      #console.log results
      #console.log "Get UserID"
      UserID = results[0].created_by_user
      params1 = [UserID]
      connection.query q1, params1, (err, results) ->
        if err
          console.log err
          #console.log params1
        else if results[0].user_bloodtype != null
          #console.log results
          console.log "Not a guest account"
          callback false
        else 
          console.log "Is Guest Account"
          callback true
      connection.end()

###***********************************************************
Check for Valid Age for New Signup
Usage:  calls checkAge function to check if the user is over 16
***********************************************************###
exports.checkAge = (dobDate, callback) ->
  todayYear = moment(Date()).year()
  teenYear = moment(dobDate).year()
  #.moment().add('y', 13);
  #console.log "Teen Year " + teenYear
  if ((todayYear - teenYear) < 13)
    console.log "Not of age"
    notofage = true
    callback notofage
    return
  else
    callback false
    console.log "Of Age..."
    return

###***********************************************************
Check for Valid Blood Type to Donate
Usage:  calls checkCompat function to check if the blood type of the user matches the drive bloodtype
***********************************************************###
exports.checkCompat = (userbloodtype, drivebloodtype, callback) ->
  connection = database.getConnection()
  console.log "Check Compat - User Bloodtype: " + userbloodtype
  compat = "b.`o-pos-receive`"  if drivebloodtype is 1
  compat = "b.`o-neg-receive`"  if drivebloodtype is 2
  compat = "b.`a-pos-receive`"  if drivebloodtype is 3
  compat = "b.`a-neg-receive`"  if drivebloodtype is 4
  compat = "b.`b-pos-receive`"  if drivebloodtype is 5
  compat = "b.`b-neg-receive`"  if drivebloodtype is 6
  compat = "b.`ab-pos-receive`"  if drivebloodtype is 7
  compat = "b.`ab-neg-receive`"  if drivebloodtype is 8
  compat = "b.`ab-pos-receive`"  if drivebloodtype is 9
  q = "SELECT b.`bloodtype_name` FROM bloodtype b WHERE " + compat + "  = 1 AND b.`bloodtype_id` = ?;"
  connection.query q, [userbloodtype], (err, results, fields) ->
    if err
      console.log err
      callback null
    else
      if results.length > 0
        console.log "Bloodtype is a match..."
        callback false
      else
        console.log "Bloodtype does not match..."
        notcompat = true
        callback notcompat
        return
  connection.end()

###***********************************************************
CHECK FOR RECENT DONATION
Usage:  calls checkRecentDonate function to check if user donated during the past 3 months (90 days)
***********************************************************###
exports.checkRecentDonate = (UserID, callback) ->
  connection = database.getConnection()
  q = "SELECT p.`date_pledged`, d.`drive_name`, b.`bloodtype_name`, d.`pledge_count`, d.`drive_enddate` FROM drive d LEFT OUTER JOIN pledge_users p ON p.drive_id=d.drive_id, bloodtype b WHERE DATEDIFF(p.`date_pledged`, NOW()) > -90 AND d.drive_bloodtype=b.bloodtype_id AND p.users_id = ?;"
  params = [UserID]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback null
    if results.length > 0
      console.log "User donated too recently..."
      notrecent = false
      callback notrecent
      return
    else
      notrecent = true
      callback notrecent
      console.log "User can donate again..."
  connection.end()

###***********************************************************
CHECK IF BLOOD DRIVE HAS ALREADY ENDED
Usage:  calls checkAlreadyEnded function to check if the drive has ended
***********************************************************###

# Check if Drive Has Ended
###
exports.checkAlreadyEnded = (DriveID, callback) ->
  q = "SELECT d.`drive_id` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id AND d.`drive_id` = ?"
  connection.query q, [DriveID], (err, results) ->
    if err
      console.log err
      callback err
    if results.length > 0
      console.log "Drive Ended Already..."
      ended = true
      callback null, ended
    else
      ended = true
      callback null, ended
      console.log "Drive Is Still Active..."
###

###***********************************************************
CHECK IF BLOOD DRIVE HAS ALREADY ENDED
Usage:  calls checkHasAlreadyEnded function to check if the drive has ended
***********************************************************###
exports.checkHasAlreadyEnded = (DriveID, callback) ->
  connection = database.getConnection()
  connection.connect()
  q = "SELECT d.`drive_id` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id AND d.`drive_id` = ?"
  connection.query q, [DriveID], (err, results) ->
    if err
      console.log err
      callback err
    if results.length > 0
      console.log "Drive Ended Already..."
      ended = true
      callback ended
    else
      ended = false
      callback ended
      console.log "Drive Is Still Active..."
  connection.end()



# Check if User Already Pledged to A Drive
###
exports.checkAlreadyPledged = (UserID, DriveID, callback) ->
  q = "SELECT p.`date_pledged`, d.`drive_name`, b.`bloodtype_name`, d.`pledge_count`, d.`drive_enddate` FROM drive d LEFT OUTER JOIN pledge_users p ON p.drive_id=d.drive_id, bloodtype b WHERE d.drive_bloodtype=b.bloodtype_id AND p.users_id = ? AND d.drive_id=?;"
  params = [
    UserID
    DriveID
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback err
    if results.length > 0
      console.log "User Already Donated..."
      already = true
      callback already
      return
    else
      already = false
      callback already
      console.log "User Has Not Donated..."
###

###***********************************************************
# Check if User Already Pledged to This Drive
***********************************************************###

exports.checkAlreadyPledgedThisDrive = (UserID, DriveID, callback) ->  
  connection = database.getConnection()
  q = "SELECT p.`date_pledged`, d.`drive_name`, b.`bloodtype_name`, d.`pledge_count`, d.`drive_enddate` FROM drive d LEFT OUTER JOIN pledge_users p ON p.drive_id=d.drive_id, bloodtype b WHERE d.drive_bloodtype=b.bloodtype_id AND p.users_id = ? AND d.drive_id=?;"
  params = [
    UserID
    DriveID
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback null    
    #return;
    if results.length > 0
      console.log "User Already Donated To This Drive..."
      pledgedalready = true
      callback pledgedalready
      return
    else
      pledgedalready = false
      callback pledgedalready
      console.log "User Has Not Donated To This Drive..."
  connection.end()


###***********************************************************
Check if User and Drive Location Is In The Same Country
***********************************************************###
exports.checkSameLocation = (DriveID, LocationID, callback) ->
  connection = database.getConnection()
  q = "SELECT d.`drive_id` FROM drive d WHERE d.`drive_id` = ? AND d.`drive_country` = ?;"
  params = [
    DriveID
    LocationID
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback null
    if results.length > 0
      console.log "User is in same location..."
      same = true
      callback same
    else
      same = false
      callback same
      console.log "User is not in same location..."
  connection.end()


# Get Compatible Blood Types [For Detail Page] (Using Drive Info)
###
exports.bloodcompat = (driveID, callback) ->
  q = "SELECT d.`drive_bloodtype` FROM drive d WHERE d.`drive_id` = ?;"
  connection.query q, [driveID], (err, results) ->
    if err
      console.log err
      callback null
    else
      bloodtype = results[0]
      drivebloodtype = bloodtype.drive_bloodtype      
      #console.log("Bloodtype: " + drivebloodtype);
      callback null, drivebloodtype
###

###***********************************************************
ADD NEW DONATION PLEDGE
Usage: i. gets the use of the blood drive
			ii. checks that the blood drive use exists
			iii. inserts pledge information and updates pledge count
***********************************************************###
exports.pledgeDonation = (DriveID, UserID, BloodBankID, LocationID, GenderID, OrgID, PledgeCount, UserName) ->
  connection = database.getConnection()
  connection.connect()
  datePledged = new Date()  
  #var datePledged = new time.Date()
  #datePledged.setTimezone("America/New_York")
  pledgeType = 1
  driveUse = undefined
  async.waterfall [
    (callback) ->
      
      # i. Get the use of the blood drive from the drive table
      q3 = "SELECT d.drive_use FROM drive d WHERE d.drive_id = ?;"
      params3 = [DriveID]
      connection.query q3, params3, (err, results) ->
        if err
          console.log err
          callback err
        else
          driveUse = results[0].drive_use
          callback null, driveUse

    (driveUse, callback) ->
      
      # ii. Check that Use of Drive exists
      if driveUse isnt null
        
        # iii. Inserts pledge information and updates pledge count
        q = "INSERT INTO pledge_users SET drive_id = ?, users_id = ?, pledge_type = ?, pledge_location = ?, pledge_country = ?, pledge_use = ?, pledge_gender = ?, pledge_org = ?, date_pledged = ?;"
        q2 = "UPDATE drive SET pledge_count = ? WHERE drive_id = ?;"
        q3 = "SELECT d.drive_name, l.location_name, c.country_name, b.bloodtype_name, d.drive_enddate, u.user_name, u.user_email FROM pledge_users p, location l, drive d, country c, bloodtype b, users u WHERE p.pledge_location=l.location_id AND p.drive_id=d.drive_id AND p.pledge_country=c.country_id AND d.drive_id = ? AND d.drive_bloodtype=b.bloodtype_id AND p.users_id=u.users_id;"
        q4 = "SELECT u.user_name, drive_name, pledge_count, u.user_email FROM `drive`, users u WHERE drive.created_by_user=u.users_id AND drive_id = ?;"
        PledgeCount = PledgeCount++
        console.log PledgeCount
        params = [
          DriveID
          UserID
          pledgeType
          BloodBankID
          LocationID
          driveUse
          GenderID
          OrgID
          datePledged
        ]
        params2 = [
          PledgeCount
          DriveID
        ]
        params3 = [
          DriveID
        ]
        params4 = [
          DriveID
        ]
        connection.query q, params, (err, results) ->
          if err
            console.log err
          else        
            #console.log "insert pledge"
        connection.query q2, params2, (err, results) ->
          if err
            console.log err
          else
            #console.log "update pledge count"
        connection.query q3, params3, (err, results) ->
          if err
            console.log err
          else
            # Send Pledge Donation Email
            #console.log results
            #console.log "send pledge email"
            userName = results[0].user_name
            driveName = results[0].drive_name
            deadlineDate = moment(results[0].drive_enddate).format("dddd, MMMM Do YYYY")
            donationCentre = results[0].location_name
            countryName = results[0].country_name
            userEmail = results[0].user_email            
            
           
            message =
              html: "<html>" + "<title>Red Donor - Pledged Donation</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Thank you for your pledge, " + userName + "!</h3>" + "<p style='font-size: 14px;'>Thank you for pledging to donate blood to <strong>" + driveName + "</strong>. The requested deadline date is <strong>" + deadlineDate + "</strong> at the <strong>" + donationCentre + "</strong>, " + countryName + ".</p>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Support</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
              subject: "Red Donor - Pledged Donation"
              from_email: "support@reddonor.com"
              from_name: "Red Donor"
              to: [
                email: userEmail
                name: userName
                type: "to"
              ]
              headers:
                "Reply-To": "support@reddonor.com"

              important: false
              inline_css: true
              merge: true
            
            mandrill_client.messages.send
              message: message
            , ((result) ->
              #console.log result
              console.log "pledge email sent"
            ), (e) ->
              console.log "A mandrill error occurred: " + e.name + " - " + e.message
            
        connection.query q4, params4, (err, results) ->
          if err
            console.log err
          else
            # Send Donation Pledged Email
            #console.log results
            #console.log "send pledge email"
            #userName = req.session.user.user_name
            pledgeName = results[0].user_name
            driveName = results[0].drive_name
            pledgeCount = results[0].pledge_count
            userEmail = results[0].user_email  

            authdrive.checkguest DriveID, (guest) ->
              if guest is true
                message =
                  html: "<html>" + "<title>Red Donor - Donation Pledged</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Hi, " + UserName + "!</h3>" + "<p style='font-size: 14px;'><strong>"+ pledgeName + "</strong> has pledged to donate blood to <strong>" + driveName + "</strong>. So far <strong>" + pledgeCount + "</strong> people have pledged to donate boood." + "<p style='font-size: 14px;'>You are using a Guest account. To access more features of Red Donor please <a href='http://my.reddonor.com/guestsignup' target='_blank'>click here</a> to complete your account signup or copy and paste this link into your web browser. - <a href='my.reddonor.com/guestsignup'>http://my.reddonor.com/guestsignup</a>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Support</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
                  subject: "Red Donor - Pledged Donation"
                  from_email: "support@reddonor.com"
                  from_name: "Red Donor"
                  to: [
                    email: userEmail
                    name: UserName
                    type: "to"
                  ]
                  headers:
                    "Reply-To": "support@reddonor.com"

                  important: false
                  inline_css: true
                  merge: true
              else            
                message =
                  html: "<html>" + "<title>Red Donor - Donation Pledged</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Hi, " + UserName + "!</h3>" + "<p style='font-size: 14px;'><strong>"+ pledgeName + "</strong> has pledged to donate blood to <strong>" + driveName + "</strong>. So far <strong>" + pledgeCount + "</strong> people have pledged to donate boood." + "<p style='color:#979797; font-size: 13px;'>Red Donor Support</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
                  subject: "Red Donor - Pledged Donation"
                  from_email: "support@reddonor.com"
                  from_name: "Red Donor"
                  to: [
                    email: userEmail
                    name: UserName
                    type: "to"
                  ]
                  headers:
                    "Reply-To": "support@reddonor.com"

                  important: false
                  inline_css: true
                  merge: true
              
              mandrill_client.messages.send
                message: message
              , ((result) ->
                #console.log result
                console.log "pledge to donate email sent"
              ), (e) ->
                console.log "A mandrill error occurred: " + e.name + " - " + e.message
              
              console.log "Pledge Updated & Emails Sent"
        connection.end()
  ], (err) ->
    err  if err
  #connection.end()

###***********************************************************
Unpledge Donation Pledge
Usage: Delete donation pledge and pledge count
***********************************************************###
exports.unpledgeDonation = (DriveID, UserID, PledgeCount) ->
  connection = database.getConnection()
  connection.connect()
  #var datePledged = new Date();
  #var q = "INSERT INTO pledge_users SET drive_id = ?, users_id = ?, date_pledged = ?;";
  Pledge_Count = --PledgeCount
  q = "DELETE FROM pledge_users WHERE drive_id = ? AND users_id = ?"
  q2 = "UPDATE drive SET pledge_count = ? WHERE drive_id = ?;"
  params = [
    DriveID
    UserID
  ]
  params2 = [
    Pledge_Count
    DriveID
  ]
  connection.query q, params, (err, results, fields) ->
    if err
      console.log err
    else 
    	console.log "--- Deleted Donation ---" 
  connection.query q2, params2, (err, results, fields) ->
    if err
      console.log err
    else
    	console.log "--- Deleted Pledge Count ---"
  connection.end()

###***********************************************************
Get Pledge Count for Current Drive
***********************************************************###
exports.getPledgeCount = (DriveID, callback) ->
  connection = database.getConnection()
  connection.connect()
  q = "SELECT COUNT(*) AS pledge_count FROM drive d LEFT OUTER JOIN pledge_users p ON p.drive_id=d.drive_id, bloodtype b WHERE d.drive_bloodtype=b.bloodtype_id AND d.drive_id = ?"
  connection.query q, [DriveID], (err, results, fields) ->
    if err
      console.log err
      callback false
      return
    else
      pledge_count = results[0].pledge_count
      callback pledge_count
      console.log pledge_count
      return
  connection.end()

###***********************************************************
Create Drive Title
***********************************************************###
exports.createTitle = (recipientName, callback) ->
  connection = database.getConnection()
  connection.connect()
  q1 = "SELECT REPLACE(TRIM(LOWER(?)), ' ', '-') AS 'drive_title' FROM drive d;"
  connection.query q1, [recipientName], (err, results) ->
    if err
      console.log err
      callback null
      return
    else      
      #console.log(results[0].drive_title);
      driveTitle = results[0].drive_title
      callback driveTitle
      console.log "Title created"
      return
  connection.end()

###***********************************************************
Create New Blood Drive
***********************************************************###
exports.authenticateDrive = (recipientName, driveSummary, donationNeeded, bloodTypes, deadlineDate, driveCountry, driveCentre, userCreated, driveUse, orgCreated, callback) ->
  connection = database.getConnection()
  #connection.connect()
  dateCreated = new Date() 
  deadline = new Date(deadlineDate)
  #console.log(deadline)
  #var dateCreated = new time.Date();
  #dateCreated.setTimezone("America/New_York");
  authdrive.createTitle recipientName, (driveTitle) ->
    success = true
    q = "INSERT INTO drive SET drive_name = ?, drive_title = ?, drive_description = ?, donations_needed = ?, drive_bloodtype = ?, drive_enddate = ?, drive_country = ?, drive_location = ?, created_by_user = ?, date_created = ?, drive_use = ?, created_by_org = ?"
    params = [
      recipientName
      driveTitle
      driveSummary
      donationNeeded
      bloodTypes
      deadline
      driveCountry
      driveCentre
      userCreated
      dateCreated
      driveUse
      orgCreated
    ]
    connection.query q, params, (err, results) ->
      if err
        console.log err        
        console.log(params);
        callback false      
      #return;
      else
        success = true
        callback success
        console.log "New Blood Drive Created!"
    connection.end()
  #connection.end()

