 /******************************************************************************************************************
  Profile Appointments Controller 
 - Get user profile info and appointment schedules
******************************************************************************************************************/

reddonor.controller('ApptsCtrl', ['$scope', '$http', function($scope, $http) {
	$http.get('/api/appointment-schedules/').
    success(function(data) {
      $scope.appts = data;
        //console.log(data);
    });
  $http.get('/api/profile-info/').
    success(function(data) {
      $scope.profiles = data;
    });
}]);