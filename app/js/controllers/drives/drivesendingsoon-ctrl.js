/******************************************************************************************************************
 Drives Ending Soon Controller (Paged)
  - Display drives ending soon, pagination and no blood drives (empty) set
******************************************************************************************************************/

reddonor.controller('SoonEndPagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window',
	function($scope, $http, $routeParams, $location, $window) {
			$scope.drives = [];
			$scope.content = [];
			$scope.empty = true;
			$scope.emptydrive = false;
			$scope.currentPage = 0;
			$scope.pageSize = 8;			

	// Get Previous Ending Soon Drive Page
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$location.url('../drives/soon-ending?page=' + $scope.currentPage);
		}
	};
	// Get Next Ending Soon Drive Page
	$scope.nextPage = function () {
		if ($scope.currentPage <  $scope.numPages-1) {
			$scope.nextPage = $scope.currentPage + 2;
			$location.url('../drives/soon-ending?page=' + $scope.nextPage);
		}
	};
	// Get blood drive information
	$scope.getDrives = function() {
		$http.get('/api/drives-endingsoon/' + $routeParams.page).
				success(function(data) {
					$scope.drives = data[0];
					$scope.empty = false;
					$scope.limit = 8;

					if(data[0][0])
						$scope.totalItems = data[0][0].Result_Count;
					$scope.pageSize = 8;
					$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
					for(var i = 0; i<$scope.numPages; i++) {
						$scope.content.push(i);
					}
					
					if(($routeParams.page != null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}
					if($scope.currentPage >= 3) {
						$scope.showCurrent = false;
						$scope.hideCurrent = false;
					} else {
						$scope.showCurrent = true;
						$scope.hideCurrent = true;
					}

					if($scope.drives == "") {
						$scope.text = "No New Ending Soon Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			// Get Soon Ending Drives
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getDrives();
		}
	}]);