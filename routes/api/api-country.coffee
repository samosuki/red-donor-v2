###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###******************************************************************************************************************
GET COUNTRIES LIST
Usage: Populates dropdown list for with all countries user to select a Country
******************************************************************************************************************###
exports.countries = (req, res) ->
  connection = database.getConnection()  
  # returns all in list of caricom countries
  q = "SELECT `country_id`, `country_name`, `country_url` FROM `country`"
  connection.connect()
  connection.query q, (err, results) ->
    if err
      console.log err
    else
      res.json results
    connection.end()

###******************************************************************************************************************
GET COUNTRY [CREATE DRIVE PAGE]
Usage: Populates dropdown list for user to select country drive takes place in. The Selected Country is the same
country that they reside in stored in the User Session
******************************************************************************************************************###
exports.country = (req, res) ->
  connection = database.getConnection()
  # if session for user exists get country from data and return country id and name
  if req.session.user
    country = req.session.user.user_country
    q = "SELECT c.`country_id`, c.`country_name` FROM country c WHERE c.`country_id` = ?;"
    connection.connect()
    connection.query q, [country], (err, results) ->
      if err        
        console.log err
      else
        res.json results
      connection.end()