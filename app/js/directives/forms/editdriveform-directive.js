/******************************************************************************************************************
 Edit Drive Directive
 - Gets countries, bloodtypes, uses, donations
******************************************************************************************************************/

reddonor.directive('editdriveform', ['$http', '$location', '$routeParams', function ($http, $location, $routeParams) {
    var editdriveformFn;
    editdriveformFn = function(scope, element, attrs) {
      
      $http.get('/api/countries/').
        success(function(data) {
          scope.country = data;
          //console.log(data);
          return;
      });
      $http.get('/api/bloodtype/').
        success(function(data) {
          scope.bloodtype = data;
          //console.log(data);
          return;
      });

      // Get Uses List
      $http.get('/api/uses/').
        success(function(data) {
          scope.uses = data;
          //console.log(data);
          return;
      });

      scope.donations = [
        { id: 1, nos:"1" },
        { id: 2, nos:"2" },
        { id: 3, nos:"3" },
        { id: 4, nos:"4" },
        { id: 5, nos:"5" },
        { id: 6, nos:"6" },
        { id: 7, nos:"7"},
        { id: 8, nos:"8"},
        { id: 9, nos:"9"},
        { id: 10, nos:"10"},
        { id: 11, nos:"11"},
        { id: 12, nos:"12"}
      ];
    };

    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/editDrive-form.jade',
      link: editdriveformFn

    };
  }]);
  