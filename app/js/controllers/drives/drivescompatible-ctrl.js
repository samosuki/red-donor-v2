/******************************************************************************************************************
 Get Compatible Drives Via User Bloodtype (Paged)
  - Display compatible drives, pagination, no blood drives (empty) set and compatible blood types list
******************************************************************************************************************/

reddonor.controller('CompatPagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window', 
	function($scope, $http, $routeParams, $location, $window) {
			$scope.drives = [];
			$scope.content = [];
			$scope.empty = true;
			$scope.emptydrive = false;
			$scope.currentPage = 0;
			$scope.pageSize = 8;

	// Get Previous Compatible Drive Page
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$location.url('../drives/compatible?page=' + $scope.currentPage);
			//$window.location = '../drives/compatible?page=' + $scope.currentPage;
		}
	};
	// Get Next Compatible Drive Page
	$scope.nextPage = function () {
		if ($scope.currentPage <  $scope.numPages-1) {
			$scope.nextPage = $scope.currentPage + 2;
			$location.url('../drives/compatible?page=' + $scope.nextPage);
			//$window.location = '../drives/compatible?page=' + $scope.nextPage;
		}
	};

	// Get Count of Number of Drives for Pagination
	$scope.getItemCount = function () {
		$http.get('/api/drives-compat/' + $routeParams.cat).
			success(function(datas) {
				$scope.totalItems = datas[0].Result_Count;
				$scope.pageSize = 8;
				$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
				for(var i = 0; i<$scope.numPages; i++) {
					$scope.content.push(i);
				}

				if($scope.currentPage >= 3) {
					$scope.showCurrent = false;
					$scope.hideCurrent = false;
				} else {
					$scope.showCurrent = true;
					$scope.hideCurrent = true;
				}				
			});

	};

	// Get blood drive information
	$scope.getDrives = function() {

		$scope.getItemCount();

		$http.get('/api/drives-compat/' + $routeParams.page).
				success(function(data) {
      		$scope.drives = data;
					$scope.empty = false;
					$scope.limit = 8;

					if(($routeParams.page != null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}

					if($scope.drives == "") {
						$scope.text = "No New Compatible Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getDrives();
		}

    // Get Compatible Blood Types For List
    $http.get('/api/blood-compat/').
    success(function(data) {
      $scope.bloods = data[0];
      $scope.lists = data[1];
    });
	}]);