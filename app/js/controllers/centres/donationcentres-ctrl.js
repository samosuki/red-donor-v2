/******************************************************************************************************************
 Donation Centres Controller
 - Display donation centre data with filter by country
******************************************************************************************************************/

reddonor.controller('CentresCtrl', ['$scope', '$http', '$location', '$routeParams', 'countryListData', 'ngProgress',
	function($scope, $http, $location, $routeParams, countryListData, ngProgress) {
		
		ngProgress.start();

		// Get Locations Info (ALL)
		$http.get('/api/centres/').
			success(function(data) {
				$scope.locations = data;
				$scope.filteredLocations = data;
				ngProgress.complete();
		});

		$scope.countries = countryListData.getCountries();
		$scope.countries.then(function (countries) {
			$scope.countries = countries;
		});


		$scope.chosenCountry = "All";

		//console.log("countries")
		//console.log($scope.countries);

		$scope.$watch('centreFilter',function(){
		$scope.filteredLocations = [];
		angular.forEach($scope.locations, function(val, key){
			if(val.location_country == $scope.centreFilter){
				this.push(val);
				$scope.chosenCountry = val.country_name;
				/*console.log("Val");
				console.log(val);
				console.log("Val Country ID");
				console.log(val.location_id);
				console.log("Centre Filter");*/
			}
			else if($scope.centreFilter === undefined) {
				//console.log("centre undefined");
				this.push(val);
			}
		},$scope.filteredLocations);
	});
}]);