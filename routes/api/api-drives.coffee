###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore")
moment = require("moment")
bcrypt = require("bcrypt") # Encryption
async = require("async") # Async Functions
#time = require("time") # Time Zone Setup
nodemailer = require("nodemailer") # Email
#database = require(__dirname + "/database")
database = require(path.join(__dirname, "../database"))
apidrive = require("./api-drives") # API-Drives
apiauth = require("./api-auth") # Authentication API
authform = require("../auth/auth-forms") # Authentication Forms
authdrive = require("../auth/auth-drives") # Authentication Drives
#connection = database.getConnection()
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')


###******************************************************************************************************************
GET ENDING SOON OF BLOOD DRIVES [ENDING SOON PAGE]
Usage: i. Displays Blood drives ending soon
******************************************************************************************************************###
exports.drivesending = (req, res, callback) ->
  connection = database.getConnection()
  currentpage = undefined
  if req.params.page
    currentpage = req.params.page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    q = "SELECT(SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND DATEDIFF(d.`drive_enddate`, NOW()) < 7) AND d.drive_country=c.country_id ORDER BY d.`drive_enddate`) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, d.`pledge_count` AS 'Pledge_Count', b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND DATEDIFF(d.`drive_enddate`, NOW()) < 7) AND d.drive_country=c.country_id ORDER BY d.`drive_enddate` LIMIT ?,?;"
    connection.connect()
    connection.query q, [startindex, itemsperpage], (err, results) ->
      if err
        console.log err
        callback null
      else
        console.log "--- Ending Soon of Drives Loaded (Paged) ---"
        #res.json results
        callback results
    connection.end()
  else
    currentpage = 1
    # returns all blood drives where ending date is 7 days (1 week) less than current date
    q2 = "SELECT(SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND DATEDIFF(d.`drive_enddate`, NOW()) < 7) AND d.drive_country=c.country_id ORDER BY d.`drive_enddate`) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, d.`pledge_count` AS 'Pledge_Count', b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND DATEDIFF(d.`drive_enddate`, NOW()) < 7) AND d.drive_country=c.country_id ORDER BY d.`drive_enddate` LIMIT 0,8;"
    connection.connect()
    connection.query q2, (err, results) ->
      if err
        console.log err
        callback null
      else
        console.log "--- Ending Soon of Drives Loaded ---"
        #res.json results
        callback results
    connection.end()


###******************************************************************************************************************
GET DRIVES USES [CREATE DRIVE PAGE]
Usage: Populates dropdown list for user to select the reason (use) behind the blood drive.
******************************************************************************************************************###
exports.uses = (req, res) ->
  connection = database.getConnection()
  # returns all in list of drives uses (add more uses to this list or an other)
  q = "SELECT `uses_id`, `uses_name` FROM `drive_uses`"
  connection.connect()
  connection.query q, (err, results) ->
    if err
      console.log err
    else
      res.send results
    connection.end()

###******************************************************************************************************************
GET ALL ACTIVE BLOOD DRIVES [EXPLORE DRIVES PAGE]
Usage: i. Displays All the active blood drives
******************************************************************************************************************###
exports.drivesactive = (req, res) ->
  connection = database.getConnection()
  currentpage = undefined
  if req.params.page
    currentpage = req.params.page
    # limit to 8 drives per page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    # returns all active blood drives where ending date is more than current date
    q = "SELECT (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', c.`country_name` AS 'Drive_Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT ?, ?;"
    connection.connect()
    connection.query q, [
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
        console.log "this1"
        console.log itemsperpage
        console.log currentpage
        console.log req.params.page
        console.log startindex
      else
        console.log "--- All Active Drives Loaded (Paged) ---"
        res.json results
    connection.end()
  else
    currentpage = 1
    # returns all active blood drives where ending day is more than current date
    q = "SELECT (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', c.`country_name` AS 'Drive_Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT 0, 8;"
    connection.connect()
    connection.query q, (err, results) ->
      if err
        console.log err
        console.log "this2"
      else
        console.log "--- All Active Drives Loaded ---"
        res.json results
    connection.end()

###******************************************************************************************************************
GET ALL NON ACTIVE BLOOD DRIVES [EXPLORE DRIVES ENDED PAGE]
Usage: i. Displays All the non active blood drives
******************************************************************************************************************###
exports.drivesnonactive = (req, res) ->
  connection = database.getConnection()
  currentpage = undefined
  if req.params.page
    currentpage = req.params.page
    # limit to 8 drives per page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    # returns all active blood drives where ending date is more than current date
    q = "SELECT(SELECT COUNT(*) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id ORDER BY d.`date_created`) AS 'Result_Count', d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, d.`pledge_count` AS 'Pledge_Count', u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT ?, ?;"
    connection.connect()
    connection.query q, [
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
      else
        console.log "--- Drives Ended Loaded (Paged) ---"
        res.json results
      connection.end()
  else
    currentpage = 1
    # returns all active blood drives where ending day is more than current date
    q = "SELECT(SELECT COUNT(*) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id ORDER BY d.`date_created`) AS 'Result_Count', d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, d.`pledge_count` AS 'Pledge_Count', u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) < 1 AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT 0,8;"
    connection.connect()
    connection.query q, (err, results) ->
      if err
        console.log err
      else
        console.log "--- Drives Completed Loaded ---"
        res.json results
      connection.end()

###******************************************************************************************************************
GET COMPATIBLE BLOOD DRIVES [COMPATIBLE DRIVES PAGE]
Usage: i. Displays all the Compatible blood drives
******************************************************************************************************************###
exports.drivescompat = (req, res) ->
  connection = database.getConnection()
  # returns all compatible blood drives using the user bloodtype and country from the session variable
  userbloodtype = req.session.user.user_bloodtype
  usercountry = req.session.user.user_country
  currentpage = undefined
  compat = undefined
  # if a page parameter exists and sets current page to that value
  if req.params.page
    currentpage = req.params.page
    # limit to 8 drives per page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    compat = "b.`o-pos-compat`"  if userbloodtype is 1
    compat = "b.`o-neg-compat`"  if userbloodtype is 2
    compat = "b.`a-pos-compat`"  if userbloodtype is 3
    compat = "b.`a-neg-compat`"  if userbloodtype is 4
    compat = "b.`b-pos-compat`"  if userbloodtype is 5
    compat = "b.`b-neg-compat`"  if userbloodtype is 6
    compat = "b.`ab-pos-compat`"  if userbloodtype is 7
    compat = "b.`ab-neg-compat`"  if userbloodtype is 8

    # returns all compatible blood drives using the user bloodtype and country from the session variable
    q = "SELECT (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND d.drive_country=c.country_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND " + compat + " = 1 AND d.`drive_country` = ?) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, d.`pledge_count` AS 'Pledge_Count', DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, c.`country_name` AS Country, c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND d.drive_country=c.country_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND " + compat + " = 1 AND d.`drive_country` = ? LIMIT ?,?"
    connection.connect()
    connection.query q, [
      usercountry
      usercountry
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
      else
        console.log "--- Compatible Drives Loaded (Paged) ---"
        #console.log results
        res.json results
      connection.end()
  else
    currentpage = 1
    compat = "b.`o-pos-compat`"  if userbloodtype is 1
    compat = "b.`o-neg-compat`"  if userbloodtype is 2
    compat = "b.`a-pos-compat`"  if userbloodtype is 3
    compat = "b.`a-neg-compat`"  if userbloodtype is 4
    compat = "b.`b-pos-compat`"  if userbloodtype is 5
    compat = "b.`b-neg-compat`"  if userbloodtype is 6
    compat = "b.`ab-pos-compat`"  if userbloodtype is 7
    compat = "b.`ab-neg-compat`"  if userbloodtype is 8
    q = "SELECT d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, d.`pledge_count` AS 'Pledge_Count', DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, c.`country_name` AS Country, c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND d.drive_country=c.country_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND " + compat + "  = 1 AND d.`drive_country` = ? LIMIT 0,8"
    connection.connect()
    connection.query q, [usercountry], (err, results) ->
      if err
        console.log err
      else
        console.log "--- Compatible Drives Loaded ---"
        res.json results
      connection.end()

###******************************************************************************************************************
GET BLOOD DRIVE DETAIL INFO [DRIVE DETAILS PAGE]
Usage: i. Gets the Date Difference between the Ending Date and Current Date
      ii. Displays Compatible Blood Types for Selected Blood Drive
******************************************************************************************************************###
exports.drivedetailinfo = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  async.parallel [

    # Usage: Gets the Date Difference between the Ending Date and Current Date

    (callback) ->

      # returns date difference between ending date and current date
      did = req.params.did
      q = "SELECT DATEDIFF(d.`drive_enddate`, NOW()) AS 'Date_Diff' FROM drive d WHERE d.`drive_id` = ?;"
      connection.query q, [did], (err, results) ->
        if err
          console.log err
        else
          # if the date difference is less than 0 display
          if results[0].Date_Diff <= 0
            results[0].Date_Diff = 0
            callback null, results
          else
            #console.log(results);
            callback null, results

    # Usage: Displays Compatible Blood Types for Selected Blood Drive

    (callback) ->

      # get blood type id from drive id parameter
      driveID = req.params.did
      q = "SELECT d.`drive_bloodtype` FROM drive d WHERE d.`drive_id` = ?;"
      connection.query q, [driveID], (err, results) ->
        if err
          console.log err
        else
          drivebloodtype = results[0].drive_bloodtype
          # get compatible blood types from bloodtype id for drive
          if drivebloodtype
            compat = "b.`o-pos-receive`"  if drivebloodtype is 1
            compat = "b.`o-neg-receive`"  if drivebloodtype is 2
            compat = "b.`a-pos-receive`"  if drivebloodtype is 3
            compat = "b.`a-neg-receive`"  if drivebloodtype is 4
            compat = "b.`b-pos-receive`"  if drivebloodtype is 5
            compat = "b.`b-neg-receive`"  if drivebloodtype is 6
            compat = "b.`ab-pos-receive`"  if drivebloodtype is 7
            compat = "b.`ab-neg-receive`"  if drivebloodtype is 8
            compat = "b.`ab-pos-receive`"  if drivebloodtype is 9
            q = "SELECT b.`bloodtype_name` FROM bloodtype b WHERE " + compat + "  = 1"
            connection.query q, (err, results) ->
              if err
                console.log err
              else
                console.log "--- Compatible Blood Types Loaded ---"
                callback null, results
  ], (err, results) ->
    res.json results
    connection.end()


###******************************************************************************************************************
GET DRIVE DETAIL [DRIVE DETAIL PAGE]
Usage: Gets the Date Difference between the Ending Date and Current Date
******************************************************************************************************************###
exports.drivedetail = (req, res) ->
  connection = database.getConnection()
  did = req.params.did

  # returns organisation id if it exists for selected drive
  q1 = "SELECT d.created_by_org FROM drive d WHERE d.drive_id = ?"
  connection.query q1, [did], (err, results) ->
    console.log err  if err

    # if an org id exists return drive details with organisation data
    if (results[0].created_by_org) isnt null
      orgCreated = results[0].created_by_org
      q2 = "SELECT (SELECT COUNT(users_id) FROM pledge_users p, drive d WHERE p.drive_id=d.drive_id AND d.drive_id = ?) AS Pledge_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, b.`bloodtype_url`, l.`location_name`, l.`location_address`, l.`location_tel`, l.`location_hours`, c.`country_name`, c.`country_url`, o.org_name, o.org_id, o.org_title, o.org_description FROM drive d, users u, bloodtype b, country c, location l, org o WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND d.`drive_id` = ? AND d.drive_country=c.country_id AND l.location_country=c.country_id AND d.created_by_org=o.org_id = ?;"
      console.log "Org Exists"
      connection.query q2, [
        did
        did
        orgCreated
      ], (err, results) ->
        if err
          console.log err
        else
          console.log "--- Detail of Drive Loaded (with Org)---"
          #console.log results
          #res.json _.first(results)
          res.json results
      connection.end()
    else
      # if an org id exists return drive details without organisation data
      console.log "No Org Exists"
      q3 = "SELECT (SELECT COUNT(users_id) FROM pledge_users p, drive d WHERE p.drive_id=d.drive_id AND d.drive_id = ?) AS Pledge_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, b.`bloodtype_url`, l.`location_name`, l.`location_address`, l.`location_tel`, l.`location_hours`, c.`country_name`, c.`country_url` FROM drive d, users u, bloodtype b, country c, location l WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND d.`drive_id` = ? AND d.drive_country=c.country_id AND l.location_country=c.country_id;"
      connection.query q3, [
        did
        did
      ], (err, results) ->
        if err
          console.log err
        else
          console.log "--- Detail of Drive Loaded ---"
          #res.json _.first(results)
          res.json results
      connection.end()
  #connection.end()

###******************************************************************************************************************
GET BLOOD DRIVES BY BLOODTYPE [DRIVE BY BLOODTYPE PAGE]
Usage: i. Displays Blood Drives By Bloodtype
******************************************************************************************************************###
exports.drivesbybloodtype = (req, res) ->
  connection = database.getConnection()
  currentpage = undefined
  cat = undefined
  # if a page parameter exists and sets current page to that value
  if req.params.page
    currentpage = req.params.cat
    cat = req.params.cat
    # limit to 8 drives per page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    # returns blood drives by bloodtype using the bloodtype url
    q = "SELECT (SELECT `bloodtype_name` FROM `bloodtype` WHERE `bloodtype_url` = ?) AS 'bloodtype_name', (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND b.`bloodtype_url`= ? AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id) AS 'Result_Count', d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, d.`pledge_count` AS 'Pledge_Count', u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND b.`bloodtype_url`= ? AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id LIMIT ?,?;"
    connection.connect()
    connection.query q, [
      cat
      cat
      cat
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
        connection.end()
      else
        console.log "--- Categories of Drives Loaded (Paged) ---"
        res.json results
    connection.end()
  else
    cat = req.params.cat
    currentpage = 1
    # returns all active blood drives where ending day is more than current date
    q = "SELECT (SELECT `bloodtype_name` FROM `bloodtype` WHERE `bloodtype_url` = ?) AS 'bloodtype_name', (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND b.`bloodtype_url`= ? AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id) AS 'Result_Count', d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, d.`pledge_count` AS 'Pledge_Count', u.`user_name` AS Created_By, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND b.`bloodtype_url`= ? AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id LIMIT 0,8;"
    connection.connect()
    connection.query q, [
      cat
      cat
      cat
    ], (err, results) ->
      if err
        console.log err
        connection.end()
      else
        console.log "--- Categories of Drives Loaded ---"
        res.json results
    connection.end()

###******************************************************************************************************************
GET COUNT OF BLOOD DRIVES BY BLOODTYPE BREADCRUMB [DRIVE BY BLOODTYPE PAGE]
Usage: i. Gets the Count of Total Blood Drives by Blood Type & Blood Type Name for the Breadcrumb
******************************************************************************************************************###
exports.drivesbybloodtypecountcrumb = (req, res) ->
  connection = database.getConnection()
  # returns the count of drives by bloodtype and the bloodtype name breadcrumb
  cat = req.params.cat
  q = "SELECT (SELECT COUNT(d.`drive_id`) AS Result_Count FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND b.`bloodtype_url`= ? AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id) AS 'Result_Count', (SELECT `bloodtype_name` FROM `bloodtype` WHERE `bloodtype_url` = ?) AS 'bloodtype_name'"
  connection.connect()
  connection.query q, [
    cat
    cat
  ], (err, results) ->
    if err
      console.log err
      connection.end()
    else
      console.log "--- Bloodtype Drive Count & Bloodtype Breadcrumb ---"
      res.json results
  connection.end()


###******************************************************************************************************************
GET MOST RECENT BLOOD DRIVES [MOST RECENT PAGE]
Usage: i. Displays most recent Blood drives
******************************************************************************************************************###
exports.drivesrecent = (req, res, callback) ->
  connection = database.getConnection()
  currentpage = undefined
  if req.params.page
    currentpage = req.params.page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    q1 = "SELECT (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`date_created`, NOW()) > -42) AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0) AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', d.`date_created`, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`date_created`, NOW()) > -42) AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0) AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT ?,?;"
    connection.connect()
    connection.query q1, [
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
        callback null
      else
        console.log "--- Most Recent of Drives Loaded (Paged) ---"
        #console.log results
        callback results
    connection.end()
  else
    currentpage = 1
    q2 = "SELECT (SELECT COUNT(d.`drive_id`) FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`date_created`, NOW()) > -42) AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0) AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC) AS Result_Count, d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', d.`date_created`, c.`country_name` AS 'Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND (DATEDIFF(d.`date_created`, NOW()) > -42) AND (DATEDIFF(d.`drive_enddate`, NOW()) > 0) AND d.drive_country=c.country_id ORDER BY d.`date_created` DESC LIMIT 0,8;"
    connection.connect()
    connection.query q2, (err, results) ->
      if err
        console.log err
        callback null
      else
        console.log "--- Most Recent of Drives Loaded ---"
        callback results
    connection.end()

###******************************************************************************************************************
GET BLOOD DRIVES BY LOCATION [DRIVES BY LOCATION PAGE]
Usage: i. Displays most recent Blood drives
******************************************************************************************************************###
exports.drivesbylocation = (req, res) ->
  connection = database.getConnection()
  loc = req.params.loc
  currentpage = undefined
  if req.params.page
    currentpage = req.params.page
    itemsperpage = 8
    startindex = (currentpage - 1) * itemsperpage
    q1 = "SELECT d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', c.`country_name` AS 'Drive_Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id AND c.`country_url` = ? ORDER BY d.`date_created` DESC LIMIT ?,?;"
    connection.connect()
    connection.query q1, [
      loc
      startindex
      itemsperpage
    ], (err, results) ->
      if err
        console.log err
      else
        console.log "--- Drives By Location Loaded (Paged) ---"
        res.json results
      connection.end()
  else
    currentpage = 1
    q2 = "SELECT d.`drive_id` AS Drive_ID, d.`drive_name` AS Drive_For, d.drive_title, d.`drive_description` AS Drive_Description, b.`bloodtype_name` AS BloodType_Needed, DATEDIFF(d.`drive_enddate`, NOW()) AS Days_Left, d.`donations_needed` AS Pints_Needed, u.`user_name` AS Created_By, d.`pledge_count` AS 'Pledge_Count', c.`country_name` AS 'Drive_Country', c.`country_url` FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id AND c.`country_url` = ? ORDER BY d.`date_created` DESC LIMIT 0,8;"
    connection.connect()
    connection.query q2, [loc], (err, results) ->
      if err
        console.log err
      else
        console.log "--- Drives By Location Loaded ---"
        res.send results
      connection.end()

###******************************************************************************************************************
GET BLOOD DRIVES BY LOCATION BREADCRUMB [DRIVE BY LOCATION PAGE]
Usage: i. Gets the Count of Total Blood Drives by Location & Country Name for the Breadcrumb
******************************************************************************************************************###
exports.drivesbylocationcountcrumb = (req, res) ->
  connection = database.getConnection()
  # returns the count of drives by location and the country name breadcrumb
  loc = req.params.loc
  q = "SELECT (SELECT COUNT(d.`drive_id`) AS Result_Count FROM drive d, users u, bloodtype b, country c WHERE d.created_by_user=u.users_id AND d.drive_bloodtype=b.bloodtype_id AND DATEDIFF(d.`drive_enddate`, NOW()) > 0 AND d.drive_country=c.country_id AND c.`country_url` = ?) AS `Result_Count`, (SELECT `country_name` FROM `country` WHERE `country_url` = ?) AS 'country_name';"
  connection.connect()
  connection.query q, [
    loc
    loc
  ], (err, results) ->
    if err
      console.log err
    else
      console.log "--- Location Drive Count & Country Name Breadcrumb ---"
      res.json results
      connection.end()


###***********************************************************
Retrieve Drive Information For Edit
***********************************************************###
exports.driveedit = (req, res) ->
  connection = database.getConnection()
  userID = req.session.user.users_id
  driveID = req.params.did
  q = undefined
  q1 = undefined
  params = [
    userID
    driveID
  ]
  # returns UserID if drive was started by signed in user
  q1 = "SELECT d.`created_by_user` FROM drive d WHERE d.`created_by_user` = ? AND d.`drive_id` = ?;"
  connection.query q1, params, (err, results) ->
    if err
      console.log err
    # if a UserID exists return drive information
    if results.length > 0
      q = "SELECT d.`drive_id`, d.`drive_name`, d.`drive_description`, d.`donations_needed`, d.`drive_bloodtype`, d.`drive_country`, d.`drive_enddate`, d.`drive_use` FROM drive d WHERE d.`created_by_user` = ? AND d.`drive_id` = ?;"
      connection.query q, params, (err, results) ->
        if err
          console.log err
        else
          console.log "--- Drive Info Loaded ---"
          res.json _.first(results)
      connection.end()
    # if UserID does not exist send to unauthorised page
    else
      console.log "-- not authorised to view --"
      res.send notauth: "Not Auth"
      connection.end()

###***********************************************************
Update Drive Info
***********************************************************###
exports.savedrive = (req, res) ->
  connection = database.getConnection()
  userID = req.session.user.users_id
  driveID = req.params.did
  drive_name = req.body.drive_name
  drive_description = req.body.drive_description
  donations_needed = req.body.donations_needed
  drive_bloodtype = req.body.drive_bloodtype
  drive_use = req.body.drive_use
  drive_country = req.body.drive_country
  drive_enddate = req.body.enddate
  deadline = new Date(drive_enddate)
  q = undefined
  params = undefined
  console.log(drive_enddate)
  if(drive_enddate != undefined)
    q = "UPDATE drive SET drive_name = ?, drive_description = ?, donations_needed = ?, drive_bloodtype = ?, drive_country = ?, drive_enddate = ?, drive_use = ? WHERE created_by_user = ? AND drive_id = ?;"
    params = [
      drive_name
      drive_description
      donations_needed
      drive_bloodtype
      drive_country
      deadline
      drive_use
      userID
      driveID
    ]
    console.log "End Date Exists"
    connection.connect()
    connection.query q, params, (err, results) ->
      if err
        console.log err
      else
        res.send req.body
        console.log "--- when is not undefined ---"
    connection.end()
  else if (drive_enddate == undefined)
    q = "UPDATE drive SET drive_name = ?, drive_description = ?, donations_needed = ?, drive_bloodtype = ?, drive_country = ? WHERE created_by_user = ? AND drive_id = ?;"
    params = [
      drive_name
      drive_description
      donations_needed
      drive_bloodtype
      drive_country
      userID
      driveID
    ]
    connection.connect()
    connection.query q, params, (err, results) ->
      if err
        console.log err
      else
        res.send req.body
        console.log "--- when is undefined ---"
    connection.end()
