/******************************************************************************************************************
 Drive Details Controller 
 - Display blood drive details, blood type compatibility, pledge/unpledge buttons, share buttons, flash message
******************************************************************************************************************/
reddonor.controller('DriveReadCtrl', ['$scope', '$http', '$timeout', '$routeParams', '$location', '$window', 'toaster',
	function($scope, $http, $timeout, $routeParams, $location, $window, toaster) {

	$scope.poperr = function() {
		toaster.pop('error', "", $scope.error, 5000, true);
	};

	$scope.popsuccess = function() {
		toaster.pop('success', "", $scope.success, 5000, true);
	};

	$scope.notlogged = false;
	$scope.orgexist = false;

	$scope.tooltip = {
		"title": "Click to Unpledge Donation"
	};
	$scope.fbshare = {
		"title": "Share to Facebook"
	};
	$scope.twitshare = {
		"title": "Share to Twitter"
	};

	$scope.button = {
		"active": false
	};

	$scope.drivelist = function() {
		$http.get('/api/drivelist/' + $routeParams.did).
		success(function(data) {
			if(data.org_name) {
				$scope.orgexist = true;
			}
			$scope.drives = data[0];
			$scope.page.setTitle($scope.drives.Drive_For + " Blood Request");
			$scope.ended = false;
		}).
		error(function(data) {
			$scope.data = data || "Request failed";
		});
	};

	$scope.drivelist();

		$http.get('/api/blood-info/' + $routeParams.did).
		success(function(data) {
				$scope.datediff = data[0][0];
				$scope.bloods = data[1];
		});

		// Get Pledge Button Info
		$http.get('/api/drivecheck/' + $routeParams.did).
		success(function(data) {
			if(data.driveended) {
				$scope.drivestatus = 'Drive Ended';
				$scope.ended = true;
				$scope.unpledge = false;
			}
			if(data.pledgedalready) {
				$scope.drivestatus = 'Donation Pledged';
				$scope.ended = false;
				$scope.unpledge = true;
				$scope.actionPledge = 'onSubmitUnPledgeDonation';
			}
			if(data.safe) {
				$scope.drivestatus = 'Pledge Donation';
				$scope.ended = false;
				$scope.unpledge = false;
				$scope.actionPledge = 'onSubmitPledgeDonation';
			}
		});

		// Get Locations Info
		$http.get('/api/locations/' + $routeParams.did).
		success(function(data) {
			$scope.locations = data;
		});

		$scope.onSubmitUnPledgeDonation = function() {
			$http.post('/api/unpledge-donation/' + $routeParams.did, $routeParams).
				success(function(data) {
					$scope.drivestatus = 'Pledge Donation';
					$scope.ended = false;
					$scope.unpledge = false;
					$scope.actionPledge = 'onSubmitPledgeDonation';
					$scope.success = 'Donation Unpledged';
					$scope.drivelist();
					$scope.popsuccess();
			});
		};

		$scope.onSubmitPledgeDonation = function(location) {

			

			$http.post('/api/pledge-donation/' + $routeParams.did + '/' + location).
					success(function(data) {

						if(data.nosession) {
							$scope.notlogged = true;
							console.log("please log in");
							$scope.error = "Please <a href='/login'> Login </a><span> or </span><a href='/signup'> Sign Up </a>";
							$scope.poperr();
						}
						else if(data.proceed) {
							$scope.error = "";
							$scope.drivestatus = 'Donation Pledged';
							$scope.ended = false;
							$scope.unpledge = true;
							$scope.actionPledge = 'onSubmitUnPledgeDonation';
							$scope.success = 'Donation Pledged';
							//$scope.drivelist();
							$timeout(function() {
								$scope.drivelist();
							}, 500);
							$scope.popsuccess();
						}
						else if(data[0].toorecent) {
							$scope.error = "Sorry, you donated blood less than 3 months ago.";
							$scope.poperr();
						}
						else if(data[1].wrongLocation) {
							$scope.error = "Sorry, You must live in the same country to donate.";
							$scope.poperr();
						}
						}).error(function() {
						
					});
				};
	}]);