/******************************************************************************************************************
 Navigation Menu Directive
 - Get username, donation centre locations, countries and logout
******************************************************************************************************************/

	reddonor.directive('onenav', ['$http', '$location', function ($http, $location) {
		var navOriginal;
		navOriginal = function(scope, element, attrs) {

			scope.init = function () {
				$http.get('/api/check-logged/').
				success(function(data) {
					//console.log(data);
					if(data.loggedin) {
						//scope.loggedin = true;
						scope.loggedin = "logged";
						scope.getUsername();
						scope.getLocations();
					}
					if(data.notloggedin) {
						//scope.loggedin = false;
						scope.loggedin = "notlogged";
					}
				});
			};

			scope.init();

			scope.onlogOut = function() {
				$http.get('/session/logout').
					success(function() {
					//console.log("Logout clicked!");
					//window.location = "/login";
					window.location = "/";
				}).error(function() {
					//console.log("Did not work?");
				});
			};

			scope.getUsername = function() {
				$http.get('/api/profile-info/').
				success(function(data) {
					scope.profiles = data;
					scope.username = data[0].user_name;
				});
			};

			scope.getLocations = function() {
				$http.get('/api/centre-locations/').
					success(function(data) {
						scope.userlocations = data;
						//console.log(data);
						return;
					});
			};

			scope.onlogOut = function() {
        $http.get('/session/logout').
          success(function() {
          //console.log("Logout clicked!");
          //window.location = "/login";
          window.location = "/";
				}).error(function() {
					//console.log("Did not work?");
				});
      };

			$http.get('/api/countries/').
				success(function(data) {
					scope.country = data;
					//console.log(data);
					return;
				});
			};

		return {
			restrict: 'E',
			replace: true,
			templateUrl: '/partials/not-loggedin-menu.jade',
			link: navOriginal

		};
	}]);