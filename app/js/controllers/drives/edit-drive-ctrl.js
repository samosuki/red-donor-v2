/******************************************************************************************************************
 Edit Drive Controller 
 - Get drive detail data, set date options, check errors and save drive details
******************************************************************************************************************/
reddonor.controller('EditDriveCtrl', ['$scope', '$http', '$location', '$routeParams',
	function($scope, $http, $location, $routeParams) {

    $scope.editabledrive = false;
    $scope.notauth = false;

    $http.get('/api/countries/').
        success(function(data) {
          $scope.country = data;
          //console.log(data);
          return;
      });
      $http.get('/api/bloodtype/').
        success(function(data) {
          $scope.bloodtype = data;
          //console.log(data);
          return;
      });

      // Get Uses List
      $http.get('/api/uses/').
        success(function(data) {
          $scope.uses = data;
          //console.log(data);
          return;
      });

		  $http.get('/api/edit-drive/' + $routeParams.did).
        success(function(data, status) {
          if(data.notauth) {
            //$location.url('/notauth');
            $scope.notauth = true;
            console.log("not auth");
          } else {
            $scope.recipient = data;
            $scope.status = status;
            $scope.editabledrive = true;
          }
      });
        
        
        $scope.onSubmitEditDrive = function() {
        $http.put('/api/save-drive/' + $routeParams.did, $scope.recipient).
          success(function(data) {
            if(data.error) {
              $scope.error = "Sorry something went wrong :(";
              //window.location = "/login";
            } else {
              $scope.error = "";
                //window.location = "/";
              $location.url('/drive/' + $routeParams.did + '/');
            }
					});
       };
	}]);