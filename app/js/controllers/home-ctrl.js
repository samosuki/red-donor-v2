/******************************************************************************************************************
 Welcome Page Controller
******************************************************************************************************************/
reddonor.controller('WelcomeCtrl', ['$scope', '$http', '$location',
	function($scope, $http, $location) {

		$scope.page.setTitle("Connecting Blood Donors");

		$http.get('/api/drives-welcome/').
			success(function(data) {
				$scope.drivesend = data[0];
				$scope.drivesrecent = data[1];
				$scope.alsoempty = false;
				$scope.limit = 4;

				if($scope.drivesend == "") {
					$scope.textend = "No New Ending Soon Blood Drives Found";
					$scope.alsoempty = true;
				}
				if($scope.drivesrecent == "") {
					$scope.emptydrive = true;
					$scope.textrecent = "No New Recent Blood Drives Found";
				}
			});
	}]);