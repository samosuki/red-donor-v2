###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore")
bcrypt = require("bcrypt") # Encryption
async = require("async") # Async Functions
#time = require("time") # Time Zone Setup
moment = require("moment")
nodemailer = require("nodemailer") # Email
database = require(path.join(__dirname, "../database"))
apiappt = require("./api-appointments") # Appointments API
apiauth = require("./api-auth") # Authentication API
apiblood = require("./api-blood") # Blood API
apicentres = require("./api-centres") # Donation Centres API
apicharts = require("./api-charts") # Charts API
apicountry = require("./api-country") # Country API
apidrives = require("./api-drives") # Drives API
apilocs = require("./api-locations") # Locations API
apiorgs = require("./api-organisations") # Organisations API
apiprofile = require("./api-profiles") # Profiles API
authform = require("../auth/auth-forms") # Authentication Forms
authdrive = require("../auth/auth-drives") # Authentication Drives
#connection = database.getConnection()
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')

###***********************************************************
Welcome (Home) Page
***********************************************************###
exports.welcome = (req, res) ->
  async.series [

  	(callback) ->
  		apidrives.drivesending req, res, (results) ->
  			callback null, results

 		(callback) ->
  		apidrives.drivesrecent req, res, (results) ->
  			callback null, results

  ], (err, results) ->
  	res.send results

###***********************************************************
Drives Most Recent Page
***********************************************************###

exports.recent = (req, res) ->
  async.series [

 		(callback) ->
  		apidrives.drivesrecent req, res, (results) ->
  			callback null, results

  ], (err, results) ->
  	res.send results

###***********************************************************
Drives Ending Soon Page
***********************************************************###

exports.ending = (req, res) ->
  async.series [

 		(callback) ->
  		apidrives.drivesending req, res, (results) ->
  			callback null, results

  ], (err, results) ->
  	res.send results
