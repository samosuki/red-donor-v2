/******************************************************************************************************************
 Charts Directive
 - Display Charts
******************************************************************************************************************/

reddonor.directive('chart', function(){
    return {
        scope:{
            'title': '@',  // what the the '@' mean or do?
            'data': '=',
            'type': '@',
            'number': '@'
        },
        restrict: "EA",
        link: function(scope, element, attrs){

            scope.$watch('data', function(a){
                if(scope.data[0]){
                    if(a == []) console.log("Watch " + scope.data == [], 'data');

                    var id = "chart" + Math.floor((Math.random()*1000)+1);
                    element[0].id = id;

                    var chart = new google.visualization.ChartWrapper();

                    chart.setContainerId(id);
                    chart.setOptions({
                        'title': attrs.title || "No Title",
                        'width':attrs.width || 400,
                        'height': attrs.height || 300,
                        'backgroundColor': { fill:'transparent' }
                      });

                    //chart.setChartType('BarChart');

                    scope.$watch('type', function(a){
                        chart.setChartType(a || 'BarChart');
                        chart.draw();
                    });

                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Causes');
                    data.addColumn('number', attrs.value);
                    //console.log(scope.number);
                    data.addRows(scope.data);
                    chart.setDataTable(data);
                    chart.draw();
                }
            }, true);
        }
    };
  });