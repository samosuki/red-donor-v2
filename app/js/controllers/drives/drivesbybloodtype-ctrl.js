/******************************************************************************************************************
 Drives By Blood Type Controller (Paged)
 - Display drives by blood type, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('DriveCatPagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window', 
	function($scope, $http, $routeParams, $location, $window) {
	$scope.drives = [];
	$scope.content = [];
	$scope.empty = true;
	$scope.emptydrive = false;
	$scope.currentPage = 0;
	$scope.pageSize = 8;

	// Get Count of Number of Drives for Pagination
	$scope.getItemCount = function () {
		$http.get('/api/drives-count-crumb/' + $routeParams.cat).
			success(function(datas) {
				//$scope.totalItems = datas.drivecount;
				$scope.totalItems = datas[0].Result_Count;
				$scope.breadcrumb = datas[0].bloodtype_name;
				$scope.pageSize = 8;
				$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
				for(var i = 0; i<$scope.numPages; i++) {
					$scope.content.push(i);
				}

				if($scope.currentPage >= 3) {
					$scope.showCurrent = false;
					$scope.hideCurrent = false;
				} else {
					$scope.showCurrent = true;
					$scope.hideCurrent = true;
				}
			});

	};
	// Get Previous Drive Completed Page
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$location.url('/drives/' + $routeParams.cat + '?page=' + $scope.currentPage);
			//$window.location = '../drives/' + $routeParams.cat + '?page=' + $scope.currentPage;
		}
	};
	// Get Next Drive Completed Page
	$scope.nextPage = function () {
		if ($scope.currentPage <  $scope.numPages-1) {
			$scope.nextPage = $scope.currentPage + 2;
			$location.url('../drives/' + $routeParams.cat + '?page=' + $scope.nextPage);
			//$window.location = '../drives/' + $routeParams.cat + '?page=' + $scope.nextPage;
		}
	};

	// Get blood drive information
	$scope.getDrives = function() {
		$http.get('/api/drives/' + $routeParams.cat).
				success(function(data) {
					$scope.drives = data;
					$scope.empty = false;
					$scope.limit = 8;
					$scope.blood = $routeParams.cat;
	
					if(($routeParams.page != null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}
					if($scope.drives == "") {
						$scope.text = "No New Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
					
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			$scope.getItemCount();
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getItemCount();
			$scope.getDrives();
		}
}]);