/* -----------------------------------------------------------------
	App.js: contains routing setup
------------------------------------------------------------------- */

var reddonor = angular.module('reddonor', ['ngRoute','ngProgress','ngSanitize','ngAnimate','toaster','ui.mask','$strap.directives']);

reddonor.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
					$routeProvider
						.when('/', { title: 'You Can Save A Life', templateUrl: '/partials/home', controller: 'WelcomeCtrl' })
						.when('/login', { title: 'Login', templateUrl: '/partials/login', controller: 'LoginCtrl' })
						.when('/about', { title: 'About Us', templateUrl: '/partials/aboutreddonor' })
						.when('/howitworks', { title: 'How It Works', templateUrl: '/partials/howitworks' })
						.when('/faqs', { title: 'FAQs', templateUrl: '/partials/faqs' })
						.when('/termsofuse', { title: 'Terms of Use', templateUrl: '/partials/terms' })
						.when('/privacy', { title: 'Privacy Policy', templateUrl: '/partials/privacy' })
						.when('/contact', { title: 'Contact Us', templateUrl: '/partials/contact', controller: 'ContactCtrl' })
						.when('/signup', { title: 'Sign Up', templateUrl: '/partials/signup', controller: 'SignUpCtrl' })
						.when('/drive/create', { title: 'Create Blood Drive', templateUrl: '/sessions/drive-add', controller: 'AddDriveCtrl' })
						.when('/create', { title: 'Create Blood Drive', templateUrl: '/partials/drive-add', controller: 'CreateDriveCtrl' })
						.when('/guestsignup', { title: 'Sign Up', templateUrl: '/partials/guest-signup', controller: 'GuestSignUpCtrl' })
						.when('/drive/settings/:did', { title: 'Edit Blood Drive', templateUrl: '/sessions/drive-edit', controller: 'EditDriveCtrl' })
						.when('/drives/list', { templateUrl: '/partials/drive-list', controller: 'ListDrivePagesCtrl' })
						.when('/drive/:did/:title', {  templateUrl: '/partials/drive-detail', controller: 'DriveReadCtrl' })
						.when('/drives/soon-ending', { title: 'Soon Ending Drives', templateUrl: '/partials/drive-soon', controller: 'SoonEndPagesCtrl' })
						.when('/drives/country/:loc', { title: 'Discover Drives', templateUrl: '/partials/drive-location', controller: 'DriveLocPagesCtrl' })
						.when('/drives/most-recent', { title: 'Most Recent Drives', templateUrl: '/partials/drive-most-recent', controller: 'MostRecentPagesCtrl' })
						.when('/drives/compatible', { title: 'Compatible Drives', templateUrl: '/sessions/drive-list-compat', controller: 'CompatPagesCtrl' })
						.when('/donors/signup', { title: 'Blood Donor Signup', templateUrl: '/partials/list-signup', controller: 'DonorSignUpCtrl' })
						.when('/drives/drive-completed', { title: 'Completed Drives', templateUrl: '/partials/drive-ended', controller: 'EndDrivePagesCtrl' })
						.when('/drives/:cat', { templateUrl: '/partials/drive-list-type', controller: 'DriveCatPagesCtrl' })
						.when('/drives/:page', { title: 'Discover Drives', templateUrl: '/partials/drive-list-type', controller: 'DriveCatPagesCtrl' })
						.when('/org/:oid/:title', { title: 'Organisation', templateUrl: '/partials/org-profile', controller: 'OrgProfilePages' })
						.when('/centres/:bid/:title', { title: 'Blood Banks', templateUrl: '/partials/bloodbank-profile', controller: 'BloodBankPages' })
						.when('/profile/history', { title: 'Donation History', templateUrl: '/sessions/donation-history', controller: 'HistoryCtrl' })
						.when('/profile/drives', { title: 'My Drives', templateUrl: '/sessions/my-drives', controller: 'MyDrivesCtrl' })
						.when('/profile/appointments', { title: 'My Appointments', templateUrl: '/sessions/appointment-history', controller: 'ApptsCtrl' })
						.when('/profile/settings', { title: 'Settings', templateUrl: '/sessions/settings', controller: 'ProfileSettingsCtrl' })
						.when('/profile', { title: 'Profile', templateUrl: '/sessions/profile', controller: 'ProfileCtrl' })
						.when('/reset-password/:token', {  title: 'Reset Password', templateUrl: '/partials/reset-password', controller: 'ResetCtrl' })
						.when('/reset-password', { title: 'Reset Password', templateUrl: '/partials/reset-password', controller: 'ResetCtrl' })
						.when('/centre/list', { title: 'Donation Centres', templateUrl: '/partials/donation-centres', controller: 'CentresCtrl' })
						.when('/centre/:loc', { title: 'Donation Centres', templateUrl: '/partials/donation-centre-country', controller: 'CentresCountryCtrl' })

						.otherwise({redirectTo:'/'});
					$locationProvider.html5Mode({
						enabled: true,
						requireBase: false
					});
}]);

/* ---------- Dynamic Title ---------- */
/* takes the title from the route scope to bind it to root scope to use as the page title */

reddonor.run(['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll) {
	$rootScope.page = {
		setTitle: function(title) {
			this.title = title + ' | Red Donor';
		}
	};
	$rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
		$rootScope.page.setTitle(current.$$route.title || 'Red Donor');
		//$location.hash('body');
		$anchorScroll();
	});
}]);
