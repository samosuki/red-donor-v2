 /******************************************************************************************************************
 Blood Bank Controller
 - 
******************************************************************************************************************/
reddonor.controller('BloodBankPages', ['$scope', '$http', '$location', '$routeParams', 'toaster',
	function($scope, $http, $location, $routeParams, toaster) {

/**********************************************************
	Appointment Scheduling Modal
**********************************************************/

		$scope.steps = ['one','two'];
		$scope.step = 0;
		$scope.maxappt = 4;
		$scope.apptfull = false;
		///$scope.schedule = {};
		$scope.schedule;		
		//$scope.schedule.apptSession;
		//$scope.schedule.apptStatus = 7;
		//$scope.schedule.apptType = 1;

		$scope.bloodtypes = [
		{ id: 1, name:"O+" },
		{ id: 2, name:"O-" },
		{ id: 3, name:"A+" },
		{ id: 4, name:"A-" },
		{ id: 5, name:"B+" },
		{ id: 6, name:"B-" },
		{ id: 7, name:"AB+"},
		{ id: 8, name:"AB-"},
		{ id: 9, name:"Unknown"}
		];

		$scope.times = [
			{ time_value: '1', time_name: 'Early Morning' },
			{ time_value: '2', time_name: 'Mid Morning' },
			{ time_value: '3', time_name: 'Early Afternoon' },
			{ time_value: '4', time_name: 'Late Afternoon'}
		];

		$scope.isCurrentStep = function(step) {
			return $scope.step === step;
		};
		$scope.setCurrentStep = function(step) {
			$scope.step = step;
		};
		$scope.getCurrentStep = function() {
			return $scope.steps[$scope.step];
		};
		$scope.isFirstStep = function() {
		return $scope.step === 0;
	};
	$scope.isLastStep = function() {
		return $scope.step === ($scope.steps.length - 1);
	};

	$scope.getNextLabel = function() {
		return ($scope.isLastStep()) ? 'Confirm' : 'Next';
	};

	$scope.handlePrevious = function() {
		$scope.step -= ($scope.isFirstStep()) ? 0 : 1;
	};

	$scope.handleNext = function(dismiss) {
		if($scope.isLastStep()) {
			$scope.makeAppt();
			dismiss();
			$scope.handlePrevious();
		} else {
			$scope.step += 1;
		}
	};

	$scope.makeAppt = function() {
		$scope.schedule.apptTime = $scope.apptTime;
		$http.post('/api/add-appointment/'  + $routeParams.bid, $scope.schedule).
			success(function(data) {
				 console.log('data sent');
			});
			$scope.schedule.apptDate = "";
			$scope.appt = "";
			$scope.checkSchedule();

	};

	$http.get('/api/profile-info/').
		success(function(data) {
		$scope.profiles = data;
		//console.log(data);
	});

	$scope.getAppts = function() {
		//console.log($scope.schedule.apptDate);
		$scope.apptmsg = false;

		$routeParams.date = $scope.schedule.apptDate;
		//console.log("routes " + $routeParams.date);

		$http.post('/api/appointments/'  + $routeParams.bid, $scope.schedule).
			success(function(data) {
			
			if(data.dategone) {
				console.log("The date has passed!");
				$scope.appt = "";
				$scope.apptend = true;
				$scope.apptmsg = true;
				$scope.appterror = "Sorry, that date has already passed.";
			} else {
				$scope.appt = data;
				console.log(data);
				$scope.apptend = false;
				$scope.apptmsg = true;
				($scope.maxappt === $scope.appt.time1) ? $scope.apptfull1 = true : $scope.apptfull1 = false;
				($scope.maxappt === $scope.appt.time2) ? $scope.apptfull2 = true : $scope.apptfull2 = false;
				($scope.maxappt === $scope.appt.time3) ? $scope.apptfull3 = true : $scope.apptfull3 = false;
				($scope.maxappt === $scope.appt.time4) ? $scope.apptfull4 = true : $scope.apptfull4 = false;
			}

			
					
		});	

		console.log($scope.schedule);

	}

	$scope.selectSession = function(time) {
		$scope.apptTime = time;
		if(time === 1)
			$scope.schedule.apptSession = "early morning";
		if(time === 2)
			$scope.schedule.apptSession = "mid morning";
		if(time === 3)
			$scope.schedule.apptSession = "early afternoon";
		if(time === 4)
			$scope.schedule.apptSession = "late afternoon";

		$scope.handleNext();
	}

	$scope.tooltip = {
			"title": "Select Session"
		};

		$scope.popup = function() {
			return Flash.pop({
				'title': $scope.error,
				'type': "error"
			});
		};

		$scope.scheduledtooltip = {
			"title": "You have donation scheduled! </br> Click for your next appointment."
		};

		$scope.checksignin = function() {
			$scope.error = "Please <a href='/login'> Login </a><span> or </span><a href='/signup'> Sign Up </a>";
		$scope.popup();
		};

		$scope.checkSchedule = function() {
		// Get Schedule Donation Button Info
		
			$http.get('/api/schedule-check').
				success(function(data) {
					console.log(data);
					if(data.nosession) {
						$scope.donatestatus = "Donate Blood";
						$scope.notsigned = true;
						$scope.scheduleaction = false;
						$scope.scheduled = false;
					}
					if(data[0].apptexists) {
						$scope.donatestatus = "Blood Donation Scheduled";
						$scope.scheduled = true;
						$scope.scheduleaction = false;
					}
					if(data[0].apptfree) {
						$scope.donatestatus = "Donate Blood";
						$scope.scheduleaction = true;
						$scope.scheduled = false;
					}

			});
		};


/**********************************************************
	Blood Bank Location
**********************************************************/
		
		//$scope.bvalue = "13.092685,-59.607477";
		//$scope.bbloc = new google.maps.LatLng(16,-70);

		$scope.init = function () {
			$http.get('/api/bloodbank-profile/' + $routeParams.bid).
		success(function(data) {
			$scope.bankprofile = data;
			//addMarker(data);
			$scope.page.setTitle($scope.bankprofile[0].location_name + ' > ' + $scope.bankprofile[0].country_name);
			//$scope.bbloc = new google.maps.LatLng(data[0].location_lat, data[0].location_lng);
			//console.log(data);
			//console.log(data[0].location_lat);
			$scope.checkSchedule();
		});
		};
		
		$scope.init();
/*
	var addMarker = function(object){
			var lat = object[0].location_lat;
			var lng = object[0].location_lng;
			//console.log(lat);
			//console.log(lng);
			var centre = new google.maps.LatLng(lat, lng);
			var pos = new google.maps.LatLng(lat, lng);
			var marker = new google.maps.Marker({
				position: pos,
				title: object[0].location_name,
				map: $scope.myMap
			 
			});
				$scope.myMarkers = [marker, ];
				$scope.bbloc = new google.maps.LatLng(lat, lng);
		
		};

	 
	$scope.mapOptions = {
			center: $scope.bbloc,
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	$scope.openMarkerInfo = function(marker) {
		$scope.currentMarker = marker;
		$scope.currentMarkerLat = marker.getPosition().lat();
		$scope.currentMarkerLng = marker.getPosition().lng();
		//$scope.myInfoWindow.open($scope.myMap, marker);
		};

	

		//Markers should be added after map is loaded
	/*	$scope.onMapIdle = function() {
		if ($scope.myMarkers === undefined){    
			var marker = new google.maps.Marker({
				map: $scope.myMap,
				position: $scope.bbloc,
			});
			$scope.myMarkers = [marker, ];
		}
		}; */


/**********************************************************
	Blood Bank Blood Levels
**********************************************************/

	$scope.opos = "";
	$scope.oneg = "";
	$scope.apos = "";
	$scope.aneg = "";
	$scope.bpos = "";
	$scope.bneg = "";
	$scope.abpos = "";
	$scope.abneg = "";
	$scope.opostext = "";
	$scope.onegtext = "";
	$scope.apostext = "";
	$scope.anegtext = "";
	$scope.bpostext = "";
	$scope.bnegtext = "";
	$scope.abpostext = "";
	$scope.abnegtext = "";
	
	$scope.statuses = ['danger','warning','info','success'];

	$http.get('/api/bloodbank-levels/' + $routeParams.bid).
	success(function(data) {
		$scope.banklevels = data;
			if ($scope.banklevels[0].oposlevel < 25 ) {
				$scope.opos = 'danger';
				$scope.opostext = "Critical";
			}
			else if ($scope.banklevels[0].oposlevel < 50 ) {
				$scope.opos = 'warning';
				$scope.opostext = "Low";
			}
			else if ($scope.banklevels[0].oposlevel < 75 ) {
				$scope.opos = 'info';
				$scope.opostext = "Moderate";
			}
			else {
				$scope.opos = 'success';
				$scope.opostext = "High";
			}

			if ($scope.banklevels[0].oneglevel < 25 ) {
				$scope.oneg = 'danger';
				$scope.onegtext = "Critical";
			}
			else if ($scope.banklevels[0].oneglevel < 50 ) {
				$scope.oneg = 'warning';
				$scope.onegtext = "Low";
			}
			else if ($scope.banklevels[0].oneglevel < 75 ) {
				$scope.oneg = 'info';
				$scope.onegtext = "Moderate";
			}
			else {
				$scope.oneg = 'success';
				$scope.onegtext = "High";
			}

			if ($scope.banklevels[0].aposlevel < 25 ) {
				$scope.apos = 'danger';
				$scope.apostext = "Critical";
			}
			else if ($scope.banklevels[0].aposlevel < 50 ) {
				$scope.apos = 'warning';
				$scope.apostext = "Low";
			}
			else if ($scope.banklevels[0].aposlevel < 75 ) {
				$scope.apos = 'info';
				$scope.apostext = "Moderate";
			}
			else {
				$scope.apos = 'success';
				$scope.apostext = "High";
			}

			if ($scope.banklevels[0].aneglevel < 25 ) {
				$scope.aneg = 'danger';
				$scope.anegtext = "Critical";
			}
			else if ($scope.banklevels[0].aneglevel < 50 ) {
				$scope.aneg = 'warning';
				$scope.anegtext = "Low";
			}
			else if ($scope.banklevels[0].aneglevel < 75 ) {
				$scope.aneg = 'info';
				$scope.anegtext = "Moderate";
			}
			else {
				$scope.aneg = 'success';
				$scope.anegtext = "High";
			}

			if ($scope.banklevels[0].bposlevel < 25 ) {
				$scope.bpos = 'danger';
				$scope.bpostext = "Critical";
			}
			else if ($scope.banklevels[0].bposlevel < 50 ) {
				$scope.bpos = 'warning';
				$scope.bpostext = "Low";
			}
			else if ($scope.banklevels[0].bposlevel < 75 ) {
				$scope.bpos = 'info';
				$scope.bpostext = "Moderate";
			}
			else {
				$scope.bpos = 'success';
				$scope.bpostext = "High";
			}

			if ($scope.banklevels[0].bneglevel < 25 ) {
				$scope.bneg = 'danger';
				$scope.bnegtext = "Critical";
			}
			else if ($scope.banklevels[0].bneglevel < 50 ) {
				$scope.bneg = 'warning';
				$scope.bnegtext = "Low";
			}
			else if ($scope.banklevels[0].bneglevel < 75 ) {
				$scope.bneg = 'info';
				$scope.bnegtext = "Moderate";
			}
			else {
				$scope.bneg = 'success';
				$scope.bnegtext = "High";
			}

			if ($scope.banklevels[0].abposlevel < 25 ) {
				$scope.abpos = 'danger';
				$scope.abpostext = "Critical";
			}
			else if ($scope.banklevels[0].abposlevel < 50 ) {
				$scope.abpos = 'warning';
				$scope.abpostext = "Low";
			}
			else if ($scope.banklevels[0].abposlevel < 75 ) {
				$scope.abpos = 'info';
				$scope.abpostext = "Moderate";
			}
			else {
				$scope.abpos = 'success';
				$scope.abpostext = "High";
			}

			if ($scope.banklevels[0].abneglevel < 25 ) {
				$scope.abneg = 'danger';
				$scope.abnegtext = "Critical";
			}
			else if ($scope.banklevels[0].abneglevel < 50 ) {
				$scope.abneg = 'warning';
				$scope.abnegtext = "Low";
			}
			else if ($scope.banklevels[0].abneglevel < 75 ) {
				$scope.abneg = 'info';
				$scope.abnegtext = "Moderate";
			}
			else {
				$scope.abneg = 'success';
				$scope.abnegtext = "High";
			}
	 });

 /**********************************************************
	Blood Bank Charts

	- Display charts for gender, bloodtype, causes and 
		blood levels
**********************************************************/

	$scope.cdata = [];

	$http.get('/api/loc-gender-chart/' + $routeParams.bid).
	success(function(cdata) {
		$scope.barData = cdata;
			for(var i=0; i<$scope.barData.length; i++) {
				//console.log($scope.barData[i]);
			for(var prop in $scope.barData[i]) {
			//console.log(prop);
				$scope.cdata.push([prop, +$scope.barData[i][prop]]);
			} 
			}
			//console.log($scope.cdata);
	 });

	$scope.ddata = [];

	$http.get('/api/loc-bloodtype-chart/' + $routeParams.bid).
	success(function(ddata) {
		$scope.chartData = ddata;
			for(var i=0; i<$scope.chartData.length; i++) {
				//console.log($scope.chartData[i]);
			for(var prop in $scope.chartData[i]) {
			//console.log(prop);
				$scope.ddata.push([prop, +$scope.chartData[i][prop]]);
			} 
			}
			//console.log($scope.ddata);
	 });

	$scope.edata = [];

	$http.get('/api/loc-causes-chart/' + $routeParams.bid).
	success(function(edata) {
		$scope.chartData = edata;
			for(var i=0; i<$scope.chartData.length; i++) {
				//console.log($scope.chartData[i]);
			for(var prop in $scope.chartData[i]) {
			//console.log(prop);
				$scope.edata.push([prop, +$scope.chartData[i][prop]]);
			} 
			}
			//console.log($scope.edata);
	 }); 

	$scope.fdata = [];

	$http.get('/api/loc-levels-chart/' + $routeParams.bid).
	success(function(fdata) {
		$scope.chartData = fdata;
			for(var i=0; i<$scope.chartData.length; i++) {
				//console.log($scope.chartData[i]);
			for(var prop in $scope.chartData[i]) {
			//console.log(prop);
				$scope.fdata.push([prop, +$scope.chartData[i][prop]]);
			} 
			}
			//console.log($scope.fdata);
	 }); 

	}]);