/******************************************************************************************************************
 Log In Form Directive
 - Performs log in
******************************************************************************************************************/


  reddonor.directive('loginform', ['$http', '$location', '$window', 'toaster', function ($http, $location, $window, toaster) {
    var loginformFn;
    loginformFn = function(scope, element, attrs) {

      scope.loginerror = false;

      scope.poperr = function() {
        toaster.pop('error', "", scope.error, 5000, true);
      };

      scope.popsuccess = function() {
        toaster.pop('success', "", scope.success, 5000, true);
      };

      scope.onSubmitLogin = function() {
        $http.post('/api/login/', scope.user).
          success(function(data) {
            if(data.noemail) {
              scope.error = "Email Address Was Not Found";
              scope.incorrect = "Email address was not found";
              scope.poperr();
            }
            else if(data.incorrect) {
              scope.error = "Incorrect Password Entered";
              scope.incorrect = "Incorrect password enetered";
              scope.poperr();
            } else {
              scope.incorrect = "";
              scope.success = 'Login Successful!';
              scope.popsuccess();
              //window.history.back();
              window.location = "/";
              //$location.path('/profile');
            }
      })
      .error(function(data, status, headers, config) {
          //console.log("Data for login POST encountered an error.");
            window.location = "/";
        });
      };
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/login-form.jade',
      link: loginformFn

    }
  }]);