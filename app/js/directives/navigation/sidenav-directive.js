
reddonor.directive('sidenav', ['$http', function ($http) {
      var bloodtypeFn;
      bloodtypeFn = function (scope, element, attrs) {

        scope.bloodtype = [
          { bloodtype_url: "o-pos", bloodtype_name:"O+" },
          { bloodtype_url: "o-neg", bloodtype_name:"O-" },
          { bloodtype_url: "a-pos", bloodtype_name:"A+" },
          { bloodtype_url: "a-neg", bloodtype_name:"A-" },
          { bloodtype_url: "b-pos", bloodtype_name:"B+" },
          { bloodtype_url: "b-neg", bloodtype_name:"B-" },
          { bloodtype_url: "ab-pos", bloodtype_name:"AB+"},
          { bloodtype_url: "ab-neg", bloodtype_name:"AB-"}
        ];
      };
    return {
        restrict: 'E',
        replace: true,
        //scope: {},
        templateUrl: '/partials/bloodtype-menu.jade',
        link: bloodtypeFn
        };
    }]);