###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
connection = database.getConnection()


###***********************************************************
CHECK FOR USER DONATION & ALREADY SIGN IN [BLOOD DONATION PAGE (BUTTON)]
Usage: i. Check to see if user has already scheduled an appointment
      ii. Check to see if user is signed in
***********************************************************###
exports.schedulescheck = (req, res) ->
  connection = database.getConnection()
  # ii. if user session exists (i.e. signed in)
  if req.session.user
    apptUser = req.session.user.users_id    
    # i. check to see if donation already scheduled
    async.series [(callback) ->
      q = "SELECT appt_id FROM appointments a WHERE a.appt_user = ? AND a.appt_status = 7;"
      params = [apptUser]
      connection.connect()
      connection.query q, params, (err, results) ->
        console.log err  if err
        if results.length > 0
          console.log results
          console.log "--- Appointment Exists ---"
          results = (apptexists: "apptexists")
          callback null, results
        else
          console.log results
          console.log "--- Appointment Free ---"
          results = (apptfree: "apptfree")
          callback null, results
    ], (err, results) ->
      res.send results
      console.log results
      connection.end()
  else
    res.send nosession: "No Session"
    connection.end()


###***********************************************************
SCHEDULE COUNT DONATION APPOINTMENT [BLOOD DONATION PAGE (MODAL)]
Usage: i. Count of scheduled appointments for chosen date
      ii. returns the number of appointments scheduled for a chosen date
***********************************************************###
exports.getapptschedules = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  apptdate = req.body.apptDate
  appLocation = req.params.loc
  appStatus = req.body.apptStatus #scheduled
  dateNow = new Date()
  async.waterfall [
    (callback) ->
      
      # i. converts the queried apppointment date and the current date to a readable format and compare
      q = "SELECT (SELECT DATE_FORMAT(?,'%Y-%m-%d')) AS appt_date, (SELECT DATE_FORMAT(?,'%Y-%m-%d')) AS date_now"
      params = [
        apptdate
        dateNow
      ]      
      connection.query q, params, (err, results) ->
        if err
          console.log err
        else
          apptdate = results[0].appt_date
          dateNow = results[0].date_now
          if apptdate < dateNow
            console.log "living in the past!"            
            #res.send({ dategone: "dategone" })
            #dateGone = results[0].dategone = 0
            apptdate = 0            
            #console.log(results)
            callback null, apptdate
          else
            console.log "living in the future!"
            callback null, apptdate

    (apptdate, callback) ->
      
      # ii. returns the number of appointments scheduled for a chosen date
      if apptdate isnt 0
        q = "SELECT (SELECT COUNT(*) FROM appointments a WHERE a.appt_date = ? AND a.appt_status = ? AND a.appt_location = ? AND a.appt_time = 1) AS time1, (SELECT COUNT(*) FROM appointments a WHERE a.appt_date = ? AND a.appt_status = ? AND a.appt_location = ? AND a.appt_time = 2) AS time2, (SELECT COUNT(*) FROM appointments a WHERE a.appt_date = ? AND a.appt_status = ? AND a.appt_location = ? AND a.appt_time = 3) AS time3, (SELECT COUNT(*) FROM appointments a WHERE a.appt_date = ? AND a.appt_status = ? AND a.appt_location = ? AND a.appt_time = 4) AS time4"
        params = [
          apptdate
          appStatus
          appLocation
          apptdate
          appStatus
          appLocation
          apptdate
          appStatus
          appLocation
          apptdate
          appStatus
          appLocation
        ]
        connection.query q, params, (err, results) ->
          if err
            console.log err
          else
            console.log "--- Count of Scheduled Appointments ---"
            #console.log results
            
            #res.json(results);
            callback null, results

      
      # iii. if date has passed return error message
      else
        console.log "--- Date Has Passed ---"
        results = (dategone: "dategone")
        callback null, results
  ], (err, results) ->
    res.json results
    #console.log results
    connection.end()



###***********************************************************
SCHEDULE DONATION APPOINTMENT [BLOOD DONATION PAGE]
Usage: i. Add scheduled appointments for chosen date
***********************************************************###
exports.addscheduleappt = (req, res) ->
  connection = database.getConnection()
  apptDate = req.body.apptDate
  apptLocation = req.params.loc
  apptTime = req.body.apptTime
  apptType = req.body.apptType
  apptStatus = req.body.apptStatus
  apptCreated = new Date()
  apptUser = req.session.user.users_id
  q = "INSERT INTO appointments SET appt_date = ?, appt_time = ?, appt_user = ?, appt_location = ?, appt_type = ?, appt_status = ?, appt_created = ?"
  params = [
    apptDate
    apptTime
    apptUser
    apptLocation
    apptType
    apptStatus
    apptCreated
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      #console.log results
      console.log "--- New Appointment Added ---"
    connection.end()