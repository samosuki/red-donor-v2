/******************************************************************************************************************
 Sign Up Fom Directive
 - Gets countries, bloodtypes, gender, add user
******************************************************************************************************************/

reddonor.directive('signupform', ['$http', '$location', 'toaster', function ($http, $location, toaster) {
		var signupformFn;
		signupformFn = function(scope, element, attrs) {

			scope.poperr = function() {
				toaster.pop('error', "", scope.error, 5000, true);
			};

			scope.popsuccess = function() {
				toaster.pop('success', "", scope.success, 5000, true);
			};

			scope.onSubmitSignup = function() {
				$http.post('/api/signup/', scope.user).
					success(function(data) {
						if(data.eerror) {
							scope.eerror = true;
							scope.aerror = false;
							scope.error = "Email address matches an existing account";
							scope.poperr();
						}
						else if(data.aerror) {
							scope.aerror = true;
							scope.eerror = false;
							scope.error = "You must be over 12 years old to sign up";
						} else {
							scope.error = "";
							window.location = "/";
						}
				});
			};
			
			$http.get('/api/countries').
				success(function(data) {
					scope.country = data;
					return;
		});
			scope.bloodtypes = [
				{ id: 1, name:"O+" },
				{ id: 2, name:"O-" },
				{ id: 3, name:"A+" },
				{ id: 4, name:"A-" },
				{ id: 5, name:"B+" },
				{ id: 6, name:"B-" },
				{ id: 7, name:"AB+"},
				{ id: 8, name:"AB-"},
				{ id: 9, name:"Unknown"}

			];

			scope.gender = [
				{ id: 1, name:"Male" },
				{ id: 2, name:"Female" }
			];
		};

		return {
			restrict: 'E',
			replace: true,
			templateUrl: '/partials/signup-form.jade',
			link: signupformFn

		};
	}]);