 /******************************************************************************************************************
 Donation Centres By Country Controller 
 - Get Donation Centre Info By Country
******************************************************************************************************************/

reddonor.controller('CentresCountryCtrl', ['$scope', '$http', '$location', '$routeParams', 'ngProgress',
	function($scope, $http, $location, $routeParams, ngProgress) {

		ngProgress.start();

		// Get Locations Info by country
		$http.get('/api/centres/' + $routeParams.loc).
		success(function(data) {
			$scope.locations = data;
			$scope.chosenCountry = data[0].country_name;
			ngProgress.complete();
		});
}]);