###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
authform = require("../auth/auth-forms") # Authentication Forms
authdrive = require("../auth/auth-drives") # Authentication Drives
#connection = database.getConnection()


###***********************************************************
USER LOGIN & AUTHETICATION
Usage: calls authenticateLogin function and if user returns add data to new session
***********************************************************###
exports.userlogin = (req, res) ->
  connection = database.getConnection()
  #connection.connect()
  authform.existingLoginEmail req.body.loginEmail, (taken) ->
    if taken
      authform.authenticateLogin req.body.loginEmail, req.body.loginPassword, (user) ->
        if user
          sess = req.session
          req.session.user = user
          req.session.userid = user.users_id
          console.log "Login and Session success!"
          console.log req.session.user.user_name
          #console.log(sess);
          #console.log(req.cookies);
          #console.log(req.sessionID); 
          #console.log(req.user);  
          #connection.end()
          res.redirect "/"
        else
          res.send incorrect: "incorrect"
          console.log "Wrong password"
    else
      res.send noemail: "No Email Exists"
      console.log "Email doesn't exist"
  connection.end()


###***********************************************************
USER SIGN UP & AUTHENTICATION
Usage:  calls existingEmail function to check if email address exists already then authenticateSignup function and if user returns add data to new session
***********************************************************###
exports.usersignup = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  authform.existingEmail req.body.signupEmail, (taken) ->
    if taken
      res.send eerror: "Email Taken..."
      console.log "Email Taken..."
      #connection.end()
    else
      authdrive.checkAge req.body.signupDOB, (notofage) ->
        if notofage
          res.send aerror: "Not of Age"
          console.log "Not of Age"
        else
          authform.authenticateSignup req.body.signupName, req.body.signupEmail, req.body.signupPassword, req.body.signupbloodTypes, req.body.signupGender, req.body.signupDOB, req.body.signupLocation, (user) ->
            if user
              sess = req.session
              req.session.user = user
              req.session.userid = user.users_id
              #connection.end()
              console.log "Signup and Session success!"
              console.log sess.user.user_name
              #console.log "----- " + req.body.signupbloodTypes
              console.log "Donor Signup Email Sent"
              res.redirect "/"
            else
              console.log "User was not found..."
  connection.end()

###***********************************************************
GUEST USER SIGN UP & AUTHENTICATION
Usage:  calls authenticateSignup function and if user returns add data to new session
***********************************************************###
exports.guestusersignup = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  authdrive.checkAge req.body.signupDOB, (notofage) ->
    if notofage
      res.send aerror: "Not of Age"
      console.log "Not of Age"
    else
      authform.authenticateGuestSignup req.body.signupName, req.body.signupEmail, req.body.signupPassword, req.body.signupbloodTypes, req.body.signupGender, req.body.signupDOB, req.body.signupLocation, (user) ->
        if user
          sess = req.session
          req.session.user = user
          req.session.userid = user.users_id
          #connection.end()
          console.log "Signup and Session success!"
          console.log sess.user.user_name
          #console.log "----- " + req.body.signupbloodTypes
          console.log "Donor Signup Email Sent"
          res.redirect "/"
        else
          console.log "User was not found..."
  connection.end()

###***********************************************************
DONOR SIGN UP & AUTHENTICATION
Usage: calls existingEmail function to check if email address exists already then authenticateDonorSignup function
***********************************************************###
exports.donorsignup = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  auth.existingEmail req.body.signupEmail, (taken) ->
    if taken
      res.send error: "Email Taken..."
      console.log "Email Taken..."
      connection.end()
    else
      auth.authenticateDonorSignup req.body.signupName, req.body.signupEmail, req.body.signupTel, req.body.signupPassword, req.body.signupbloodTypes, req.body.signupGender, req.body.signupLocation, (user) ->
        if user
          res.send success: "Success..."
          console.log "Signup of Donor Success!"
          console.log "----- " + req.body.signupbloodTypes
          console.log "Donor Signup Email Sent"
          connection.end()   
        else
          console.log "User was not created"
        connection.end()

###***********************************************************
CREATE NEW BLOOD DRIVE
Usage:  calls checkDate function to check if the deadline date has already passed, if not, call autheticateDrive to create a new blood drive
***********************************************************###
exports.createblooddrive = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  authdrive.checkDate req.body.deadlineDate, (passed) ->
    if passed
      res.send aerror: "Date Passed..."
      console.log "Date Passed..."
      #connection.end()
    else
      console.log "Date Has Not Passed... go to autheticate drive"
      userCreated = req.session.user.users_id
      orgCreated = req.session.user.user_org      
      # returns success if blood drive was successfully created and added to database
      authdrive.authenticateDrive req.body.recipientName, req.body.driveSummary, req.body.donationNeeded, req.body.bloodTypes, req.body.deadlineDate, req.body.driveCountry, req.body.driveCentre, userCreated, req.body.driveUse, orgCreated, (success) ->
        if success
          console.log "New Blood Drive Added!"
          res.send success: "New Drive Added"
          #connection.end()   
        else
          console.log "Blood Drive Not Created!"
          res.send berror: "Blood Drive Not Created"
  connection.end()

###***********************************************************
CREATE NEW BLOOD DRIVE WITHOUT LOGIN
Usage:  calls existingEmail function to check if email address exists already, calls checkDate function to check if the deadline date has already passed, if not, call autheticateDrive to create a new blood drive
***********************************************************###
exports.createnewdrive = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  userID = undefined
  authform.existingEmail req.body.userEmail, (exists) ->
    if exists
      console.log "get userid from email"
      # get userid using email address
      userEmail = req.body.userEmail
      q = "SELECT users_id FROM users u WHERE u.`user_email` = ?;"
      params = [userEmail]
      connection = database.getConnection()
      connection.connect()
      connection.query q, params, (err, results) ->
        if err
          console.log err
          console.log "can't find user"
        else
          userCreated = results[0].users_id
          authdrive.checkDate req.body.deadlineDate, (passed) ->
            if passed
              res.send aerror: "Date Passed..."
              console.log "Date Passed..."
              #connection.end()
            else
              console.log "Date Has Not Passed... go to autheticate drive"
              #userCreated = req.session.user.users_id
              #orgCreated = req.session.user.user_org      
              #userCreated = userID
              orgCreated = null
              #console.log "userCreated"
              #console.log userCreated
              # returns success if blood drive was successfully created and added to database
              #authdrive.authenticateDrive req.body.recipientName, req.body.driveSummary, req.body.donationNeeded, req.body.bloodTypes, req.body.deadlineDate, req.body.driveCountry, userCreated, req.body.driveUse, orgCreated, (success) ->
              authdrive.authenticateDrive req.body.recipientName, req.body.driveSummary, req.body.donationNeeded, req.body.bloodTypes, req.body.deadlineDate, req.body.driveCountry, req.body.driveCentre, userCreated, req.body.driveUse, orgCreated, (success) ->
                if success
                  console.log "New Blood Drive Added!"
                  res.send success: "New Drive Added"
                  #connection.end()   
                else
                  console.log "Blood Drive Not Created!"
                  res.send berror: "Blood Drive Not Created"
      connection.end()
    else
      # create new user
      console.log "create new user"
      authform.guestSignup req.body.userEmail, (userid) ->
        if userid
          userID = userid      
          authdrive.checkDate req.body.deadlineDate, (passed) ->
            if passed
              res.send aerror: "Date Passed..."
              console.log "Date Passed..."
              #connection.end()
            else
              console.log "Date Has Not Passed... go to autheticate drive"
              #userCreated = req.session.user.users_id
              #orgCreated = req.session.user.user_org      
              userCreated = userID
              orgCreated = null
              # returns success if blood drive was successfully created and added to database
              authdrive.authenticateDrive req.body.recipientName, req.body.driveSummary, req.body.donationNeeded, req.body.bloodTypes, req.body.deadlineDate, req.body.driveCountry, req.body.driveCentre, userCreated, req.body.driveUse, orgCreated, (success) ->
              #authdrive.authenticateDrive req.body.recipientName, req.body.driveSummary, req.body.donationNeeded, req.body.bloodTypes, req.body.deadlineDate, req.body.driveCountry, userCreated, req.body.driveUse, orgCreated, (success) ->
                if success
                  console.log "New Blood Drive Added!"
                  res.send success: "New Drive Added"
                  #connection.end()   
                else
                  console.log "Blood Drive Not Created!"
                  res.send berror: "Blood Drive Not Created"
  connection.end()

###***********************************************************
SET UP PLEDGE BUTTON
Usage:  calls drivecheck function to set up the status of the pledge donation button
***********************************************************###
exports.drivecheck = (req, res) ->
  #connection = database.getConnection()
  #connection.connect()
  DriveID = req.params.did
  
  # Check if the Blood Drive Has Ended
  authdrive.checkHasAlreadyEnded DriveID, (ended) ->
    if ended      
      # If Drive has ended send error message to page
      res.send driveended: "Drive Ended"
      console.log "Drive Already Ended"
      #connection.end()
    else
      console.log "Drive is active..."
      #console.log req.session
      if req.session.user        
        authdrive.checkAlreadyPledgedThisDrive req.session.user.users_id, DriveID, (pledgedalready) ->
          if pledgedalready            
            # If Drive has already been pledged send error message to page
            console.log "User Already Pledged To This Drive"
            res.send pledgedalready: "Pledged Already"            
            #connection.end()
          else
            console.log "User Has Not Pledged To This Drive..."
            res.send safe: "Drive Active"
            #connection.end()
      else
        console.log "No User Signed In"
        res.send safe: "Drive Active" 
  #connection.end()   


###******************************************************************************************************************
PLEDGE DONATION TO DRIVE
Usage:  i. calls checkRecentDonate function to check if user recently donated
        ii. calls checkSameLocation function to check if user is in the same country
            if no errors are found pledge donation
******************************************************************************************************************###
exports.pledgetodrive = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  errors = 0 # count of errors
  DriveID = req.params.did # driveID
  BloodBankID = req.params.loc # BloodBankID  
  
  # if user session exists (i.e. signed in)
  if req.session.user
    userName = req.session.user.user_name
    UserID = req.session.user.users_id
    LocationID = req.session.user.user_country
    userbloodtype = req.session.user.user_bloodtype
    GenderID = req.session.user.user_gender
    OrgID = req.session.user.user_org
    async.series [
      
      # i. check if user has donated within the last 3 months
      (callback) ->
        authdrive.checkRecentDonate UserID, (notrecent) ->
          if notrecent is false
            console.log "User Donated Too Recently"
            results = (toorecent: "Too Recent")
            errors++
            callback null, results
          else
            results = false
            console.log "User Can Donate"
            callback null, results
      
      #ii. check if user is in the same country as blood drive
      (callback) ->
        authdrive.checkSameLocation DriveID, LocationID, (same) ->
          if same is true
            results = false
            console.log "User is in same location..."
            callback null, results
          else
            console.log "User is not in same location..."
            results = (wrongLocation: "Not In Country")
            errors++
            callback null, results
    ], (err, results) ->
      # Once no errors occur, add donation pledge
      if errors is 0        
        # Get Pledge Count for Current Drive
        authdrive.getPledgeCount req.params.did, (pledge_count) ->
          authdrive.pledgeDonation DriveID, UserID, BloodBankID, LocationID, GenderID, OrgID, pledge_count, userName  if pledge_count
          results = (proceed: "Proceed")
          res.send results
          #console.log results
          #connection.end()
      else
        #console.log results
        res.send results
        #connection.end()
  else
    res.send nosession: "No Session"
  connection.end()

###***********************************************************
UNPLEDGE DONATION
Usage:  calls getPledgeCount & unpledgeDonation functions to unpledge donation and substract from Pledge Count
***********************************************************###
exports.unpledeblooddonation = (req, res) ->
  connection = database.getConnection()
  connection.connect()
  UserID = req.session.user.users_id
  DriveID = req.params.did
  authdrive.getPledgeCount req.params.did, (pledge_count) ->
    authdrive.unpledgeDonation DriveID, UserID, pledge_count  if pledge_count
    res.redirect "."
  connection.end()


###***********************************************************
AUTHENTICATE USER TOKEN
Usage:  calls checkToken to check if the right token exists, calls checkTokenTime to check if token was created
within the past 2 hours
***********************************************************###
exports.authenticatetoken = (req, res) ->
  connection = database.getConnection()
  #connection.connect()
  # returns the time the token was created if user token exists
  authform.checkToken req.params.token, (tokenTime) ->
    if tokenTime      
      # returns userID if token was created within the last two horus
      authform.checkTokenTime tokenTime, (userID) ->
        if userID
          res.send success: "User Authenticated"
          console.log "User Authenticated"
          #connection.end()        
        # Have send success display form with ng-show
        else
          res.send expired: "Token is no longer valid... expired."
          console.log "Token is no longer valid... expired."
          #connection.end()
    else
      res.send error: "Token is no longer valid..."
      console.log "Token is no longer valid..."
  connection.end()


###***********************************************************
CHECK LOGGED IN MENU
Usage:  checks to see if user exists and if a session is active
***********************************************************###
exports.checkLoggedMenu = (req, res) ->
  connection = database.getConnection()
  q = "SELECT u.user_name FROM users u WHERE u.users_id = 1;"
  connection.connect()
  connection.query q, (err, results) ->
    if err
      console.log err
      connection.end()
    else
      if req.session.user
        console.log "user logged in - show menu"
        res.send loggedin: "logged"
      else
        console.log "user not logged in - hide menu"
        res.send notloggedin: "notlogged"
      connection.end()




