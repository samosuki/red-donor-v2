/*!
 * Bootstrap v3.0.2 by @fat and @mdo
 * Copyright 2013 Twitter, Inc.
 * Licensed under http://www.apache.org/licenses/LICENSE-2.0
 *
 * Designed and built with all the love in the world by @mdo and @fat.
 */

if("undefined"==typeof jQuery)throw new Error("Bootstrap requires jQuery");+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]}}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one(a.support.transition.end,function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b()})}(jQuery),+function(a){"use strict";var b='[data-dismiss="alert"]',c=function(c){a(c).on("click",b,this.close)};c.prototype.close=function(b){function c(){f.trigger("closed.bs.alert").remove()}var d=a(this),e=d.attr("data-target");e||(e=d.attr("href"),e=e&&e.replace(/.*(?=#[^\s]*$)/,""));var f=a(e);b&&b.preventDefault(),f.length||(f=d.hasClass("alert")?d:d.parent()),f.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one(a.support.transition.end,c).emulateTransitionEnd(150):c())};var d=a.fn.alert;a.fn.alert=function(b){return this.each(function(){var d=a(this),e=d.data("bs.alert");e||d.data("bs.alert",e=new c(this)),"string"==typeof b&&e[b].call(d)})},a.fn.alert.Constructor=c,a.fn.alert.noConflict=function(){return a.fn.alert=d,this},a(document).on("click.bs.alert.data-api",b,c.prototype.close)}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d)};b.DEFAULTS={loadingText:"loading..."},b.prototype.setState=function(a){var b="disabled",c=this.$element,d=c.is("input")?"val":"html",e=c.data();a+="Text",e.resetText||c.data("resetText",c[d]()),c[d](e[a]||this.options[a]),setTimeout(function(){"loadingText"==a?c.addClass(b).attr(b,b):c.removeClass(b).removeAttr(b)},0)},b.prototype.toggle=function(){var a=this.$element.closest('[data-toggle="buttons"]');if(a.length){var b=this.$element.find("input").prop("checked",!this.$element.hasClass("active")).trigger("change");"radio"===b.prop("type")&&a.find(".active").removeClass("active")}this.$element.toggleClass("active")};var c=a.fn.button;a.fn.button=function(c){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof c&&c;e||d.data("bs.button",e=new b(this,f)),"toggle"==c?e.toggle():c&&e.setState(c)})},a.fn.button.Constructor=b,a.fn.button.noConflict=function(){return a.fn.button=c,this},a(document).on("click.bs.button.data-api","[data-toggle^=button]",function(b){var c=a(b.target);c.hasClass("btn")||(c=c.closest(".btn")),c.button("toggle"),b.preventDefault()})}(jQuery),+function(a){"use strict";var b=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=this.sliding=this.interval=this.$active=this.$items=null,"hover"==this.options.pause&&this.$element.on("mouseenter",a.proxy(this.pause,this)).on("mouseleave",a.proxy(this.cycle,this))};b.DEFAULTS={interval:5e3,pause:"hover",wrap:!0},b.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},b.prototype.getActiveIndex=function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},b.prototype.to=function(b){var c=this,d=this.getActiveIndex();return b>this.$items.length-1||0>b?void 0:this.sliding?this.$element.one("slid",function(){c.to(b)}):d==b?this.pause().cycle():this.slide(b>d?"next":"prev",a(this.$items[b]))},b.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition.end&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},b.prototype.next=function(){return this.sliding?void 0:this.slide("next")},b.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},b.prototype.slide=function(b,c){var d=this.$element.find(".item.active"),e=c||d[b](),f=this.interval,g="next"==b?"left":"right",h="next"==b?"first":"last",i=this;if(!e.length){if(!this.options.wrap)return;e=this.$element.find(".item")[h]()}this.sliding=!0,f&&this.pause();var j=a.Event("slide.bs.carousel",{relatedTarget:e[0],direction:g});if(!e.hasClass("active")){if(this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid",function(){var b=a(i.$indicators.children()[i.getActiveIndex()]);b&&b.addClass("active")})),a.support.transition&&this.$element.hasClass("slide")){if(this.$element.trigger(j),j.isDefaultPrevented())return;e.addClass(b),e[0].offsetWidth,d.addClass(g),e.addClass(g),d.one(a.support.transition.end,function(){e.removeClass([b,g].join(" ")).addClass("active"),d.removeClass(["active",g].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger("slid")},0)}).emulateTransitionEnd(600)}else{if(this.$element.trigger(j),j.isDefaultPrevented())return;d.removeClass("active"),e.addClass("active"),this.sliding=!1,this.$element.trigger("slid")}return f&&this.cycle(),this}};var c=a.fn.carousel;a.fn.carousel=function(c){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c),g="string"==typeof c?c:f.slide;e||d.data("bs.carousel",e=new b(this,f)),"number"==typeof c?e.to(c):g?e[g]():f.interval&&e.pause().cycle()})},a.fn.carousel.Constructor=b,a.fn.carousel.noConflict=function(){return a.fn.carousel=c,this},a(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(b){var c,d=a(this),e=a(d.attr("data-target")||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"")),f=a.extend({},e.data(),d.data()),g=d.attr("data-slide-to");g&&(f.interval=!1),e.carousel(f),(g=d.attr("data-slide-to"))&&e.data("bs.carousel").to(g),b.preventDefault()}),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var b=a(this);b.carousel(b.data())})})}(jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.transitioning=null,this.options.parent&&(this.$parent=a(this.options.parent)),this.options.toggle&&this.toggle()};b.DEFAULTS={toggle:!0},b.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},b.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b=a.Event("show.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.$parent&&this.$parent.find("> .panel > .in");if(c&&c.length){var d=c.data("bs.collapse");if(d&&d.transitioning)return;c.collapse("hide"),d||c.data("bs.collapse",null)}var e=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[e](0),this.transitioning=1;var f=function(){this.$element.removeClass("collapsing").addClass("in")[e]("auto"),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return f.call(this);var g=a.camelCase(["scroll",e].join("-"));this.$element.one(a.support.transition.end,a.proxy(f,this)).emulateTransitionEnd(350)[e](this.$element[0][g])}}},b.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"),this.transitioning=1;var d=function(){this.transitioning=0,this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};return a.support.transition?(this.$element[c](0).one(a.support.transition.end,a.proxy(d,this)).emulateTransitionEnd(350),void 0):d.call(this)}}},b.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var c=a.fn.collapse;a.fn.collapse=function(c){return this.each(function(){var d=a(this),e=d.data("bs.collapse"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c);e||d.data("bs.collapse",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.collapse.Constructor=b,a.fn.collapse.noConflict=function(){return a.fn.collapse=c,this},a(document).on("click.bs.collapse.data-api","[data-toggle=collapse]",function(b){var c,d=a(this),e=d.attr("data-target")||b.preventDefault()||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,""),f=a(e),g=f.data("bs.collapse"),h=g?"toggle":d.data(),i=d.attr("data-parent"),j=i&&a(i);g&&g.transitioning||(j&&j.find('[data-toggle=collapse][data-parent="'+i+'"]').not(d).addClass("collapsed"),d[f.hasClass("in")?"addClass":"removeClass"]("collapsed")),f.collapse(h)})}(jQuery),+function(a){"use strict";function b(){a(d).remove(),a(e).each(function(b){var d=c(a(this));d.hasClass("open")&&(d.trigger(b=a.Event("hide.bs.dropdown")),b.isDefaultPrevented()||d.removeClass("open").trigger("hidden.bs.dropdown"))})}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}var d=".dropdown-backdrop",e="[data-toggle=dropdown]",f=function(b){a(b).on("click.bs.dropdown",this.toggle)};f.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){if("ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b),f.trigger(d=a.Event("show.bs.dropdown")),d.isDefaultPrevented())return;f.toggleClass("open").trigger("shown.bs.dropdown"),e.focus()}return!1}},f.prototype.keydown=function(b){if(/(38|40|27)/.test(b.keyCode)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var f=c(d),g=f.hasClass("open");if(!g||g&&27==b.keyCode)return 27==b.which&&f.find(e).focus(),d.click();var h=a("[role=menu] li:not(.divider):visible a",f);if(h.length){var i=h.index(h.filter(":focus"));38==b.keyCode&&i>0&&i--,40==b.keyCode&&i<h.length-1&&i++,~i||(i=0),h.eq(i).focus()}}}};var g=a.fn.dropdown;a.fn.dropdown=function(b){return this.each(function(){var c=a(this),d=c.data("dropdown");d||c.data("dropdown",d=new f(this)),"string"==typeof b&&d[b].call(c)})},a.fn.dropdown.Constructor=f,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=g,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",e,f.prototype.toggle).on("keydown.bs.dropdown.data-api",e+", [role=menu]",f.prototype.keydown)}(jQuery),+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.load(this.options.remote)};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(d),this.isShown||d.isDefaultPrevented()||(this.isShown=!0,this.escape(),this.$element.on("click.dismiss.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("fade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show(),d&&c.$element[0].offsetWidth,c.$element.addClass("in").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.modal",{relatedTarget:b});d?c.$element.find(".modal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)}))},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one(a.support.transition.end,a.proxy(this.hideModal,this)).emulateTransitionEnd(300):this.hideModal())},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.modal")},b.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.modal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var d=a.support.transition&&c;if(this.$backdrop=a('<div class="modal-backdrop '+c+'" />').appendTo(document.body),this.$element.on("click.dismiss.modal",a.proxy(function(a){a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus.call(this.$element[0]):this.hide.call(this))},this)),d&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;d?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.modal;a.fn.modal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},b.DEFAULTS,e.data(),"object"==typeof c&&c);f||e.data("bs.modal",f=new b(this,g)),"string"==typeof c?f[c](d):g.show&&f.show(d)})},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function(){return a.fn.modal=c,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());b.preventDefault(),e.modal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.modal",".modal",function(){a(document.body).addClass("modal-open")}).on("hidden.bs.modal",".modal",function(){a(document.body).removeClass("modal-open")})}(jQuery),+function(a){"use strict";var b=function(a,b){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null,this.init("tooltip",a,b)};b.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},b.prototype.init=function(b,c,d){this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d);for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focus",i="hover"==g?"mouseleave":"blur";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},b.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},b.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show),void 0):c.show()},b.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide),void 0):c.hide()},b.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){if(this.$element.trigger(b),b.isDefaultPrevented())return;var c=this.tip();this.setContent(),this.options.animation&&c.addClass("fade");var d="function"==typeof this.options.placement?this.options.placement.call(this,c[0],this.$element[0]):this.options.placement,e=/\s?auto?\s?/i,f=e.test(d);f&&(d=d.replace(e,"")||"top"),c.detach().css({top:0,left:0,display:"block"}).addClass(d),this.options.container?c.appendTo(this.options.container):c.insertAfter(this.$element);var g=this.getPosition(),h=c[0].offsetWidth,i=c[0].offsetHeight;if(f){var j=this.$element.parent(),k=d,l=document.documentElement.scrollTop||document.body.scrollTop,m="body"==this.options.container?window.innerWidth:j.outerWidth(),n="body"==this.options.container?window.innerHeight:j.outerHeight(),o="body"==this.options.container?0:j.offset().left;d="bottom"==d&&g.top+g.height+i-l>n?"top":"top"==d&&g.top-l-i<0?"bottom":"right"==d&&g.right+h>m?"left":"left"==d&&g.left-h<o?"right":d,c.removeClass(k).addClass(d)}var p=this.getCalculatedOffset(d,g,h,i);this.applyPlacement(p,d),this.$element.trigger("shown.bs."+this.type)}},b.prototype.applyPlacement=function(a,b){var c,d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),a.top=a.top+g,a.left=a.left+h,d.offset(a).addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;if("top"==b&&j!=f&&(c=!0,a.top=a.top+f-j),/bottom|top/.test(b)){var k=0;a.left<0&&(k=-2*a.left,a.left=0,d.offset(a),i=d[0].offsetWidth,j=d[0].offsetHeight),this.replaceArrow(k-e+i,i,"left")}else this.replaceArrow(j-f,j,"top");c&&d.offset(a)},b.prototype.replaceArrow=function(a,b,c){this.arrow().css(c,a?50*(1-a/b)+"%":"")},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},b.prototype.hide=function(){function b(){"in"!=c.hoverState&&d.detach()}var c=this,d=this.tip(),e=a.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(d.removeClass("in"),a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,b).emulateTransitionEnd(150):b(),this.$element.trigger("hidden.bs."+this.type),this)},b.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},b.prototype.hasContent=function(){return this.getTitle()},b.prototype.getPosition=function(){var b=this.$element[0];return a.extend({},"function"==typeof b.getBoundingClientRect?b.getBoundingClientRect():{width:b.offsetWidth,height:b.offsetHeight},this.$element.offset())},b.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},b.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},b.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},b.prototype.validate=function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},b.prototype.enable=function(){this.enabled=!0},b.prototype.disable=function(){this.enabled=!1},b.prototype.toggleEnabled=function(){this.enabled=!this.enabled},b.prototype.toggle=function(b){var c=b?a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type):this;c.tip().hasClass("in")?c.leave(c):c.enter(c)},b.prototype.destroy=function(){this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var c=a.fn.tooltip;a.fn.tooltip=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof c&&c;e||d.data("bs.tooltip",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.tooltip.Constructor=b,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=c,this}}(jQuery),+function(a){"use strict";var b=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");b.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),b.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),b.prototype.constructor=b,b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content")[this.options.html?"html":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},b.prototype.hasContent=function(){return this.getTitle()||this.getContent()},b.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")},b.prototype.tip=function(){return this.$tip||(this.$tip=a(this.options.template)),this.$tip};var c=a.fn.popover;a.fn.popover=function(c){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof c&&c;e||d.data("bs.popover",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.popover.Constructor=b,a.fn.popover.noConflict=function(){return a.fn.popover=c,this}}(jQuery),+function(a){"use strict";function b(c,d){var e,f=a.proxy(this.process,this);this.$element=a(c).is("body")?a(window):a(c),this.$body=a("body"),this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",f),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||(e=a(c).attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.offsets=a([]),this.targets=a([]),this.activeTarget=null,this.refresh(),this.process()}b.DEFAULTS={offset:10},b.prototype.refresh=function(){var b=this.$element[0]==window?"offset":"position";this.offsets=a([]),this.targets=a([]);var c=this;this.$body.find(this.selector).map(function(){var d=a(this),e=d.data("target")||d.attr("href"),f=/^#\w/.test(e)&&a(e);return f&&f.length&&[[f[b]().top+(!a.isWindow(c.$scrollElement.get(0))&&c.$scrollElement.scrollTop()),e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){c.offsets.push(this[0]),c.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,d=c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(b>=d)return g!=(a=f.last()[0])&&this.activate(a);for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(!e[a+1]||b<=e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,a(this.selector).parents(".active").removeClass("active");var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate")};var c=a.fn.scrollspy;a.fn.scrollspy=function(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=c,this},a(window).on("load",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);b.scrollspy(b.data())})})}(jQuery),+function(a){"use strict";var b=function(b){this.element=a(b)};b.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a")[0],f=a.Event("show.bs.tab",{relatedTarget:e});if(b.trigger(f),!f.isDefaultPrevented()){var g=a(d);this.activate(b.parent("li"),c),this.activate(g,g.parent(),function(){b.trigger({type:"shown.bs.tab",relatedTarget:e})})}}},b.prototype.activate=function(b,c,d){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),b.addClass("active"),g?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu")&&b.closest("li.dropdown").addClass("active"),d&&d()}var f=c.find("> .active"),g=d&&a.support.transition&&f.hasClass("fade");g?f.one(a.support.transition.end,e).emulateTransitionEnd(150):e(),f.removeClass("in")};var c=a.fn.tab;a.fn.tab=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new b(this)),"string"==typeof c&&e[c]()})},a.fn.tab.Constructor=b,a.fn.tab.noConflict=function(){return a.fn.tab=c,this},a(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(b){b.preventDefault(),a(this).tab("show")})}(jQuery),+function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$window=a(window).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(c),this.affixed=this.unpin=null,this.checkPosition()};b.RESET="affix affix-top affix-bottom",b.DEFAULTS={offset:0},b.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},b.prototype.checkPosition=function(){if(this.$element.is(":visible")){var c=a(document).height(),d=this.$window.scrollTop(),e=this.$element.offset(),f=this.options.offset,g=f.top,h=f.bottom;"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top()),"function"==typeof h&&(h=f.bottom());var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.$element.height()>=c-h?"bottom":null!=g&&g>=d?"top":!1;this.affixed!==i&&(this.unpin&&this.$element.css("top",""),this.affixed=i,this.unpin="bottom"==i?e.top-d:null,this.$element.removeClass(b.RESET).addClass("affix"+(i?"-"+i:"")),"bottom"==i&&this.$element.offset({top:document.body.offsetHeight-h-this.$element.height()}))}};var c=a.fn.affix;a.fn.affix=function(c){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof c&&c;e||d.data("bs.affix",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.affix.Constructor=b,a.fn.affix.noConflict=function(){return a.fn.affix=c,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var b=a(this),c=b.data();c.offset=c.offset||{},c.offsetBottom&&(c.offset.bottom=c.offsetBottom),c.offsetTop&&(c.offset.top=c.offsetTop),b.affix(c)})})}(jQuery);;/*
 AngularJS v1.4.6
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(F,x,Oa){'use strict';function va(a,b,c){if(!a)throw ngMinErr("areq",b||"?",c||"required");return a}function wa(a,b){if(!a&&!b)return"";if(!a)return b;if(!b)return a;Y(a)&&(a=a.join(" "));Y(b)&&(b=b.join(" "));return a+" "+b}function Fa(a){var b={};a&&(a.to||a.from)&&(b.to=a.to,b.from=a.from);return b}function V(a,b,c){var d="";a=Y(a)?a:a&&M(a)&&a.length?a.split(/\s+/):[];q(a,function(a,y){a&&0<a.length&&(d+=0<y?" ":"",d+=c?b+a:a+b)});return d}function Ga(a){if(a instanceof I)switch(a.length){case 0:return[];
case 1:if(1===a[0].nodeType)return a;break;default:return I(Z(a))}if(1===a.nodeType)return I(a)}function Z(a){if(!a[0])return a;for(var b=0;b<a.length;b++){var c=a[b];if(1==c.nodeType)return c}}function Ha(a,b,c){q(b,function(b){a.addClass(b,c)})}function Ia(a,b,c){q(b,function(b){a.removeClass(b,c)})}function Q(a){return function(b,c){c.addClass&&(Ha(a,b,c.addClass),c.addClass=null);c.removeClass&&(Ia(a,b,c.removeClass),c.removeClass=null)}}function ia(a){a=a||{};if(!a.$$prepared){var b=a.domOperation||
L;a.domOperation=function(){a.$$domOperationFired=!0;b();b=L};a.$$prepared=!0}return a}function ea(a,b){xa(a,b);ya(a,b)}function xa(a,b){b.from&&(a.css(b.from),b.from=null)}function ya(a,b){b.to&&(a.css(b.to),b.to=null)}function R(a,b,c){var d=(b.addClass||"")+" "+(c.addClass||""),e=(b.removeClass||"")+" "+(c.removeClass||"");a=Ja(a.attr("class"),d,e);c.preparationClasses&&(b.preparationClasses=$(c.preparationClasses,b.preparationClasses),delete c.preparationClasses);d=b.domOperation!==L?b.domOperation:
null;za(b,c);d&&(b.domOperation=d);b.addClass=a.addClass?a.addClass:null;b.removeClass=a.removeClass?a.removeClass:null;return b}function Ja(a,b,c){function d(a){M(a)&&(a=a.split(" "));var b={};q(a,function(a){a.length&&(b[a]=!0)});return b}var e={};a=d(a);b=d(b);q(b,function(a,b){e[b]=1});c=d(c);q(c,function(a,b){e[b]=1===e[b]?null:-1});var y={addClass:"",removeClass:""};q(e,function(b,c){var e,d;1===b?(e="addClass",d=!a[c]):-1===b&&(e="removeClass",d=a[c]);d&&(y[e].length&&(y[e]+=" "),y[e]+=c)});
return y}function G(a){return a instanceof x.element?a[0]:a}function Ka(a,b,c){var d="";b&&(d=V(b,"ng-",!0));c.addClass&&(d=$(d,V(c.addClass,"-add")));c.removeClass&&(d=$(d,V(c.removeClass,"-remove")));d.length&&(c.preparationClasses=d,a.addClass(d))}function ja(a,b){var c=b?"-"+b+"s":"";fa(a,[ga,c]);return[ga,c]}function ma(a,b){var c=b?"paused":"",d=W+"PlayState";fa(a,[d,c]);return[d,c]}function fa(a,b){a.style[b[0]]=b[1]}function $(a,b){return a?b?a+" "+b:a:b}function Aa(a,b,c){var d=Object.create(null),
e=a.getComputedStyle(b)||{};q(c,function(a,b){var c=e[a];if(c){var l=c.charAt(0);if("-"===l||"+"===l||0<=l)c=La(c);0===c&&(c=null);d[b]=c}});return d}function La(a){var b=0;a=a.split(/\s*,\s*/);q(a,function(a){"s"==a.charAt(a.length-1)&&(a=a.substring(0,a.length-1));a=parseFloat(a)||0;b=b?Math.max(a,b):a});return b}function na(a){return 0===a||null!=a}function Ba(a,b){var c=N,d=a+"s";b?c+="Duration":d+=" linear all";return[c,d]}function Ca(){var a=Object.create(null);return{flush:function(){a=Object.create(null)},
count:function(b){return(b=a[b])?b.total:0},get:function(b){return(b=a[b])&&b.value},put:function(b,c){a[b]?a[b].total++:a[b]={total:1,value:c}}}}var L=x.noop,za=x.extend,I=x.element,q=x.forEach,Y=x.isArray,M=x.isString,oa=x.isObject,pa=x.isUndefined,qa=x.isDefined,Da=x.isFunction,ra=x.isElement,N,sa,W,ta;pa(F.ontransitionend)&&qa(F.onwebkittransitionend)?(N="WebkitTransition",sa="webkitTransitionEnd transitionend"):(N="transition",sa="transitionend");pa(F.onanimationend)&&qa(F.onwebkitanimationend)?
(W="WebkitAnimation",ta="webkitAnimationEnd animationend"):(W="animation",ta="animationend");var ka=W+"Delay",ua=W+"Duration",ga=N+"Delay";F=N+"Duration";var Ma={transitionDuration:F,transitionDelay:ga,transitionProperty:N+"Property",animationDuration:ua,animationDelay:ka,animationIterationCount:W+"IterationCount"},Na={transitionDuration:F,transitionDelay:ga,animationDuration:ua,animationDelay:ka};x.module("ngAnimate",[]).provider("$$body",function(){this.$get=["$document",function(a){return I(a[0].body)}]}).directive("ngAnimateChildren",
[function(){return function(a,b,c){a=c.ngAnimateChildren;x.isString(a)&&0===a.length?b.data("$$ngAnimateChildren",!0):c.$observe("ngAnimateChildren",function(a){b.data("$$ngAnimateChildren","on"===a||"true"===a)})}}]).factory("$$rAFScheduler",["$$rAF",function(a){function b(a){d=d.concat(a);c()}function c(){if(d.length){for(var b=d.shift(),v=0;v<b.length;v++)b[v]();e||a(function(){e||c()})}}var d,e;d=b.queue=[];b.waitUntilQuiet=function(b){e&&e();e=a(function(){e=null;b();c()})};return b}]).factory("$$AnimateRunner",
["$q","$sniffer","$$animateAsyncRun",function(a,b,c){function d(a){this.setHost(a);this._doneCallbacks=[];this._runInAnimationFrame=c();this._state=0}d.chain=function(a,b){function c(){if(d===a.length)b(!0);else a[d](function(a){!1===a?b(!1):(d++,c())})}var d=0;c()};d.all=function(a,b){function c(v){l=l&&v;++d===a.length&&b(l)}var d=0,l=!0;q(a,function(a){a.done(c)})};d.prototype={setHost:function(a){this.host=a||{}},done:function(a){2===this._state?a():this._doneCallbacks.push(a)},progress:L,getPromise:function(){if(!this.promise){var b=
this;this.promise=a(function(a,c){b.done(function(b){!1===b?c():a()})})}return this.promise},then:function(a,b){return this.getPromise().then(a,b)},"catch":function(a){return this.getPromise()["catch"](a)},"finally":function(a){return this.getPromise()["finally"](a)},pause:function(){this.host.pause&&this.host.pause()},resume:function(){this.host.resume&&this.host.resume()},end:function(){this.host.end&&this.host.end();this._resolve(!0)},cancel:function(){this.host.cancel&&this.host.cancel();this._resolve(!1)},
complete:function(a){var b=this;0===b._state&&(b._state=1,b._runInAnimationFrame(function(){b._resolve(a)}))},_resolve:function(a){2!==this._state&&(q(this._doneCallbacks,function(b){b(a)}),this._doneCallbacks.length=0,this._state=2)}};return d}]).factory("$$animateAsyncRun",["$$rAF",function(a){function b(b){c.push(b);1<c.length||a(function(){for(var a=0;a<c.length;a++)c[a]();c=[]})}var c=[];return function(){var a=!1;b(function(){a=!0});return function(c){a?c():b(c)}}}]).provider("$$animateQueue",
["$animateProvider",function(a){function b(a,b,c,q){return d[a].some(function(a){return a(b,c,q)})}function c(a,b){a=a||{};var c=0<(a.addClass||"").length,d=0<(a.removeClass||"").length;return b?c&&d:c||d}var d=this.rules={skip:[],cancel:[],join:[]};d.join.push(function(a,b,d){return!b.structural&&c(b.options)});d.skip.push(function(a,b,d){return!b.structural&&!c(b.options)});d.skip.push(function(a,b,c){return"leave"==c.event&&b.structural});d.skip.push(function(a,b,c){return c.structural&&2===c.state&&
!b.structural});d.cancel.push(function(a,b,c){return c.structural&&b.structural});d.cancel.push(function(a,b,c){return 2===c.state&&b.structural});d.cancel.push(function(a,b,c){a=b.options;c=c.options;return a.addClass&&a.addClass===c.removeClass||a.removeClass&&a.removeClass===c.addClass});this.$get=["$$rAF","$rootScope","$rootElement","$document","$$body","$$HashMap","$$animation","$$AnimateRunner","$templateRequest","$$jqLite","$$forceReflow",function(d,y,v,z,l,s,O,x,t,la,E){function h(a,b){var c=
G(a),f=[],g=w[b];g&&q(g,function(a){a.node.contains(c)&&f.push(a.callback)});return f}function S(a,b,c,f){d(function(){q(h(b,a),function(a){a(b,c,f)})})}function u(a,g,m){function d(b,c,g,f){S(c,a,g,f);b.progress(c,g,f)}function w(b){var c=a,g=m;g.preparationClasses&&(c.removeClass(g.preparationClasses),g.preparationClasses=null);g.activeClasses&&(c.removeClass(g.activeClasses),g.activeClasses=null);Ea(a,m);ea(a,m);m.domOperation();e.complete(!b)}var B,h;if(a=Ga(a))B=G(a),h=a.parent();m=ia(m);var e=
new x;Y(m.addClass)&&(m.addClass=m.addClass.join(" "));m.addClass&&!M(m.addClass)&&(m.addClass=null);Y(m.removeClass)&&(m.removeClass=m.removeClass.join(" "));m.removeClass&&!M(m.removeClass)&&(m.removeClass=null);m.from&&!oa(m.from)&&(m.from=null);m.to&&!oa(m.to)&&(m.to=null);if(!B)return w(),e;var k=[B.className,m.addClass,m.removeClass].join(" ");if(!C(k))return w(),e;var l=0<=["enter","move","leave"].indexOf(g),u=!H||U.get(B),k=!u&&A.get(B)||{},v=!!k.state;u||v&&1==k.state||(u=!p(a,h,g));if(u)return w(),
e;l&&J(a);h={structural:l,element:a,event:g,close:w,options:m,runner:e};if(v){if(b("skip",a,h,k)){if(2===k.state)return w(),e;R(a,k.options,m);return k.runner}if(b("cancel",a,h,k))if(2===k.state)k.runner.end();else if(k.structural)k.close();else return R(a,k.options,h.options),k.runner;else if(b("join",a,h,k))if(2===k.state)R(a,m,{});else return Ka(a,l?g:null,m),g=h.event=k.event,m=R(a,k.options,h.options),k.runner}else R(a,m,{});(v=h.structural)||(v="animate"===h.event&&0<Object.keys(h.options.to||
{}).length||c(h.options));if(!v)return w(),f(a),e;var t=(k.counter||0)+1;h.counter=t;r(a,1,h);y.$$postDigest(function(){var b=A.get(B),h=!b,b=b||{},J=0<(a.parent()||[]).length&&("animate"===b.event||b.structural||c(b.options));if(h||b.counter!==t||!J){h&&(Ea(a,m),ea(a,m));if(h||l&&b.event!==g)m.domOperation(),e.end();J||f(a)}else g=!b.structural&&c(b.options,!0)?"setClass":b.event,r(a,2),b=O(a,g,b.options),b.done(function(b){w(!b);(b=A.get(B))&&b.counter===t&&f(G(a));d(e,g,"close",{})}),e.setHost(b),
d(e,g,"start",{})});return e}function J(a){a=G(a).querySelectorAll("[data-ng-animate]");q(a,function(a){var b=parseInt(a.getAttribute("data-ng-animate")),c=A.get(a);switch(b){case 2:c.runner.end();case 1:c&&A.remove(a)}})}function f(a){a=G(a);a.removeAttribute("data-ng-animate");A.remove(a)}function B(a,b){return G(a)===G(b)}function p(a,b,c){c=B(a,l)||"HTML"===a[0].nodeName;var g=B(a,v),f=!1,d;for((a=a.data("$ngAnimatePin"))&&(b=a);b&&b.length;){g||(g=B(b,v));a=b[0];if(1!==a.nodeType)break;var w=
A.get(a)||{};f||(f=w.structural||U.get(a));if(pa(d)||!0===d)a=b.data("$$ngAnimateChildren"),qa(a)&&(d=a);if(f&&!1===d)break;g||(g=B(b,v),g||(a=b.data("$ngAnimatePin"))&&(b=a));c||(c=B(b,l));b=b.parent()}return(!f||d)&&g&&c}function r(a,b,c){c=c||{};c.state=b;a=G(a);a.setAttribute("data-ng-animate",b);c=(b=A.get(a))?za(b,c):c;A.put(a,c)}var A=new s,U=new s,H=null,g=y.$watch(function(){return 0===t.totalPendingRequests},function(a){a&&(g(),y.$$postDigest(function(){y.$$postDigest(function(){null===
H&&(H=!0)})}))}),w={},k=a.classNameFilter(),C=k?function(a){return k.test(a)}:function(){return!0},Ea=Q(la);return{on:function(a,b,c){b=Z(b);w[a]=w[a]||[];w[a].push({node:b,callback:c})},off:function(a,b,c){function g(a,b,c){var f=Z(b);return a.filter(function(a){return!(a.node===f&&(!c||a.callback===c))})}var f=w[a];f&&(w[a]=1===arguments.length?null:g(f,b,c))},pin:function(a,b){va(ra(a),"element","not an element");va(ra(b),"parentElement","not an element");a.data("$ngAnimatePin",b)},push:function(a,
b,c,g){c=c||{};c.domOperation=g;return u(a,b,c)},enabled:function(a,b){var c=arguments.length;if(0===c)b=!!H;else if(ra(a)){var g=G(a),f=U.get(g);1===c?b=!f:(b=!!b)?f&&U.remove(g):U.put(g,!0)}else b=H=!!a;return b}}}]}]).provider("$$animation",["$animateProvider",function(a){function b(a){return a.data("$$animationRunner")}var c=this.drivers=[];this.$get=["$$jqLite","$rootScope","$injector","$$AnimateRunner","$$HashMap","$$rAFScheduler",function(a,e,y,v,z,l){function s(a){function b(a){if(a.processed)return a;
a.processed=!0;var f=a.domNode,d=f.parentNode;e.put(f,a);for(var h;d;){if(h=e.get(d)){h.processed||(h=b(h));break}d=d.parentNode}(h||c).children.push(a);return a}var c={children:[]},d,e=new z;for(d=0;d<a.length;d++){var l=a[d];e.put(l.domNode,a[d]={domNode:l.domNode,fn:l.fn,children:[]})}for(d=0;d<a.length;d++)b(a[d]);return function(a){var b=[],c=[],d;for(d=0;d<a.children.length;d++)c.push(a.children[d]);a=c.length;var h=0,e=[];for(d=0;d<c.length;d++){var l=c[d];0>=a&&(a=h,h=0,b.push(e),e=[]);e.push(l.fn);
l.children.forEach(function(a){h++;c.push(a)});a--}e.length&&b.push(e);return b}(c)}var O=[],x=Q(a);return function(t,z,E){function h(a){a=a.hasAttribute("ng-animate-ref")?[a]:a.querySelectorAll("[ng-animate-ref]");var b=[];q(a,function(a){var c=a.getAttribute("ng-animate-ref");c&&c.length&&b.push(a)});return b}function S(a){var b=[],c={};q(a,function(a,g){var d=G(a.element),f=0<=["enter","move"].indexOf(a.event),d=a.structural?h(d):[];if(d.length){var e=f?"to":"from";q(d,function(a){var b=a.getAttribute("ng-animate-ref");
c[b]=c[b]||{};c[b][e]={animationID:g,element:I(a)}})}else b.push(a)});var d={},f={};q(c,function(c,e){var h=c.from,r=c.to;if(h&&r){var J=a[h.animationID],k=a[r.animationID],B=h.animationID.toString();if(!f[B]){var l=f[B]={structural:!0,beforeStart:function(){J.beforeStart();k.beforeStart()},close:function(){J.close();k.close()},classes:u(J.classes,k.classes),from:J,to:k,anchors:[]};l.classes.length?b.push(l):(b.push(J),b.push(k))}f[B].anchors.push({out:h.element,"in":r.element})}else h=h?h.animationID:
r.animationID,r=h.toString(),d[r]||(d[r]=!0,b.push(a[h]))});return b}function u(a,b){a=a.split(" ");b=b.split(" ");for(var c=[],d=0;d<a.length;d++){var f=a[d];if("ng-"!==f.substring(0,3))for(var h=0;h<b.length;h++)if(f===b[h]){c.push(f);break}}return c.join(" ")}function J(a){for(var b=c.length-1;0<=b;b--){var d=c[b];if(y.has(d)&&(d=y.get(d)(a)))return d}}function f(a,c){a.from&&a.to?(b(a.from.element).setHost(c),b(a.to.element).setHost(c)):b(a.element).setHost(c)}function B(){var a=b(t);!a||"leave"===
z&&E.$$domOperationFired||a.end()}function p(b){t.off("$destroy",B);t.removeData("$$animationRunner");x(t,E);ea(t,E);E.domOperation();H&&a.removeClass(t,H);t.removeClass("ng-animate");A.complete(!b)}E=ia(E);var r=0<=["enter","move","leave"].indexOf(z),A=new v({end:function(){p()},cancel:function(){p(!0)}});if(!c.length)return p(),A;t.data("$$animationRunner",A);var U=wa(t.attr("class"),wa(E.addClass,E.removeClass)),H=E.tempClasses;H&&(U+=" "+H,E.tempClasses=null);O.push({element:t,classes:U,event:z,
structural:r,options:E,beforeStart:function(){t.addClass("ng-animate");H&&a.addClass(t,H)},close:p});t.on("$destroy",B);if(1<O.length)return A;e.$$postDigest(function(){var a=[];q(O,function(c){b(c.element)?a.push(c):c.close()});O.length=0;var c=S(a),d=[];q(c,function(a){d.push({domNode:G(a.from?a.from.element:a.element),fn:function(){a.beforeStart();var c,d=a.close;if(b(a.anchors?a.from.element||a.to.element:a.element)){var g=J(a);g&&(c=g.start)}c?(c=c(),c.done(function(a){d(!a)}),f(a,c)):d()}})});
l(s(d))});return A}}]}]).provider("$animateCss",["$animateProvider",function(a){var b=Ca(),c=Ca();this.$get=["$window","$$jqLite","$$AnimateRunner","$timeout","$$forceReflow","$sniffer","$$rAFScheduler","$animate",function(a,e,y,v,z,l,s,O){function x(a,b){var c=a.parentNode;return(c.$$ngAnimateParentKey||(c.$$ngAnimateParentKey=++S))+"-"+a.getAttribute("class")+"-"+b}function t(h,f,l,p){var r;0<b.count(l)&&(r=c.get(l),r||(f=V(f,"-stagger"),e.addClass(h,f),r=Aa(a,h,p),r.animationDuration=Math.max(r.animationDuration,
0),r.transitionDuration=Math.max(r.transitionDuration,0),e.removeClass(h,f),c.put(l,r)));return r||{}}function la(a){u.push(a);s.waitUntilQuiet(function(){b.flush();c.flush();for(var a=z(),d=0;d<u.length;d++)u[d](a);u.length=0})}function E(c,f,h){f=b.get(h);f||(f=Aa(a,c,Ma),"infinite"===f.animationIterationCount&&(f.animationIterationCount=1));b.put(h,f);c=f;h=c.animationDelay;f=c.transitionDelay;c.maxDelay=h&&f?Math.max(h,f):h||f;c.maxDuration=Math.max(c.animationDuration*c.animationIterationCount,
c.transitionDuration);return c}var h=Q(e),S=0,u=[];return function(a,c){function d(){r()}function p(){r(!0)}function r(b){if(!(s||S&&z)){s=!0;z=!1;c.$$skipPreparationClasses||e.removeClass(a,aa);e.removeClass(a,$);ma(g,!1);ja(g,!1);q(w,function(a){g.style[a[0]]=""});h(a,c);ea(a,c);if(c.onDone)c.onDone();m&&m.complete(!b)}}function A(a){n.blockTransition&&ja(g,a);n.blockKeyframeAnimation&&ma(g,!!a)}function u(){m=new y({end:d,cancel:p});la(L);r();return{$$willAnimate:!1,start:function(){return m},
end:d}}function H(){function b(){if(!s){A(!1);q(w,function(a){g.style[a[0]]=a[1]});h(a,c);e.addClass(a,$);if(n.recalculateTimingStyles){ha=g.className+" "+aa;ba=x(g,ha);D=E(g,ha,ba);X=D.maxDelay;I=Math.max(X,0);K=D.maxDuration;if(0===K){r();return}n.hasTransitions=0<D.transitionDuration;n.hasAnimations=0<D.animationDuration}n.applyAnimationDelay&&(X="boolean"!==typeof c.delay&&na(c.delay)?parseFloat(c.delay):X,I=Math.max(X,0),D.animationDelay=X,da=[ka,X+"s"],w.push(da),g.style[da[0]]=da[1]);M=1E3*
I;Q=1E3*K;if(c.easing){var k,p=c.easing;n.hasTransitions&&(k=N+"TimingFunction",w.push([k,p]),g.style[k]=p);n.hasAnimations&&(k=W+"TimingFunction",w.push([k,p]),g.style[k]=p)}D.transitionDuration&&m.push(sa);D.animationDuration&&m.push(ta);H=Date.now();var u=M+1.5*Q;k=H+u;var p=a.data("$$animateCss")||[],z=!0;if(p.length){var C=p[0];(z=k>C.expectedEndTime)?v.cancel(C.timer):p.push(r)}z&&(u=v(d,u,!1),p[0]={timer:u,expectedEndTime:k},p.push(r),a.data("$$animateCss",p));a.on(m.join(" "),l);ya(a,c)}}
function d(){var b=a.data("$$animateCss");if(b){for(var c=1;c<b.length;c++)b[c]();a.removeData("$$animateCss")}}function l(a){a.stopPropagation();var b=a.originalEvent||a;a=b.$manualTimeStamp||b.timeStamp||Date.now();b=parseFloat(b.elapsedTime.toFixed(3));Math.max(a-H,0)>=M&&b>=K&&(S=!0,r())}if(!s)if(g.parentNode){var H,m=[],k=function(a){if(S)z&&a&&(z=!1,r());else if(z=!a,D.animationDuration)if(a=ma(g,z),z)w.push(a);else{var b=w,c=b.indexOf(a);0<=a&&b.splice(c,1)}},p=0<Z&&(D.transitionDuration&&
0===T.transitionDuration||D.animationDuration&&0===T.animationDuration)&&Math.max(T.animationDelay,T.transitionDelay);p?v(b,Math.floor(p*Z*1E3),!1):b();F.resume=function(){k(!0)};F.pause=function(){k(!1)}}else r()}var g=G(a);if(!g||!g.parentNode||!O.enabled())return u();c=ia(c);var w=[],k=a.attr("class"),C=Fa(c),s,z,S,m,F,I,M,K,Q;if(0===c.duration||!l.animations&&!l.transitions)return u();var ca=c.event&&Y(c.event)?c.event.join(" "):c.event,R="",P="";ca&&c.structural?R=V(ca,"ng-",!0):ca&&(R=ca);c.addClass&&
(P+=V(c.addClass,"-add"));c.removeClass&&(P.length&&(P+=" "),P+=V(c.removeClass,"-remove"));c.applyClassesEarly&&P.length&&h(a,c);var aa=[R,P].join(" ").trim(),ha=k+" "+aa,$=V(aa,"-active"),k=C.to&&0<Object.keys(C.to).length;if(!(0<(c.keyframeStyle||"").length||k||aa))return u();var ba,T;0<c.stagger?(C=parseFloat(c.stagger),T={transitionDelay:C,animationDelay:C,transitionDuration:0,animationDuration:0}):(ba=x(g,ha),T=t(g,aa,ba,Na));c.$$skipPreparationClasses||e.addClass(a,aa);c.transitionStyle&&(C=
[N,c.transitionStyle],fa(g,C),w.push(C));0<=c.duration&&(C=0<g.style[N].length,C=Ba(c.duration,C),fa(g,C),w.push(C));c.keyframeStyle&&(C=[W,c.keyframeStyle],fa(g,C),w.push(C));var Z=T?0<=c.staggerIndex?c.staggerIndex:b.count(ba):0;(ca=0===Z)&&!c.skipBlocking&&ja(g,9999);var D=E(g,ha,ba),X=D.maxDelay;I=Math.max(X,0);K=D.maxDuration;var n={};n.hasTransitions=0<D.transitionDuration;n.hasAnimations=0<D.animationDuration;n.hasTransitionAll=n.hasTransitions&&"all"==D.transitionProperty;n.applyTransitionDuration=
k&&(n.hasTransitions&&!n.hasTransitionAll||n.hasAnimations&&!n.hasTransitions);n.applyAnimationDuration=c.duration&&n.hasAnimations;n.applyTransitionDelay=na(c.delay)&&(n.applyTransitionDuration||n.hasTransitions);n.applyAnimationDelay=na(c.delay)&&n.hasAnimations;n.recalculateTimingStyles=0<P.length;if(n.applyTransitionDuration||n.applyAnimationDuration)K=c.duration?parseFloat(c.duration):K,n.applyTransitionDuration&&(n.hasTransitions=!0,D.transitionDuration=K,C=0<g.style[N+"Property"].length,w.push(Ba(K,
C))),n.applyAnimationDuration&&(n.hasAnimations=!0,D.animationDuration=K,w.push([ua,K+"s"]));if(0===K&&!n.recalculateTimingStyles)return u();if(null!=c.delay){var da=parseFloat(c.delay);n.applyTransitionDelay&&w.push([ga,da+"s"]);n.applyAnimationDelay&&w.push([ka,da+"s"])}null==c.duration&&0<D.transitionDuration&&(n.recalculateTimingStyles=n.recalculateTimingStyles||ca);M=1E3*I;Q=1E3*K;c.skipBlocking||(n.blockTransition=0<D.transitionDuration,n.blockKeyframeAnimation=0<D.animationDuration&&0<T.animationDelay&&
0===T.animationDuration);xa(a,c);n.blockTransition||n.blockKeyframeAnimation?A(K):c.skipBlocking||ja(g,!1);return{$$willAnimate:!0,end:d,start:function(){if(!s)return F={end:d,cancel:p,resume:null,pause:null},m=new y(F),la(H),m}}}}]}]).provider("$$animateCssDriver",["$$animationProvider",function(a){a.drivers.push("$$animateCssDriver");this.$get=["$animateCss","$rootScope","$$AnimateRunner","$rootElement","$$body","$sniffer","$$jqLite",function(a,c,d,e,y,v,z){function l(a){return a.replace(/\bng-\S+\b/g,
"")}function s(a,b){M(a)&&(a=a.split(" "));M(b)&&(b=b.split(" "));return a.filter(function(a){return-1===b.indexOf(a)}).join(" ")}function O(c,e,u){function v(a){var b={},c=G(a).getBoundingClientRect();q(["width","height","top","left"],function(a){var d=c[a];switch(a){case "top":d+=F.scrollTop;break;case "left":d+=F.scrollLeft}b[a]=Math.floor(d)+"px"});return b}function f(){var c=l(u.attr("class")||""),d=s(c,r),c=s(r,c),d=a(p,{to:v(u),addClass:"ng-anchor-in "+d,removeClass:"ng-anchor-out "+c,delay:!0});
return d.$$willAnimate?d:null}function z(){p.remove();e.removeClass("ng-animate-shim");u.removeClass("ng-animate-shim")}var p=I(G(e).cloneNode(!0)),r=l(p.attr("class")||"");e.addClass("ng-animate-shim");u.addClass("ng-animate-shim");p.addClass("ng-anchor");E.append(p);var A;c=function(){var c=a(p,{addClass:"ng-anchor-out",delay:!0,from:v(e)});return c.$$willAnimate?c:null}();if(!c&&(A=f(),!A))return z();var t=c||A;return{start:function(){function a(){c&&c.end()}var b,c=t.start();c.done(function(){c=
null;if(!A&&(A=f()))return c=A.start(),c.done(function(){c=null;z();b.complete()}),c;z();b.complete()});return b=new d({end:a,cancel:a})}}}function x(a,b,c,e){var f=t(a,L),l=t(b,L),p=[];q(e,function(a){(a=O(c,a.out,a["in"]))&&p.push(a)});if(f||l||0!==p.length)return{start:function(){function a(){q(b,function(a){a.end()})}var b=[];f&&b.push(f.start());l&&b.push(l.start());q(p,function(a){b.push(a.start())});var c=new d({end:a,cancel:a});d.all(b,function(a){c.complete(a)});return c}}}function t(c){var d=
c.element,e=c.options||{};c.structural&&(e.event=c.event,e.structural=!0,e.applyClassesEarly=!0,"leave"===c.event&&(e.onDone=e.domOperation));e.preparationClasses&&(e.event=$(e.event,e.preparationClasses));c=a(d,e);return c.$$willAnimate?c:null}if(!v.animations&&!v.transitions)return L;var F=G(y);c=G(e);var E=I(F.parentNode===c?F:c);Q(z);return function(a){return a.from&&a.to?x(a.from,a.to,a.classes,a.anchors):t(a)}}]}]).provider("$$animateJs",["$animateProvider",function(a){this.$get=["$injector",
"$$AnimateRunner","$$jqLite",function(b,c,d){function e(c){c=Y(c)?c:c.split(" ");for(var d=[],e={},s=0;s<c.length;s++){var q=c[s],y=a.$$registeredAnimations[q];y&&!e[q]&&(d.push(b.get(y)),e[q]=!0)}return d}var y=Q(d);return function(a,b,d,s){function x(){s.domOperation();y(a,s)}function F(a,b,d,e,f){switch(d){case "animate":b=[b,e.from,e.to,f];break;case "setClass":b=[b,E,h,f];break;case "addClass":b=[b,E,f];break;case "removeClass":b=[b,h,f];break;default:b=[b,f]}b.push(e);if(a=a.apply(a,b))if(Da(a.start)&&
(a=a.start()),a instanceof c)a.done(f);else if(Da(a))return a;return L}function t(a,b,d,e,f){var g=[];q(e,function(e){var h=e[f];h&&g.push(function(){var e,g,f=!1,l=function(a){f||(f=!0,(g||L)(a),e.complete(!a))};e=new c({end:function(){l()},cancel:function(){l(!0)}});g=F(h,a,b,d,function(a){l(!1===a)});return e})});return g}function G(a,b,d,e,f){var g=t(a,b,d,e,f);if(0===g.length){var h,k;"beforeSetClass"===f?(h=t(a,"removeClass",d,e,"beforeRemoveClass"),k=t(a,"addClass",d,e,"beforeAddClass")):"setClass"===
f&&(h=t(a,"removeClass",d,e,"removeClass"),k=t(a,"addClass",d,e,"addClass"));h&&(g=g.concat(h));k&&(g=g.concat(k))}if(0!==g.length)return function(a){var b=[];g.length&&q(g,function(a){b.push(a())});b.length?c.all(b,a):a();return function(a){q(b,function(b){a?b.cancel():b.end()})}}}3===arguments.length&&oa(d)&&(s=d,d=null);s=ia(s);d||(d=a.attr("class")||"",s.addClass&&(d+=" "+s.addClass),s.removeClass&&(d+=" "+s.removeClass));var E=s.addClass,h=s.removeClass,I=e(d),u,J;if(I.length){var f,B;"leave"==
b?(B="leave",f="afterLeave"):(B="before"+b.charAt(0).toUpperCase()+b.substr(1),f=b);"enter"!==b&&"move"!==b&&(u=G(a,b,s,I,B));J=G(a,b,s,I,f)}if(u||J)return{start:function(){function b(c){f=!0;x();ea(a,s);h.complete(c)}var d,e=[];u&&e.push(function(a){d=u(a)});e.length?e.push(function(a){x();a(!0)}):x();J&&e.push(function(a){d=J(a)});var f=!1,h=new c({end:function(){f||((d||L)(void 0),b(void 0))},cancel:function(){f||((d||L)(!0),b(!0))}});c.chain(e,b);return h}}}}]}]).provider("$$animateJsDriver",
["$$animationProvider",function(a){a.drivers.push("$$animateJsDriver");this.$get=["$$animateJs","$$AnimateRunner",function(a,c){function d(c){return a(c.element,c.event,c.classes,c.options)}return function(a){if(a.from&&a.to){var b=d(a.from),v=d(a.to);if(b||v)return{start:function(){function a(){return function(){q(d,function(a){a.end()})}}var d=[];b&&d.push(b.start());v&&d.push(v.start());c.all(d,function(a){e.complete(a)});var e=new c({end:a(),cancel:a()});return e}}}else return d(a)}}]}])})(window,
window.angular);
//# sourceMappingURL=angular-animate.min.js.map
;/*
 AngularJS v1.4.6
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(I,f,C){'use strict';function D(t,e){e=e||{};f.forEach(e,function(f,k){delete e[k]});for(var k in t)!t.hasOwnProperty(k)||"$"===k.charAt(0)&&"$"===k.charAt(1)||(e[k]=t[k]);return e}var y=f.$$minErr("$resource"),B=/^(\.[a-zA-Z_$@][0-9a-zA-Z_$@]*)+$/;f.module("ngResource",["ng"]).provider("$resource",function(){var t=/^https?:\/\/[^\/]*/,e=this;this.defaults={stripTrailingSlashes:!0,actions:{get:{method:"GET"},save:{method:"POST"},query:{method:"GET",isArray:!0},remove:{method:"DELETE"},"delete":{method:"DELETE"}}};
this.$get=["$http","$q",function(k,F){function w(f,g){this.template=f;this.defaults=r({},e.defaults,g);this.urlParams={}}function z(l,g,s,h){function c(b,q){var c={};q=r({},g,q);u(q,function(a,q){x(a)&&(a=a());var m;if(a&&a.charAt&&"@"==a.charAt(0)){m=b;var d=a.substr(1);if(null==d||""===d||"hasOwnProperty"===d||!B.test("."+d))throw y("badmember",d);for(var d=d.split("."),n=0,g=d.length;n<g&&f.isDefined(m);n++){var e=d[n];m=null!==m?m[e]:C}}else m=a;c[q]=m});return c}function G(b){return b.resource}
function d(b){D(b||{},this)}var t=new w(l,h);s=r({},e.defaults.actions,s);d.prototype.toJSON=function(){var b=r({},this);delete b.$promise;delete b.$resolved;return b};u(s,function(b,q){var g=/^(POST|PUT|PATCH)$/i.test(b.method);d[q]=function(a,A,m,e){var n={},h,l,s;switch(arguments.length){case 4:s=e,l=m;case 3:case 2:if(x(A)){if(x(a)){l=a;s=A;break}l=A;s=m}else{n=a;h=A;l=m;break}case 1:x(a)?l=a:g?h=a:n=a;break;case 0:break;default:throw y("badargs",arguments.length);}var w=this instanceof d,p=w?
h:b.isArray?[]:new d(h),v={},z=b.interceptor&&b.interceptor.response||G,B=b.interceptor&&b.interceptor.responseError||C;u(b,function(b,a){"params"!=a&&"isArray"!=a&&"interceptor"!=a&&(v[a]=H(b))});g&&(v.data=h);t.setUrlParams(v,r({},c(h,b.params||{}),n),b.url);n=k(v).then(function(a){var c=a.data,m=p.$promise;if(c){if(f.isArray(c)!==!!b.isArray)throw y("badcfg",q,b.isArray?"array":"object",f.isArray(c)?"array":"object",v.method,v.url);b.isArray?(p.length=0,u(c,function(a){"object"===typeof a?p.push(new d(a)):
p.push(a)})):(D(c,p),p.$promise=m)}p.$resolved=!0;a.resource=p;return a},function(a){p.$resolved=!0;(s||E)(a);return F.reject(a)});n=n.then(function(a){var b=z(a);(l||E)(b,a.headers);return b},B);return w?n:(p.$promise=n,p.$resolved=!1,p)};d.prototype["$"+q]=function(a,b,c){x(a)&&(c=b,b=a,a={});a=d[q].call(this,a,this,b,c);return a.$promise||a}});d.bind=function(b){return z(l,r({},g,b),s)};return d}var E=f.noop,u=f.forEach,r=f.extend,H=f.copy,x=f.isFunction;w.prototype={setUrlParams:function(l,g,
e){var h=this,c=e||h.template,k,d,r="",b=h.urlParams={};u(c.split(/\W/),function(d){if("hasOwnProperty"===d)throw y("badname");!/^\d+$/.test(d)&&d&&(new RegExp("(^|[^\\\\]):"+d+"(\\W|$)")).test(c)&&(b[d]=!0)});c=c.replace(/\\:/g,":");c=c.replace(t,function(b){r=b;return""});g=g||{};u(h.urlParams,function(b,e){k=g.hasOwnProperty(e)?g[e]:h.defaults[e];f.isDefined(k)&&null!==k?(d=encodeURIComponent(k).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%20/g,"%20").replace(/%26/gi,
"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+"),c=c.replace(new RegExp(":"+e+"(\\W|$)","g"),function(a,b){return d+b})):c=c.replace(new RegExp("(/?):"+e+"(\\W|$)","g"),function(a,b,c){return"/"==c.charAt(0)?c:b+c})});h.defaults.stripTrailingSlashes&&(c=c.replace(/\/+$/,"")||"/");c=c.replace(/\/\.(?=\w+($|\?))/,".");l.url=r+c.replace(/\/\\\./,"/.");u(g,function(b,c){h.urlParams[c]||(l.params=l.params||{},l.params[c]=b)})}};return z}]})})(window,window.angular);
//# sourceMappingURL=angular-resource.min.js.map
;/*
 AngularJS v1.4.6
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(p,c,C){'use strict';function v(r,h,g){return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",link:function(a,f,b,d,y){function z(){k&&(g.cancel(k),k=null);l&&(l.$destroy(),l=null);m&&(k=g.leave(m),k.then(function(){k=null}),m=null)}function x(){var b=r.current&&r.current.locals;if(c.isDefined(b&&b.$template)){var b=a.$new(),d=r.current;m=y(b,function(b){g.enter(b,null,m||f).then(function(){!c.isDefined(t)||t&&!a.$eval(t)||h()});z()});l=d.scope=b;l.$emit("$viewContentLoaded");
l.$eval(w)}else z()}var l,m,k,t=b.autoscroll,w=b.onload||"";a.$on("$routeChangeSuccess",x);x()}}}function A(c,h,g){return{restrict:"ECA",priority:-400,link:function(a,f){var b=g.current,d=b.locals;f.html(d.$template);var y=c(f.contents());b.controller&&(d.$scope=a,d=h(b.controller,d),b.controllerAs&&(a[b.controllerAs]=d),f.data("$ngControllerController",d),f.children().data("$ngControllerController",d));y(a)}}}p=c.module("ngRoute",["ng"]).provider("$route",function(){function r(a,f){return c.extend(Object.create(a),
f)}function h(a,c){var b=c.caseInsensitiveMatch,d={originalPath:a,regexp:a},g=d.keys=[];a=a.replace(/([().])/g,"\\$1").replace(/(\/)?:(\w+)([\?\*])?/g,function(a,c,b,d){a="?"===d?d:null;d="*"===d?d:null;g.push({name:b,optional:!!a});c=c||"";return""+(a?"":c)+"(?:"+(a?c:"")+(d&&"(.+?)"||"([^/]+)")+(a||"")+")"+(a||"")}).replace(/([\/$\*])/g,"\\$1");d.regexp=new RegExp("^"+a+"$",b?"i":"");return d}var g={};this.when=function(a,f){var b=c.copy(f);c.isUndefined(b.reloadOnSearch)&&(b.reloadOnSearch=!0);
c.isUndefined(b.caseInsensitiveMatch)&&(b.caseInsensitiveMatch=this.caseInsensitiveMatch);g[a]=c.extend(b,a&&h(a,b));if(a){var d="/"==a[a.length-1]?a.substr(0,a.length-1):a+"/";g[d]=c.extend({redirectTo:a},h(d,b))}return this};this.caseInsensitiveMatch=!1;this.otherwise=function(a){"string"===typeof a&&(a={redirectTo:a});this.when(null,a);return this};this.$get=["$rootScope","$location","$routeParams","$q","$injector","$templateRequest","$sce",function(a,f,b,d,h,p,x){function l(b){var e=s.current;
(v=(n=k())&&e&&n.$$route===e.$$route&&c.equals(n.pathParams,e.pathParams)&&!n.reloadOnSearch&&!w)||!e&&!n||a.$broadcast("$routeChangeStart",n,e).defaultPrevented&&b&&b.preventDefault()}function m(){var u=s.current,e=n;if(v)u.params=e.params,c.copy(u.params,b),a.$broadcast("$routeUpdate",u);else if(e||u)w=!1,(s.current=e)&&e.redirectTo&&(c.isString(e.redirectTo)?f.path(t(e.redirectTo,e.params)).search(e.params).replace():f.url(e.redirectTo(e.pathParams,f.path(),f.search())).replace()),d.when(e).then(function(){if(e){var a=
c.extend({},e.resolve),b,f;c.forEach(a,function(b,e){a[e]=c.isString(b)?h.get(b):h.invoke(b,null,null,e)});c.isDefined(b=e.template)?c.isFunction(b)&&(b=b(e.params)):c.isDefined(f=e.templateUrl)&&(c.isFunction(f)&&(f=f(e.params)),c.isDefined(f)&&(e.loadedTemplateUrl=x.valueOf(f),b=p(f)));c.isDefined(b)&&(a.$template=b);return d.all(a)}}).then(function(f){e==s.current&&(e&&(e.locals=f,c.copy(e.params,b)),a.$broadcast("$routeChangeSuccess",e,u))},function(b){e==s.current&&a.$broadcast("$routeChangeError",
e,u,b)})}function k(){var a,b;c.forEach(g,function(d,g){var q;if(q=!b){var h=f.path();q=d.keys;var l={};if(d.regexp)if(h=d.regexp.exec(h)){for(var k=1,m=h.length;k<m;++k){var n=q[k-1],p=h[k];n&&p&&(l[n.name]=p)}q=l}else q=null;else q=null;q=a=q}q&&(b=r(d,{params:c.extend({},f.search(),a),pathParams:a}),b.$$route=d)});return b||g[null]&&r(g[null],{params:{},pathParams:{}})}function t(a,b){var d=[];c.forEach((a||"").split(":"),function(a,c){if(0===c)d.push(a);else{var f=a.match(/(\w+)(?:[?*])?(.*)/),
g=f[1];d.push(b[g]);d.push(f[2]||"");delete b[g]}});return d.join("")}var w=!1,n,v,s={routes:g,reload:function(){w=!0;a.$evalAsync(function(){l();m()})},updateParams:function(a){if(this.current&&this.current.$$route)a=c.extend({},this.current.params,a),f.path(t(this.current.$$route.originalPath,a)),f.search(a);else throw B("norout");}};a.$on("$locationChangeStart",l);a.$on("$locationChangeSuccess",m);return s}]});var B=c.$$minErr("ngRoute");p.provider("$routeParams",function(){this.$get=function(){return{}}});
p.directive("ngView",v);p.directive("ngView",A);v.$inject=["$route","$anchorScroll","$animate"];A.$inject=["$compile","$controller","$route"]})(window,window.angular);
//# sourceMappingURL=angular-route.min.js.map
;/*
 AngularJS v1.4.6
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(n,h,p){'use strict';function E(a){var f=[];r(f,h.noop).chars(a);return f.join("")}function g(a,f){var d={},c=a.split(","),b;for(b=0;b<c.length;b++)d[f?h.lowercase(c[b]):c[b]]=!0;return d}function F(a,f){function d(a,b,d,l){b=h.lowercase(b);if(s[b])for(;e.last()&&t[e.last()];)c("",e.last());u[b]&&e.last()==b&&c("",b);(l=v[b]||!!l)||e.push(b);var m={};d.replace(G,function(b,a,f,c,d){m[a]=q(f||c||d||"")});f.start&&f.start(b,m,l)}function c(b,a){var c=0,d;if(a=h.lowercase(a))for(c=e.length-
1;0<=c&&e[c]!=a;c--);if(0<=c){for(d=e.length-1;d>=c;d--)f.end&&f.end(e[d]);e.length=c}}"string"!==typeof a&&(a=null===a||"undefined"===typeof a?"":""+a);var b,k,e=[],m=a,l;for(e.last=function(){return e[e.length-1]};a;){l="";k=!0;if(e.last()&&w[e.last()])a=a.replace(new RegExp("([\\W\\w]*)<\\s*\\/\\s*"+e.last()+"[^>]*>","i"),function(a,b){b=b.replace(H,"$1").replace(I,"$1");f.chars&&f.chars(q(b));return""}),c("",e.last());else{if(0===a.indexOf("\x3c!--"))b=a.indexOf("--",4),0<=b&&a.lastIndexOf("--\x3e",
b)===b&&(f.comment&&f.comment(a.substring(4,b)),a=a.substring(b+3),k=!1);else if(x.test(a)){if(b=a.match(x))a=a.replace(b[0],""),k=!1}else if(J.test(a)){if(b=a.match(y))a=a.substring(b[0].length),b[0].replace(y,c),k=!1}else K.test(a)&&((b=a.match(z))?(b[4]&&(a=a.substring(b[0].length),b[0].replace(z,d)),k=!1):(l+="<",a=a.substring(1)));k&&(b=a.indexOf("<"),l+=0>b?a:a.substring(0,b),a=0>b?"":a.substring(b),f.chars&&f.chars(q(l)))}if(a==m)throw L("badparse",a);m=a}c()}function q(a){if(!a)return"";A.innerHTML=
a.replace(/</g,"&lt;");return A.textContent}function B(a){return a.replace(/&/g,"&amp;").replace(M,function(a){var d=a.charCodeAt(0);a=a.charCodeAt(1);return"&#"+(1024*(d-55296)+(a-56320)+65536)+";"}).replace(N,function(a){return"&#"+a.charCodeAt(0)+";"}).replace(/</g,"&lt;").replace(/>/g,"&gt;")}function r(a,f){var d=!1,c=h.bind(a,a.push);return{start:function(a,k,e){a=h.lowercase(a);!d&&w[a]&&(d=a);d||!0!==C[a]||(c("<"),c(a),h.forEach(k,function(d,e){var k=h.lowercase(e),g="img"===a&&"src"===k||
"background"===k;!0!==O[k]||!0===D[k]&&!f(d,g)||(c(" "),c(e),c('="'),c(B(d)),c('"'))}),c(e?"/>":">"))},end:function(a){a=h.lowercase(a);d||!0!==C[a]||(c("</"),c(a),c(">"));a==d&&(d=!1)},chars:function(a){d||c(B(a))}}}var L=h.$$minErr("$sanitize"),z=/^<((?:[a-zA-Z])[\w:-]*)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*(>?)/,y=/^<\/\s*([\w:-]+)[^>]*>/,G=/([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,K=/^</,J=/^<\//,H=/\x3c!--(.*?)--\x3e/g,x=/<!DOCTYPE([^>]*?)>/i,
I=/<!\[CDATA\[(.*?)]]\x3e/g,M=/[\uD800-\uDBFF][\uDC00-\uDFFF]/g,N=/([^\#-~| |!])/g,v=g("area,br,col,hr,img,wbr");n=g("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr");p=g("rp,rt");var u=h.extend({},p,n),s=h.extend({},n,g("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul")),t=h.extend({},p,g("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var"));
n=g("circle,defs,desc,ellipse,font-face,font-face-name,font-face-src,g,glyph,hkern,image,linearGradient,line,marker,metadata,missing-glyph,mpath,path,polygon,polyline,radialGradient,rect,stop,svg,switch,text,title,tspan,use");var w=g("script,style"),C=h.extend({},v,s,t,u,n),D=g("background,cite,href,longdesc,src,usemap,xlink:href");n=g("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,size,span,start,summary,tabindex,target,title,type,valign,value,vspace,width");
p=g("accent-height,accumulate,additive,alphabetic,arabic-form,ascent,baseProfile,bbox,begin,by,calcMode,cap-height,class,color,color-rendering,content,cx,cy,d,dx,dy,descent,display,dur,end,fill,fill-rule,font-family,font-size,font-stretch,font-style,font-variant,font-weight,from,fx,fy,g1,g2,glyph-name,gradientUnits,hanging,height,horiz-adv-x,horiz-origin-x,ideographic,k,keyPoints,keySplines,keyTimes,lang,marker-end,marker-mid,marker-start,markerHeight,markerUnits,markerWidth,mathematical,max,min,offset,opacity,orient,origin,overline-position,overline-thickness,panose-1,path,pathLength,points,preserveAspectRatio,r,refX,refY,repeatCount,repeatDur,requiredExtensions,requiredFeatures,restart,rotate,rx,ry,slope,stemh,stemv,stop-color,stop-opacity,strikethrough-position,strikethrough-thickness,stroke,stroke-dasharray,stroke-dashoffset,stroke-linecap,stroke-linejoin,stroke-miterlimit,stroke-opacity,stroke-width,systemLanguage,target,text-anchor,to,transform,type,u1,u2,underline-position,underline-thickness,unicode,unicode-range,units-per-em,values,version,viewBox,visibility,width,widths,x,x-height,x1,x2,xlink:actuate,xlink:arcrole,xlink:role,xlink:show,xlink:title,xlink:type,xml:base,xml:lang,xml:space,xmlns,xmlns:xlink,y,y1,y2,zoomAndPan",
!0);var O=h.extend({},D,p,n),A=document.createElement("pre");h.module("ngSanitize",[]).provider("$sanitize",function(){this.$get=["$$sanitizeUri",function(a){return function(f){var d=[];F(f,r(d,function(c,b){return!/^unsafe/.test(a(c,b))}));return d.join("")}}]});h.module("ngSanitize").filter("linky",["$sanitize",function(a){var f=/((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"\u201d\u2019]/i,d=/^mailto:/i;return function(c,b){function k(a){a&&g.push(E(a))}function e(a,
c){g.push("<a ");h.isDefined(b)&&g.push('target="',b,'" ');g.push('href="',a.replace(/"/g,"&quot;"),'">');k(c);g.push("</a>")}if(!c)return c;for(var m,l=c,g=[],n,p;m=l.match(f);)n=m[0],m[2]||m[4]||(n=(m[3]?"http://":"mailto:")+n),p=m.index,k(l.substr(0,p)),e(n,m[0].replace(d,"")),l=l.substring(p+m[0].length);k(l);return a(g.join(""))}}])})(window,window.angular);
//# sourceMappingURL=angular-sanitize.min.js.map
;/**
 * AngularStrap - Twitter Bootstrap directives for AngularJS
 * @version v0.7.5 - 2013-07-21
 * @link http://mgcrea.github.com/angular-strap
 * @author Olivier Louvignes <olivier@mg-crea.com>
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
angular.module("$strap.config",[]).value("$strapConfig",{}),angular.module("$strap.filters",["$strap.config"]),angular.module("$strap.directives",["$strap.config"]),angular.module("$strap",["$strap.filters","$strap.directives","$strap.config"]),angular.module("$strap.directives").directive("bsAlert",["$parse","$timeout","$compile",function(t,e,n){return{restrict:"A",link:function(a,i,o){var r=t(o.bsAlert),s=(r.assign,r(a)),l=function(t){e(function(){i.alert("close")},1*t)};o.bsAlert?a.$watch(o.bsAlert,function(t,e){s=t,i.html((t.title?"<strong>"+t.title+"</strong>&nbsp;":"")+t.content||""),t.closed&&i.hide(),n(i.contents())(a),(t.type||e.type)&&(e.type&&i.removeClass("alert-"+e.type),t.type&&i.addClass("alert-"+t.type)),angular.isDefined(t.closeAfter)?l(t.closeAfter):o.closeAfter&&l(o.closeAfter),(angular.isUndefined(o.closeButton)||"0"!==o.closeButton&&"false"!==o.closeButton)&&i.prepend('<button type="button" class="close" data-dismiss="alert">&times;</button>')},!0):((angular.isUndefined(o.closeButton)||"0"!==o.closeButton&&"false"!==o.closeButton)&&i.prepend('<button type="button" class="close" data-dismiss="alert">&times;</button>'),o.closeAfter&&l(o.closeAfter)),i.addClass("alert").alert(),i.hasClass("fade")&&(i.removeClass("in"),setTimeout(function(){i.addClass("in")}));var u=o.ngRepeat&&o.ngRepeat.split(" in ").pop();i.on("close",function(t){var e;u?(t.preventDefault(),i.removeClass("in"),e=function(){i.trigger("closed"),a.$parent&&a.$parent.$apply(function(){for(var t=u.split("."),e=a.$parent,n=0;t.length>n;++n)e&&(e=e[t[n]]);e&&e.splice(a.$index,1)})},$.support.transition&&i.hasClass("fade")?i.on($.support.transition.end,e):e()):s&&(t.preventDefault(),i.removeClass("in"),e=function(){i.trigger("closed"),a.$apply(function(){s.closed=!0})},$.support.transition&&i.hasClass("fade")?i.on($.support.transition.end,e):e())})}}}]),angular.module("$strap.directives").directive("bsButton",["$parse","$timeout",function(t){return{restrict:"A",require:"?ngModel",link:function(e,n,a,i){if(i){n.parent('[data-toggle="buttons-checkbox"], [data-toggle="buttons-radio"]').length||n.attr("data-toggle","button");var o=!!e.$eval(a.ngModel);o&&n.addClass("active"),e.$watch(a.ngModel,function(t,e){var a=!!t,i=!!e;a!==i?$.fn.button.Constructor.prototype.toggle.call(r):a&&!o&&n.addClass("active")})}n.hasClass("btn")||n.on("click.button.data-api",function(){n.button("toggle")}),n.button();var r=n.data("button");r.toggle=function(){if(!i)return $.fn.button.Constructor.prototype.toggle.call(this);var a=n.parent('[data-toggle="buttons-radio"]');a.length?(n.siblings("[ng-model]").each(function(n,a){t($(a).attr("ng-model")).assign(e,!1)}),e.$digest(),i.$modelValue||(i.$setViewValue(!i.$modelValue),e.$digest())):e.$apply(function(){i.$setViewValue(!i.$modelValue)})}}}}]).directive("bsButtonsCheckbox",["$parse",function(){return{restrict:"A",require:"?ngModel",compile:function(t){t.attr("data-toggle","buttons-checkbox").find("a, button").each(function(t,e){$(e).attr("bs-button","")})}}}]).directive("bsButtonsRadio",["$timeout",function(t){return{restrict:"A",require:"?ngModel",compile:function(e,n){return e.attr("data-toggle","buttons-radio"),n.ngModel||e.find("a, button").each(function(t,e){$(e).attr("bs-button","")}),function(e,n,a,i){i&&(t(function(){n.find("[value]").button().filter('[value="'+i.$viewValue+'"]').addClass("active")}),n.on("click.button.data-api",function(t){e.$apply(function(){i.$setViewValue($(t.target).closest("button").attr("value"))})}),e.$watch(a.ngModel,function(t,i){if(t!==i){var o=n.find('[value="'+e.$eval(a.ngModel)+'"]');o.length&&o.button("toggle")}}))}}}}]),angular.module("$strap.directives").directive("bsButtonSelect",["$parse","$timeout",function(t){return{restrict:"A",require:"?ngModel",link:function(e,n,a,i){var o=t(a.bsButtonSelect);o.assign,i&&(n.text(e.$eval(a.ngModel)),e.$watch(a.ngModel,function(t){n.text(t)}));var r,s,l,u;n.bind("click",function(){r=o(e),s=i?e.$eval(a.ngModel):n.text(),l=r.indexOf(s),u=l>r.length-2?r[0]:r[l+1],e.$apply(function(){n.text(u),i&&i.$setViewValue(u)})})}}}]),angular.module("$strap.directives").directive("bsDatepicker",["$timeout","$strapConfig",function(t,e){var n=/(iP(a|o)d|iPhone)/g.test(navigator.userAgent),a=function a(t){return t=t||"en",{"/":"[\\/]","-":"[-]",".":"[.]"," ":"[\\s]",dd:"(?:(?:[0-2]?[0-9]{1})|(?:[3][01]{1}))",d:"(?:(?:[0-2]?[0-9]{1})|(?:[3][01]{1}))",mm:"(?:[0]?[1-9]|[1][012])",m:"(?:[0]?[1-9]|[1][012])",DD:"(?:"+$.fn.datepicker.dates[t].days.join("|")+")",D:"(?:"+$.fn.datepicker.dates[t].daysShort.join("|")+")",MM:"(?:"+$.fn.datepicker.dates[t].months.join("|")+")",M:"(?:"+$.fn.datepicker.dates[t].monthsShort.join("|")+")",yyyy:"(?:(?:[1]{1}[0-9]{1}[0-9]{1}[0-9]{1})|(?:[2]{1}[0-9]{3}))(?![[0-9]])",yy:"(?:(?:[0-9]{1}[0-9]{1}))(?![[0-9]])"}},i=function i(t,e){var n,i=t,o=a(e);return n=0,angular.forEach(o,function(t,e){i=i.split(e).join("${"+n+"}"),n++}),n=0,angular.forEach(o,function(t){i=i.split("${"+n+"}").join(t),n++}),RegExp("^"+i+"$",["i"])};return{restrict:"A",require:"?ngModel",link:function(t,a,o,r){var s=angular.extend({autoclose:!0},e.datepicker||{}),l=o.dateType||s.type||"date";angular.forEach(["format","weekStart","calendarWeeks","startDate","endDate","daysOfWeekDisabled","autoclose","startView","minViewMode","todayBtn","todayHighlight","keyboardNavigation","language","forceParse"],function(t){angular.isDefined(o[t])&&(s[t]=o[t])});var u=s.language||"en",c=o.dateFormat||s.format||$.fn.datepicker.dates[u]&&$.fn.datepicker.dates[u].format||"mm/dd/yyyy",d=n?"yyyy-mm-dd":c,p=i(d,u);r&&(r.$formatters.unshift(function(t){return"date"===l&&angular.isString(t)&&t?$.fn.datepicker.DPGlobal.parseDate(t,$.fn.datepicker.DPGlobal.parseFormat(c),u):t}),r.$parsers.unshift(function(t){return t?"date"===l&&angular.isDate(t)?(r.$setValidity("date",!0),t):angular.isString(t)&&p.test(t)?(r.$setValidity("date",!0),n?new Date(t):"string"===l?t:$.fn.datepicker.DPGlobal.parseDate(t,$.fn.datepicker.DPGlobal.parseFormat(d),u)):(r.$setValidity("date",!1),void 0):(r.$setValidity("date",!0),null)}),r.$render=function(){if(n){var t=r.$viewValue?$.fn.datepicker.DPGlobal.formatDate(r.$viewValue,$.fn.datepicker.DPGlobal.parseFormat(d),u):"";return a.val(t),t}return r.$viewValue||a.val(""),a.datepicker("update",r.$viewValue)}),n?a.prop("type","date").css("-webkit-appearance","textfield"):(r&&a.on("changeDate",function(e){t.$apply(function(){r.$setViewValue("string"===l?a.val():e.date)})}),a.datepicker(angular.extend(s,{format:d,language:u})),t.$on("$destroy",function(){var t=a.data("datepicker");t&&(t.picker.remove(),a.data("datepicker",null))}),o.$observe("startDate",function(t){a.datepicker("setStartDate",t)}),o.$observe("endDate",function(t){a.datepicker("setEndDate",t)}));var f=a.siblings('[data-toggle="datepicker"]');f.length&&f.on("click",function(){a.prop("disabled")||a.trigger("focus")})}}}]),angular.module("$strap.directives").directive("bsDropdown",["$parse","$compile","$timeout",function(t,e,n){var a=function(t,e){return e||(e=['<ul class="dropdown-menu" role="menu" aria-labelledby="drop1">',"</ul>"]),angular.forEach(t,function(t,n){if(t.divider)return e.splice(n+1,0,'<li class="divider"></li>');var i="<li"+(t.submenu&&t.submenu.length?' class="dropdown-submenu"':"")+">"+'<a tabindex="-1" ng-href="'+(t.href||"")+'"'+(t.click?'" ng-click="'+t.click+'"':"")+(t.target?'" target="'+t.target+'"':"")+(t.method?'" data-method="'+t.method+'"':"")+">"+(t.text||"")+"</a>";t.submenu&&t.submenu.length&&(i+=a(t.submenu).join("\n")),i+="</li>",e.splice(n+1,0,i)}),e};return{restrict:"EA",scope:!0,link:function(i,o,r){var s=t(r.bsDropdown),l=s(i);n(function(){!angular.isArray(l);var t=angular.element(a(l).join(""));t.insertAfter(o),e(o.next("ul.dropdown-menu"))(i)}),o.addClass("dropdown-toggle").attr("data-toggle","dropdown")}}}]),angular.module("$strap.directives").factory("$modal",["$rootScope","$compile","$http","$timeout","$q","$templateCache","$strapConfig",function(t,e,n,a,i,o,r){var s=function s(s){function l(s){var l=angular.extend({show:!0},r.modal,s),u=l.scope?l.scope:t.$new(),c=l.template;return i.when(o.get(c)||n.get(c,{cache:!0}).then(function(t){return t.data})).then(function(t){var n=c.replace(".html","").replace(/[\/|\.|:]/g,"-")+"-"+u.$id,i=$('<div class="modal hide" tabindex="-1"></div>').attr("id",n).addClass("fade").html(t);return l.modalClass&&i.addClass(l.modalClass),$("body").append(i),a(function(){e(i)(u)}),u.$modal=function(t){i.modal(t)},angular.forEach(["show","hide"],function(t){u[t]=function(){i.modal(t)}}),u.dismiss=u.hide,angular.forEach(["show","shown","hide","hidden"],function(t){i.on(t,function(e){u.$emit("modal-"+t,e)})}),i.on("shown",function(){$("input[autofocus], textarea[autofocus]",i).first().trigger("focus")}),i.on("hidden",function(){l.persist||u.$destroy()}),u.$on("$destroy",function(){i.remove()}),i.modal(l),i})}return new l(s)};return s}]).directive("bsModal",["$q","$modal",function(t,e){return{restrict:"A",scope:!0,link:function(n,a,i){var o={template:n.$eval(i.bsModal),persist:!0,show:!1,scope:n};angular.forEach(["modalClass","backdrop","keyboard"],function(t){angular.isDefined(i[t])&&(o[t]=i[t])}),t.when(e(o)).then(function(t){a.attr("data-target","#"+t.attr("id")).attr("data-toggle","modal")})}}}]),angular.module("$strap.directives").directive("bsNavbar",["$location",function(t){return{restrict:"A",link:function(e,n){e.$watch(function(){return t.path()},function(t){$("li[data-match-route]",n).each(function(e,n){var a=angular.element(n),i=a.attr("data-match-route"),o=RegExp("^"+i+"$",["i"]);o.test(t)?a.addClass("active").find(".collapse.in").collapse("hide"):a.removeClass("active")})})}}}]),angular.module("$strap.directives").directive("bsPopover",["$parse","$compile","$http","$timeout","$q","$templateCache",function(t,e,n,a,i,o){return $("body").on("keyup",function(t){27===t.keyCode&&$(".popover.in").each(function(){$(this).popover("hide")})}),{restrict:"A",scope:!0,link:function(r,s,l){var u=t(l.bsPopover),c=(u.assign,u(r)),d={};angular.isObject(c)&&(d=c),i.when(d.content||o.get(c)||n.get(c,{cache:!0})).then(function(t){angular.isObject(t)&&(t=t.data),l.unique&&s.on("show",function(){$(".popover.in").each(function(){var t=$(this),e=t.data("popover");e&&!e.$element.is(s)&&t.popover("hide")})}),l.hide&&r.$watch(l.hide,function(t,e){t?n.hide():t!==e&&n.show()}),l.show&&r.$watch(l.show,function(t,e){t?a(function(){n.show()}):t!==e&&n.hide()}),s.popover(angular.extend({},d,{content:t,html:!0}));var n=s.data("popover");n.hasContent=function(){return this.getTitle()||t},n.getPosition=function(){var t=$.fn.popover.Constructor.prototype.getPosition.apply(this,arguments);return e(this.$tip)(r),r.$digest(),this.$tip.data("popover",this),t},r.$popover=function(t){n(t)},angular.forEach(["show","hide"],function(t){r[t]=function(){n[t]()}}),r.dismiss=r.hide,angular.forEach(["show","shown","hide","hidden"],function(t){s.on(t,function(e){r.$emit("popover-"+t,e)})})})}}}]),angular.module("$strap.directives").directive("bsSelect",["$timeout",function(t){return{restrict:"A",require:"?ngModel",link:function(e,n,a,i){var o=e.$eval(a.bsSelect)||{};t(function(){n.selectpicker(o),n.next().removeClass("ng-scope")}),i&&e.$watch(a.ngModel,function(t,e){angular.equals(t,e)||n.selectpicker("refresh")})}}}]),angular.module("$strap.directives").directive("bsTabs",["$parse","$compile","$timeout",function(t,e,n){var a='<div class="tabs"><ul class="nav nav-tabs"><li ng-repeat="pane in panes" ng-class="{active:pane.active}"><a data-target="#{{pane.id}}" data-index="{{$index}}" data-toggle="tab">{{pane.title}}</a></li></ul><div class="tab-content" ng-transclude></div>';return{restrict:"A",require:"?ngModel",priority:0,scope:!0,template:a,replace:!0,transclude:!0,compile:function(){return function(e,a,i,o){var r=t(i.bsTabs);r.assign,r(e),e.panes=[];var s,l,u,c=a.find("ul.nav-tabs"),d=a.find("div.tab-content"),p=0;n(function(){d.find("[data-title], [data-tab]").each(function(t){var n=angular.element(this);s="tab-"+e.$id+"-"+t,l=n.data("title")||n.data("tab"),u=!u&&n.hasClass("active"),n.attr("id",s).addClass("tab-pane"),i.fade&&n.addClass("fade"),e.panes.push({id:s,title:l,content:this.innerHTML,active:u})}),e.panes.length&&!u&&(d.find(".tab-pane:first-child").addClass("active"+(i.fade?" in":"")),e.panes[0].active=!0)}),o&&(a.on("show",function(t){var n=$(t.target);e.$apply(function(){o.$setViewValue(n.data("index"))})}),e.$watch(i.ngModel,function(t){angular.isUndefined(t)||(p=t,setTimeout(function(){var e=$(c[0].querySelectorAll("li")[1*t]);e.hasClass("active")||e.children("a").tab("show")}))}))}}}}]),angular.module("$strap.directives").directive("bsTimepicker",["$timeout","$strapConfig",function(t,e){var n="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";return{restrict:"A",require:"?ngModel",link:function(a,i,o,r){if(r){i.on("changeTime.timepicker",function(){t(function(){r.$setViewValue(i.val())})});var s=RegExp("^"+n+"$",["i"]);r.$parsers.unshift(function(t){return!t||s.test(t)?(r.$setValidity("time",!0),t):(r.$setValidity("time",!1),void 0)})}i.attr("data-toggle","timepicker"),i.parent().addClass("bootstrap-timepicker"),i.timepicker(e.timepicker||{});var l=i.data("timepicker"),u=i.siblings('[data-toggle="timepicker"]');u.length&&u.on("click",$.proxy(l.showWidget,l))}}}]),angular.module("$strap.directives").directive("bsTooltip",["$parse","$compile",function(t){return{restrict:"A",scope:!0,link:function(e,n,a){var i=t(a.bsTooltip),o=(i.assign,i(e));e.$watch(a.bsTooltip,function(t,e){t!==e&&(o=t)}),a.unique&&n.on("show",function(){$(".tooltip.in").each(function(){var t=$(this),e=t.data("tooltip");e&&!e.$element.is(n)&&t.tooltip("hide")})}),n.tooltip({title:function(){return angular.isFunction(o)?o.apply(null,arguments):o},html:!0});var r=n.data("tooltip");r.show=function(){var t=$.fn.tooltip.Constructor.prototype.show.apply(this,arguments);return this.tip().data("tooltip",this),t},e._tooltip=function(t){n.tooltip(t)},e.hide=function(){n.tooltip("hide")},e.show=function(){n.tooltip("show")},e.dismiss=e.hide}}}]),angular.module("$strap.directives").directive("bsTypeahead",["$parse",function(t){return{restrict:"A",require:"?ngModel",link:function(e,n,a,i){var o=t(a.bsTypeahead),r=(o.assign,o(e));e.$watch(a.bsTypeahead,function(t,e){t!==e&&(r=t)}),n.attr("data-provide","typeahead"),n.typeahead({source:function(){return angular.isFunction(r)?r.apply(null,arguments):r},minLength:a.minLength||1,items:a.items,updater:function(t){return i&&e.$apply(function(){i.$setViewValue(t)}),e.$emit("typeahead-updated",t),t}});var s=n.data("typeahead");s.lookup=function(){var t;return this.query=this.$element.val()||"",this.query.length<this.options.minLength?this.shown?this.hide():this:(t=$.isFunction(this.source)?this.source(this.query,$.proxy(this.process,this)):this.source,t?this.process(t):this)},a.matchAll&&(s.matcher=function(){return!0}),"0"===a.minLength&&setTimeout(function(){n.on("focus",function(){0===n.val().length&&setTimeout(n.typeahead.bind(n,"lookup"),200)})})}}}]);;/*
 AngularJS v1.4.6
 (c) 2010-2015 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(x,s,y){'use strict';function t(f,k,p){n.directive(f,["$parse","$swipe",function(c,e){return function(l,m,g){function h(a){if(!b)return!1;var d=Math.abs(a.y-b.y);a=(a.x-b.x)*k;return r&&75>d&&0<a&&30<a&&.3>d/a}var d=c(g[f]),b,r,a=["touch"];s.isDefined(g.ngSwipeDisableMouse)||a.push("mouse");e.bind(m,{start:function(a,d){b=a;r=!0},cancel:function(a){r=!1},end:function(a,b){h(a)&&l.$apply(function(){m.triggerHandler(p);d(l,{$event:b})})}},a)}}])}var n=s.module("ngTouch",[]);n.factory("$swipe",
[function(){function f(c){c=c.originalEvent||c;var e=c.touches&&c.touches.length?c.touches:[c];c=c.changedTouches&&c.changedTouches[0]||e[0];return{x:c.clientX,y:c.clientY}}function k(c,e){var l=[];s.forEach(c,function(c){(c=p[c][e])&&l.push(c)});return l.join(" ")}var p={mouse:{start:"mousedown",move:"mousemove",end:"mouseup"},touch:{start:"touchstart",move:"touchmove",end:"touchend",cancel:"touchcancel"}};return{bind:function(c,e,l){var m,g,h,d,b=!1;l=l||["mouse","touch"];c.on(k(l,"start"),function(a){h=
f(a);b=!0;g=m=0;d=h;e.start&&e.start(h,a)});var r=k(l,"cancel");if(r)c.on(r,function(a){b=!1;e.cancel&&e.cancel(a)});c.on(k(l,"move"),function(a){if(b&&h){var c=f(a);m+=Math.abs(c.x-d.x);g+=Math.abs(c.y-d.y);d=c;10>m&&10>g||(g>m?(b=!1,e.cancel&&e.cancel(a)):(a.preventDefault(),e.move&&e.move(c,a)))}});c.on(k(l,"end"),function(a){b&&(b=!1,e.end&&e.end(f(a),a))})}}}]);n.config(["$provide",function(f){f.decorator("ngClickDirective",["$delegate",function(k){k.shift();return k}])}]);n.directive("ngClick",
["$parse","$timeout","$rootElement",function(f,k,p){function c(d,b,c){for(var a=0;a<d.length;a+=2){var e=d[a+1],g=c;if(25>Math.abs(d[a]-b)&&25>Math.abs(e-g))return d.splice(a,a+2),!0}return!1}function e(d){if(!(2500<Date.now()-m)){var b=d.touches&&d.touches.length?d.touches:[d],e=b[0].clientX,b=b[0].clientY;if(!(1>e&&1>b||h&&h[0]===e&&h[1]===b)){h&&(h=null);var a=d.target;"label"===s.lowercase(a.nodeName||a[0]&&a[0].nodeName)&&(h=[e,b]);c(g,e,b)||(d.stopPropagation(),d.preventDefault(),d.target&&
d.target.blur&&d.target.blur())}}}function l(d){d=d.touches&&d.touches.length?d.touches:[d];var b=d[0].clientX,c=d[0].clientY;g.push(b,c);k(function(){for(var a=0;a<g.length;a+=2)if(g[a]==b&&g[a+1]==c){g.splice(a,a+2);break}},2500,!1)}var m,g,h;return function(d,b,h){var a=f(h.ngClick),k=!1,q,n,t,v;b.on("touchstart",function(a){k=!0;q=a.target?a.target:a.srcElement;3==q.nodeType&&(q=q.parentNode);b.addClass("ng-click-active");n=Date.now();a=a.originalEvent||a;a=(a.touches&&a.touches.length?a.touches:
[a])[0];t=a.clientX;v=a.clientY});b.on("touchcancel",function(a){k=!1;b.removeClass("ng-click-active")});b.on("touchend",function(a){var d=Date.now()-n,f=a.originalEvent||a,u=(f.changedTouches&&f.changedTouches.length?f.changedTouches:f.touches&&f.touches.length?f.touches:[f])[0],f=u.clientX,u=u.clientY,w=Math.sqrt(Math.pow(f-t,2)+Math.pow(u-v,2));k&&750>d&&12>w&&(g||(p[0].addEventListener("click",e,!0),p[0].addEventListener("touchstart",l,!0),g=[]),m=Date.now(),c(g,f,u),q&&q.blur(),s.isDefined(h.disabled)&&
!1!==h.disabled||b.triggerHandler("click",[a]));k=!1;b.removeClass("ng-click-active")});b.onclick=function(a){};b.on("click",function(b,c){d.$apply(function(){a(d,{$event:c||b})})});b.on("mousedown",function(a){b.addClass("ng-click-active")});b.on("mousemove mouseup",function(a){b.removeClass("ng-click-active")})}}]);t("ngSwipeLeft",-1,"swipeleft");t("ngSwipeRight",1,"swiperight")})(window,window.angular);
//# sourceMappingURL=angular-touch.min.js.map
;/* jQuery Textarea Characters Counter Plugin v 2.0 Examples and documentation at: http://roy-jin.appspot.com/jsp/textareaCounter.jsp Copyright (c) 2010 Roy Jin Version: 2.0 (11-JUN-2010) Dual licensed under the MIT and GPL licenses: http://www.opensource.org/licenses/mit-license.php http://www.gnu.org/licenses/gpl.html Requires: jQuery v1.4.2 or later */
(function($){  
	$.fn.textareaCount = function(options, fn) {   
		var defaults = {  
			maxCharacterSize: -1,  
			originalStyle: 'originalTextareaInfo',
			warningStyle: 'warningTextareaInfo',  
			warningNumber: 20,
			displayFormat: '#input characters | #words words'
		};  
		var options = $.extend(defaults, options);
		
		var container = $(this);
		
		$("<span class='charleft'>&nbsp;</span>").insertAfter(container);
		
		//create charleft css
		var charLeftCss = {
			'float' : 'right',
			'text-transform' : 'uppercase',
			'font-size' : '0.9em'
		};
		
		var charLeftInfo = getNextCharLeftInformation(container);
		charLeftInfo.addClass(options.originalStyle);
		charLeftInfo.css(charLeftCss);
		
		var numInput = 0;
		var maxCharacters = options.maxCharacterSize;
		var numLeft = 0;
		var numWords = 0;
				
		container.bind('keyup', function(event){limitTextAreaByCharacterCount();})
				 .bind('mouseover', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);})
				 .bind('paste', function(event){setTimeout(function(){limitTextAreaByCharacterCount();}, 10);});
		
		
		function limitTextAreaByCharacterCount(){
			charLeftInfo.html(countByCharacters());
			//function call back
			if(typeof fn != 'undefined'){
				fn.call(this, getInfo());
			}
			return true;
		}
		
		function countByCharacters(){
			var content = container.val();
			var contentLength = content.length;
			
			//Start Cut
			if(options.maxCharacterSize > 0){
				//If copied content is already more than maxCharacterSize, chop it to maxCharacterSize.
				if(contentLength >= options.maxCharacterSize) {
					content = content.substring(0, options.maxCharacterSize); 				
				}
				
				var newlineCount = getNewlineCount(content);
				
				// newlineCount new line character. For windows, it occupies 2 characters
				var systemmaxCharacterSize = options.maxCharacterSize - newlineCount;
				if (!isWin()){
					 systemmaxCharacterSize = options.maxCharacterSize
				}
				if(contentLength > systemmaxCharacterSize){
					//avoid scroll bar moving
					var originalScrollTopPosition = this.scrollTop;
					container.val(content.substring(0, systemmaxCharacterSize));
					this.scrollTop = originalScrollTopPosition;
				}
				charLeftInfo.removeClass(options.warningStyle);
				if(systemmaxCharacterSize - contentLength <= options.warningNumber){
					charLeftInfo.addClass(options.warningStyle);
				}
				
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
			
				numWords = countWord(getCleanedWordString(container.val()));
				
				numLeft = maxCharacters - numInput;
			} else {
				//normal count, no cut
				var newlineCount = getNewlineCount(content);
				numInput = container.val().length + newlineCount;
				if(!isWin()){
					numInput = container.val().length;
				}
				numWords = countWord(getCleanedWordString(container.val()));
			}
			
			return formatDisplayInfo();
		}
		
		function formatDisplayInfo(){
			var format = options.displayFormat;
			format = format.replace('#input', numInput);
			format = format.replace('#words', numWords);
			//When maxCharacters <= 0, #max, #left cannot be substituted.
			if(maxCharacters > 0){
				format = format.replace('#max', maxCharacters);
				format = format.replace('#left', numLeft);
			}
			return format;
		}
		
		function getInfo(){
			var info = {
				input: numInput,
				max: maxCharacters,
				left: numLeft,
				words: numWords
			};
			return info;
		}
		
		function getNextCharLeftInformation(container){
				return container.next('.charleft');
		}
		
		function isWin(){
			var strOS = navigator.appVersion;
			if (strOS.toLowerCase().indexOf('win') != -1){
				return true;
			}
			return false;
		}
		
		function getNewlineCount(content){
			var newlineCount = 0;
			for(var i=0; i<content.length;i++){
				if(content.charAt(i) == '\n'){
					newlineCount++;
				}
			}
			return newlineCount;
		}
		
		function getCleanedWordString(content){
			var fullStr = content + " ";
			var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
			var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
			var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
			var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
			var splitString = cleanedStr.split(" ");
			return splitString;
		}
		
		function countWord(cleanedWordString){
			var word_count = cleanedWordString.length-1;
			return word_count;
		}
	};  
})(jQuery); ;'use strict';
 
/*
 * AngularJS Toaster
 * Version: 0.3
 *
 * Copyright 2013 Jiri Kavulak.  
 * All Rights Reserved.  
 * Use, reproduction, distribution, and modification of this code is subject to the terms and 
 * conditions of the MIT license, available at http://www.opensource.org/licenses/mit-license.php
 *
 * Author: Jiri Kavulak
 * Related to project of John Papa and Hans Fjällemark
 */
 
angular.module('toaster', ['ngAnimate'])
.service('toaster', ['$rootScope', function ($rootScope) {
    this.pop = function (type, title, body, timeout, trustedHtml) {
        this.toast = {
            type: type,
            title: title,
            body: body,
            timeout: timeout,
            trustedHtml: trustedHtml
        };
        $rootScope.$broadcast('toaster-newToast');
    };
}])
.constant('toasterConfig', {
  				'tap-to-dismiss': true,
					'newest-on-top': true,
					//'fade-in': 1000,            // done in css
					//'on-fade-in': undefined,    // not implemented
					//'fade-out': 1000,           // done in css
					// 'on-fade-out': undefined,  // not implemented
					//'extended-time-out': 1000,    // not implemented
					'time-out': 5000, // Set timeOut and extendedTimeout to 0 to make it sticky
					'icon-classes': {
						error: 'toast-error',
						info: 'toast-info',
						success: 'toast-success',
						warning: 'toast-warning'
					},
					'trustedHtml': false,
					'icon-class': 'toast-info',
					'position-class': 'toast-top-right',
					'title-class': 'toast-title',
					'message-class': 'toast-message'
				})
.directive('toasterContainer', ['$compile', '$timeout', '$sce', 'toasterConfig', 'toaster',
function ($compile, $timeout, $sce, toasterConfig, toaster) {
  return {
    replace: true,
    restrict: 'EA',
    link: function (scope, elm, attrs){
      
      var id = 0;
      
      var mergedConfig = toasterConfig;
      if (attrs.toasterOptions) {
          angular.extend(mergedConfig, scope.$eval(attrs.toasterOptions));
      }
      
      scope.config = {
          position: mergedConfig['position-class'],
          title: mergedConfig['title-class'],
          message: mergedConfig['message-class'],
          tap: mergedConfig['tap-to-dismiss']
      };
      
      function addToast (toast){
        toast.type = mergedConfig['icon-classes'][toast.type];
        if (!toast.type)
            toast.type = mergedConfig['icon-class'];
        
        id++;
        angular.extend(toast, { id: id });
        
        if (toast.trustedHtml){
          toast.html = $sce.trustAsHtml(toast.body);
        }
        
        var timeout = typeof(toast.timeout) == "number" ? toast.timeout : mergedConfig['time-out'];
        if (timeout > 0)
            setTimeout(toast, timeout);
        
        if (mergedConfig['newest-on-top'] === true)
            scope.toasters.unshift(toast);
        else
            scope.toasters.push(toast);
      }
      
      function setTimeout(toast, time){
          toast.timeout= $timeout(function (){ 
              scope.removeToast(toast.id);
            }, time);
      }
      
      scope.toasters = [];
      scope.$on('toaster-newToast', function () {
        addToast(toaster.toast);
      });
    },
    controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
      
      $scope.stopTimer = function(toast){
        if(toast.timeout)
          $timeout.cancel(toast.timeout);
      };
      
      $scope.removeToast = function (id){
        var i = 0;
        for (i; i < $scope.toasters.length; i++){
            if($scope.toasters[i].id === id)
                break;
        }
        $scope.toasters.splice(i, 1);
      };
      
      $scope.remove = function(id){
        if ($scope.config.tap === true){
            $scope.removeToast(id);
        }
      };
    }],
    template:
    '<div  id="toast-container" ng-class="config.position">' +
        '<div ng-repeat="toaster in toasters" class="toast" ng-class="toaster.type" ng-click="remove(toaster.id)" ng-mouseover="stopTimer(toaster)">' +
          '<div ng-class="config.title">{{toaster.title}}</div>' +
          '<div ng-class="config.message" ng-switch on="toaster.trustedHtml">' +
            '<div ng-switch-when="true" ng-bind-html="toaster.html"></div>' +
            '<div ng-switch-default >{{toaster.body}}</div>' +
          '</div>' +
        '</div>' +
    '</div>'
  };
}]);;/*
ngProgress 1.0.3 - slim, site-wide progressbar for AngularJS 
(C) 2013 - Victor Bjelkholm 
License: MIT 
Source: https://github.com/VictorBjelkholm/ngProgress 
Date Compiled: 2013-09-13 
*/
angular.module('ngProgress.provider', ['ngProgress.directive'])
    .provider('ngProgress', function () {
        'use strict';
        //Default values for provider
        this.autoStyle = true;
        this.count = 0;
        this.height = '6px';
        this.color = '#D94D48';

        this.$get = ['$document',
            '$window',
            '$compile',
            '$rootScope',
            '$timeout', function ($document, $window, $compile, $rootScope, $timeout) {
            var count = this.count,
                height = this.height,
                color = this.color,
                $scope = $rootScope,
                $body = $document.find('body');

            // Compile the directive
            var progressbarEl = $compile('<ng-progress></ng-progress>')($scope);
            // Add the element to body
            $body.append(progressbarEl);
            // Set the initial height
            $scope.count = count;
            // If height or color isn't undefined, set the height, background-color and color.
            if (height !== undefined) {
                progressbarEl.eq(0).children().css('height', height);
            }
            if (color !== undefined) {
                progressbarEl.eq(0).children().css('background-color', color);
                progressbarEl.eq(0).children().css('color', color);
            }
            // The ID for the interval controlling start()
            var intervalCounterId = 0;
            return {
                // Starts the animation and adds between 0 - 5 percent to loading
                // each 400 milliseconds. Should always be finished with progressbar.complete()
                // to hide it
                start: function () {
                    // TODO Use requestAnimationFrame instead of setInterval
                    // https://developer.mozilla.org/en-US/docs/Web/API/window.requestAnimationFrame
                    this.show();
                    var self = this;
                    intervalCounterId = setInterval(function () {
                        if (isNaN(count)) {
                            clearInterval(intervalCounterId);
                            count = 0;
                            self.hide();
                        } else {
                            var remaining = 100 - count;
                            count = count + (0.15 * Math.pow(1 - Math.sqrt(remaining), 2));
                            self.updateCount(count);
                        }
                    }, 200);
                },
                updateCount: function (new_count) {
                    $scope.count = new_count;
                    if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                },
                // Sets the height of the progressbar. Use any valid CSS value
                // Eg '10px', '1em' or '1%'
                height: function (new_height) {
                    if (new_height !== undefined) {
                        height = new_height;
                        $scope.height = height;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    return height;
                },
                // Sets the color of the progressbar and it's shadow. Use any valid HTML
                // color
                color: function (new_color) {
                    if (new_color !== undefined) {
                        color = new_color;
                        $scope.color = color;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    return color;
                },
                hide: function () {
                    progressbarEl.children().css('opacity', '0');
                    var self = this;
                    $timeout(function () {
                        progressbarEl.children().css('width', '0%');
                        $timeout(function () {
                            self.show();
                        }, 500);
                    }, 500);
                },
                show: function () {
                    $timeout(function () {
                        progressbarEl.children().css('opacity', '1');
                    }, 100);
                },
                // Returns on how many percent the progressbar is at. Should'nt be needed
                status: function () {
                    return count;
                },
                // Stops the progressbar at it's current location
                stop: function () {
                    clearInterval(intervalCounterId);
                },
                // Set's the progressbar percentage. Use a number between 0 - 100. 
                // If 100 is provided, complete will be called.
                set: function (new_count) {
                    this.show();
                    this.updateCount(new_count);
                    count = new_count;
                    clearInterval(intervalCounterId);
                    return count;
                },
                css: function (args) {
                    return progressbarEl.children().css(args);
                },
                // Resets the progressbar to percetage 0 and therefore will be hided after
                // it's rollbacked
                reset: function () {
                    clearInterval(intervalCounterId);
                    count = 0;
                    this.updateCount(count);
                    return 0;
                },
                // Jumps to 100% progress and fades away progressbar.
                complete: function () {
                    count = 100;
                    this.updateCount(count);
                    var self = this;
                    $timeout(function () {
                        self.hide();
                        $timeout(function () {
                            count = 0;
                            self.updateCount(count);
                        }, 500);
                    }, 1000);
                    return count;
                }
            };
        }];

        this.setColor = function (color) {
            if (color !== undefined) {
                this.color = color;
            }
            return this.color;
        };

        this.setHeight = function (height) {
            if (height !== undefined) {
                this.height = height;
            }
            return this.height;
        };
    });
angular.module('ngProgress.directive', [])
    .directive('ngProgress', ["$window", "$rootScope", function ($window, $rootScope) {
        var directiveObj = {
            // Replace the directive
            replace: true,
            // Only use as a element
            restrict: 'E',
            link: function ($scope, $element, $attrs, $controller) {
                // Watch the count on the $rootScope. As soon as count changes to something that
                // isn't undefined or null, change the counter on $scope and also the width of
                // the progressbar. The same goes for color and height on the $rootScope
                $rootScope.$watch('count', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.counter = newVal;
                        $element.eq(0).children().css('width', newVal + '%');
                    }
                });
                $rootScope.$watch('color', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.color = newVal;
                        $element.eq(0).children().css('background-color', newVal);
                        $element.eq(0).children().css('color', newVal);
                    }
                });
                $rootScope.$watch('height', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.height = newVal;
                        $element.eq(0).children().css('height', newVal);
                    }
                });
            },
            // The actual html that will be used
            template: '<div id="ngProgress-container"><div id="ngProgress"></div></div>'
        };
        return directiveObj;
    }]);

angular.module('ngProgress', ['ngProgress.directive', 'ngProgress.provider']);;/*
 Attaches input mask onto input element
 */
angular.module('ui.mask',[])
  .value('uiMaskConfig', {
    'maskDefinitions': {
      '9': /\d/,
      'A': /[a-zA-Z]/,
      '*': /[a-zA-Z0-9]/
    }
  })
  .directive('uiMask', ['uiMaskConfig', function (maskConfig) {
    return {
      priority: 100,
      require: 'ngModel',
      restrict: 'A',
      compile: function uiMaskCompilingFunction(){
        var options = maskConfig;
        
        return function uiMaskLinkingFunction(scope, iElement, iAttrs, controller){
          var maskProcessed = false, eventsBound = false,
            maskCaretMap, maskPatterns, maskPlaceholder, maskComponents,
          // Minimum required length of the value to be considered valid
            minRequiredLength,
            value, valueMasked, isValid,
          // Vars for initializing/uninitializing
            originalPlaceholder = iAttrs.placeholder,
            originalMaxlength = iAttrs.maxlength,
          // Vars used exclusively in eventHandler()
            oldValue, oldValueUnmasked, oldCaretPosition, oldSelectionLength;

          function initialize(maskAttr){
            if (!angular.isDefined(maskAttr)) {
              return uninitialize();
            }
            processRawMask(maskAttr);
            if (!maskProcessed) {
              return uninitialize();
            }
            initializeElement();
            bindEventListeners();
            return true;
          }

          function formatter(fromModelValue){
            if (!maskProcessed) {
              return fromModelValue;
            }
            value = unmaskValue(fromModelValue || '');
            isValid = validateValue(value);
            controller.$setValidity('mask', isValid);
            return isValid && value.length ? maskValue(value) : undefined;
          }

          function parser(fromViewValue){
            if (!maskProcessed) {
              return fromViewValue;
            }
            value = unmaskValue(fromViewValue || '');
            isValid = validateValue(value);
            // We have to set viewValue manually as the reformatting of the input
            // value performed by eventHandler() doesn't happen until after
            // this parser is called, which causes what the user sees in the input
            // to be out-of-sync with what the controller's $viewValue is set to.
            controller.$viewValue = value.length ? maskValue(value) : '';
            controller.$setValidity('mask', isValid);
            if (value === '' && controller.$error.required !== undefined) {
              controller.$setValidity('required', false);
            }
            return isValid ? value : undefined;
          }

          var linkOptions = {};
          
          if (iAttrs.uiOptions) {
            linkOptions = scope.$eval('[' + iAttrs.uiOptions + ']');
            if (angular.isObject(linkOptions[0])) { 
              // we can't use angular.copy nor angular.extend, they lack the power to do a deep merge
              linkOptions = (function(original, current){
                for(var i in original) {
                  if (Object.prototype.hasOwnProperty.call(original, i)) {
                    if (!current[i]) {
                      current[i] = angular.copy(original[i]);
                    } else {
                      angular.extend(current[i], original[i]);
                    }
                  }
                }
                return current;
              })(options, linkOptions[0]);
            }
          } else {
            linkOptions = options;
          }

          iAttrs.$observe('uiMask', initialize);
          controller.$formatters.push(formatter);
          controller.$parsers.push(parser);

          function uninitialize(){
            maskProcessed = false;
            unbindEventListeners();

            if (angular.isDefined(originalPlaceholder)) {
              iElement.attr('placeholder', originalPlaceholder);
            } else {
              iElement.removeAttr('placeholder');
            }

            if (angular.isDefined(originalMaxlength)) {
              iElement.attr('maxlength', originalMaxlength);
            } else {
              iElement.removeAttr('maxlength');
            }

            iElement.val(controller.$modelValue);
            controller.$viewValue = controller.$modelValue;
            return false;
          }

          function initializeElement(){
            value = oldValueUnmasked = unmaskValue(controller.$modelValue || '');
            valueMasked = oldValue = maskValue(value);
            isValid = validateValue(value);
            var viewValue = isValid && value.length ? valueMasked : '';
            if (iAttrs.maxlength) { // Double maxlength to allow pasting new val at end of mask
              iElement.attr('maxlength', maskCaretMap[maskCaretMap.length - 1] * 2);
            }
            iElement.attr('placeholder', maskPlaceholder);
            iElement.val(viewValue);
            controller.$viewValue = viewValue;
            // Not using $setViewValue so we don't clobber the model value and dirty the form
            // without any kind of user interaction.
          }

          function bindEventListeners(){
            if (eventsBound) {
              return;
            }
            iElement.bind('blur', blurHandler);
            iElement.bind('mousedown mouseup', mouseDownUpHandler);
            iElement.bind('input keyup click', eventHandler);
            eventsBound = true;
          }

          function unbindEventListeners(){
            if (!eventsBound) {
              return;
            }
            iElement.unbind('blur', blurHandler);
            iElement.unbind('mousedown', mouseDownUpHandler);
            iElement.unbind('mouseup', mouseDownUpHandler);
            iElement.unbind('input', eventHandler);
            iElement.unbind('keyup', eventHandler);
            iElement.unbind('click', eventHandler);
            eventsBound = false;
          }

          function validateValue(value){
            // Zero-length value validity is ngRequired's determination
            return value.length ? value.length >= minRequiredLength : true;
          }

          function unmaskValue(value){
            var valueUnmasked = '',
              maskPatternsCopy = maskPatterns.slice();
            // Preprocess by stripping mask components from value
            value = value.toString();
            angular.forEach(maskComponents, function (component){
              value = value.replace(component, '');
            });
            angular.forEach(value.split(''), function (chr){
              if (maskPatternsCopy.length && maskPatternsCopy[0].test(chr)) {
                valueUnmasked += chr;
                maskPatternsCopy.shift();
              }
            });
            return valueUnmasked;
          }

          function maskValue(unmaskedValue){
            var valueMasked = '',
              maskCaretMapCopy = maskCaretMap.slice();
            
            angular.forEach(maskPlaceholder.split(''), function (chr, i){
              if (unmaskedValue.length && i === maskCaretMapCopy[0]) {
                valueMasked += unmaskedValue.charAt(0) || '_';
                unmaskedValue = unmaskedValue.substr(1);
                maskCaretMapCopy.shift();
              }
              else {
                valueMasked += chr;
              }
            });
            return valueMasked;
          }

          function processRawMask(mask){
            var characterCount = 0;
            maskCaretMap = [];
            maskPatterns = [];
            maskPlaceholder = '';

            // No complex mask support for now...
            // if (mask instanceof Array) {
            //   angular.forEach(mask, function(item, i) {
            //     if (item instanceof RegExp) {
            //       maskCaretMap.push(characterCount++);
            //       maskPlaceholder += '_';
            //       maskPatterns.push(item);
            //     }
            //     else if (typeof item == 'string') {
            //       angular.forEach(item.split(''), function(chr, i) {
            //         maskPlaceholder += chr;
            //         characterCount++;
            //       });
            //     }
            //   });
            // }
            // Otherwise it's a simple mask
            // else

            if (typeof mask === 'string') {
              minRequiredLength = 0;
              var isOptional = false;

              angular.forEach(mask.split(''), function (chr){
                if (linkOptions.maskDefinitions[chr]) {
                  maskCaretMap.push(characterCount);
                  maskPlaceholder += '_';
                  maskPatterns.push(linkOptions.maskDefinitions[chr]);

                  characterCount++;
                  if (!isOptional) {
                    minRequiredLength++;
                  }
                }
                else if (chr === "?") {
                  isOptional = true;
                }
                else {
                  maskPlaceholder += chr;
                  characterCount++;
                }
              });
            }
            // Caret position immediately following last position is valid.
            maskCaretMap.push(maskCaretMap.slice().pop() + 1);
            // Generate array of mask components that will be stripped from a masked value
            // before processing to prevent mask components from being added to the unmasked value.
            // E.g., a mask pattern of '+7 9999' won't have the 7 bleed into the unmasked value.
            // If a maskable char is followed by a mask char and has a mask
            // char behind it, we'll split it into it's own component so if
            // a user is aggressively deleting in the input and a char ahead
            // of the maskable char gets deleted, we'll still be able to strip
            // it in the unmaskValue() preprocessing.
            maskComponents = maskPlaceholder.replace(/[_]+/g, '_').replace(/([^_]+)([a-zA-Z0-9])([^_])/g, '$1$2_$3').split('_');
            maskProcessed = maskCaretMap.length > 1 ? true : false;
          }

          function blurHandler(){
            oldCaretPosition = 0;
            oldSelectionLength = 0;
            if (!isValid || value.length === 0) {
              valueMasked = '';
              iElement.val('');
              scope.$apply(function (){
                controller.$setViewValue('');
              });
            }
          }

          function mouseDownUpHandler(e){
            if (e.type === 'mousedown') {
              iElement.bind('mouseout', mouseoutHandler);
            } else {
              iElement.unbind('mouseout', mouseoutHandler);
            }
          }

          iElement.bind('mousedown mouseup', mouseDownUpHandler);

          function mouseoutHandler(){
            oldSelectionLength = getSelectionLength(this);
            iElement.unbind('mouseout', mouseoutHandler);
          }

          function eventHandler(e){
            e = e || {};
            // Allows more efficient minification
            var eventWhich = e.which,
              eventType = e.type;

            // Prevent shift and ctrl from mucking with old values
            if (eventWhich === 16 || eventWhich === 91) { return;}

            var val = iElement.val(),
              valOld = oldValue,
              valMasked,
              valUnmasked = unmaskValue(val),
              valUnmaskedOld = oldValueUnmasked,
              valAltered = false,

              caretPos = getCaretPosition(this) || 0,
              caretPosOld = oldCaretPosition || 0,
              caretPosDelta = caretPos - caretPosOld,
              caretPosMin = maskCaretMap[0],
              caretPosMax = maskCaretMap[valUnmasked.length] || maskCaretMap.slice().shift(),

              selectionLenOld = oldSelectionLength || 0,
              isSelected = getSelectionLength(this) > 0,
              wasSelected = selectionLenOld > 0,

            // Case: Typing a character to overwrite a selection
              isAddition = (val.length > valOld.length) || (selectionLenOld && val.length > valOld.length - selectionLenOld),
            // Case: Delete and backspace behave identically on a selection
              isDeletion = (val.length < valOld.length) || (selectionLenOld && val.length === valOld.length - selectionLenOld),
              isSelection = (eventWhich >= 37 && eventWhich <= 40) && e.shiftKey, // Arrow key codes

              isKeyLeftArrow = eventWhich === 37,
            // Necessary due to "input" event not providing a key code
              isKeyBackspace = eventWhich === 8 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === -1)),
              isKeyDelete = eventWhich === 46 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === 0 ) && !wasSelected),

            // Handles cases where caret is moved and placed in front of invalid maskCaretMap position. Logic below
            // ensures that, on click or leftward caret placement, caret is moved leftward until directly right of
            // non-mask character. Also applied to click since users are (arguably) more likely to backspace
            // a character when clicking within a filled input.
              caretBumpBack = (isKeyLeftArrow || isKeyBackspace || eventType === 'click') && caretPos > caretPosMin;

            oldSelectionLength = getSelectionLength(this);

            // These events don't require any action
            if (isSelection || (isSelected && (eventType === 'click' || eventType === 'keyup'))) {
              return;
            }

            // Value Handling
            // ==============

            // User attempted to delete but raw value was unaffected--correct this grievous offense
            if ((eventType === 'input') && isDeletion && !wasSelected && valUnmasked === valUnmaskedOld) {
              while (isKeyBackspace && caretPos > caretPosMin && !isValidCaretPosition(caretPos)) {
                caretPos--;
              }
              while (isKeyDelete && caretPos < caretPosMax && maskCaretMap.indexOf(caretPos) === -1) {
                caretPos++;
              }
              var charIndex = maskCaretMap.indexOf(caretPos);
              // Strip out non-mask character that user would have deleted if mask hadn't been in the way.
              valUnmasked = valUnmasked.substring(0, charIndex) + valUnmasked.substring(charIndex + 1);
              valAltered = true;
            }

            // Update values
            valMasked = maskValue(valUnmasked);
            oldValue = valMasked;
            oldValueUnmasked = valUnmasked;
            iElement.val(valMasked);
            if (valAltered) {
              // We've altered the raw value after it's been $digest'ed, we need to $apply the new value.
              scope.$apply(function (){
                controller.$setViewValue(valUnmasked);
              });
            }

            // Caret Repositioning
            // ===================

            // Ensure that typing always places caret ahead of typed character in cases where the first char of
            // the input is a mask char and the caret is placed at the 0 position.
            if (isAddition && (caretPos <= caretPosMin)) {
              caretPos = caretPosMin + 1;
            }

            if (caretBumpBack) {
              caretPos--;
            }

            // Make sure caret is within min and max position limits
            caretPos = caretPos > caretPosMax ? caretPosMax : caretPos < caretPosMin ? caretPosMin : caretPos;

            // Scoot the caret back or forth until it's in a non-mask position and within min/max position limits
            while (!isValidCaretPosition(caretPos) && caretPos > caretPosMin && caretPos < caretPosMax) {
              caretPos += caretBumpBack ? -1 : 1;
            }

            if ((caretBumpBack && caretPos < caretPosMax) || (isAddition && !isValidCaretPosition(caretPosOld))) {
              caretPos++;
            }
            oldCaretPosition = caretPos;
            setCaretPosition(this, caretPos);
          }

          function isValidCaretPosition(pos){ return maskCaretMap.indexOf(pos) > -1; }

          function getCaretPosition(input){
            if (input.selectionStart !== undefined) {
              return input.selectionStart;
            } else if (document.selection) {
              // Curse you IE
              input.focus();
              var selection = document.selection.createRange();
              selection.moveStart('character', -input.value.length);
              return selection.text.length;
            }
            return 0;
          }

          function setCaretPosition(input, pos){
            if (input.offsetWidth === 0 || input.offsetHeight === 0) {
              return; // Input's hidden
            }
            if (input.setSelectionRange) {
              input.focus();
              input.setSelectionRange(pos, pos);
            }
            else if (input.createTextRange) {
              // Curse you IE
              var range = input.createTextRange();
              range.collapse(true);
              range.moveEnd('character', pos);
              range.moveStart('character', pos);
              range.select();
            }
          }

          function getSelectionLength(input){
            if (input.selectionStart !== undefined) {
              return (input.selectionEnd - input.selectionStart);
            }
            if (document.selection) {
              return (document.selection.createRange().text.length);
            }
            return 0;
          }

          // https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf
          if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (searchElement /*, fromIndex */){
              "use strict";
              if (this === null) {
                throw new TypeError();
              }
              var t = Object(this);
              var len = t.length >>> 0;
              if (len === 0) {
                return -1;
              }
              var n = 0;
              if (arguments.length > 1) {
                n = Number(arguments[1]);
                if (n !== n) { // shortcut for verifying if it's NaN
                  n = 0;
                } else if (n !== 0 && n !== Infinity && n !== -Infinity) {
                  n = (n > 0 || -1) * Math.floor(Math.abs(n));
                }
              }
              if (n >= len) {
                return -1;
              }
              var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
              for (; k < len; k++) {
                if (k in t && t[k] === searchElement) {
                  return k;
                }
              }
              return -1;
            };
          }

        };
      }
    };
  }
]);;/* -----------------------------------------------------------------
	App.js: contains routing setup
------------------------------------------------------------------- */

var reddonor = angular.module('reddonor', ['ngRoute','ngProgress','ngSanitize','ngAnimate','toaster','ui.mask','$strap.directives']);

reddonor.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
					$routeProvider
						.when('/', { title: 'You Can Save A Life', templateUrl: '/partials/home', controller: 'WelcomeCtrl' })
						.when('/login', { title: 'Login', templateUrl: '/partials/login', controller: 'LoginCtrl' })
						.when('/about', { title: 'About Us', templateUrl: '/partials/aboutreddonor' })
						.when('/howitworks', { title: 'How It Works', templateUrl: '/partials/howitworks' })
						.when('/faqs', { title: 'FAQs', templateUrl: '/partials/faqs' })
						.when('/termsofuse', { title: 'Terms of Use', templateUrl: '/partials/terms' })
						.when('/privacy', { title: 'Privacy Policy', templateUrl: '/partials/privacy' })
						.when('/contact', { title: 'Contact Us', templateUrl: '/partials/contact', controller: 'ContactCtrl' })
						.when('/signup', { title: 'Sign Up', templateUrl: '/partials/signup', controller: 'SignUpCtrl' })
						.when('/drive/create', { title: 'Create Blood Drive', templateUrl: '/sessions/drive-add', controller: 'AddDriveCtrl' })
						.when('/create', { title: 'Create Blood Drive', templateUrl: '/partials/drive-add', controller: 'CreateDriveCtrl' })
						.when('/guestsignup', { title: 'Sign Up', templateUrl: '/partials/guest-signup', controller: 'GuestSignUpCtrl' })
						.when('/drive/settings/:did', { title: 'Edit Blood Drive', templateUrl: '/sessions/drive-edit', controller: 'EditDriveCtrl' })
						.when('/drives/list', { templateUrl: '/partials/drive-list', controller: 'ListDrivePagesCtrl' })
						.when('/drive/:did/:title', {  templateUrl: '/partials/drive-detail', controller: 'DriveReadCtrl' })
						.when('/drives/soon-ending', { title: 'Soon Ending Drives', templateUrl: '/partials/drive-soon', controller: 'SoonEndPagesCtrl' })
						.when('/drives/country/:loc', { title: 'Discover Drives', templateUrl: '/partials/drive-location', controller: 'DriveLocPagesCtrl' })
						.when('/drives/most-recent', { title: 'Most Recent Drives', templateUrl: '/partials/drive-most-recent', controller: 'MostRecentPagesCtrl' })
						.when('/drives/compatible', { title: 'Compatible Drives', templateUrl: '/sessions/drive-list-compat', controller: 'CompatPagesCtrl' })
						.when('/donors/signup', { title: 'Blood Donor Signup', templateUrl: '/partials/list-signup', controller: 'DonorSignUpCtrl' })
						.when('/drives/drive-completed', { title: 'Completed Drives', templateUrl: '/partials/drive-ended', controller: 'EndDrivePagesCtrl' })
						.when('/drives/:cat', { templateUrl: '/partials/drive-list-type', controller: 'DriveCatPagesCtrl' })
						.when('/drives/:page', { title: 'Discover Drives', templateUrl: '/partials/drive-list-type', controller: 'DriveCatPagesCtrl' })
						.when('/org/:oid/:title', { title: 'Organisation', templateUrl: '/partials/org-profile', controller: 'OrgProfilePages' })
						.when('/centres/:bid/:title', { title: 'Blood Banks', templateUrl: '/partials/bloodbank-profile', controller: 'BloodBankPages' })
						.when('/profile/history', { title: 'Donation History', templateUrl: '/sessions/donation-history', controller: 'HistoryCtrl' })
						.when('/profile/drives', { title: 'My Drives', templateUrl: '/sessions/my-drives', controller: 'MyDrivesCtrl' })
						.when('/profile/appointments', { title: 'My Appointments', templateUrl: '/sessions/appointment-history', controller: 'ApptsCtrl' })
						.when('/profile/settings', { title: 'Settings', templateUrl: '/sessions/settings', controller: 'ProfileSettingsCtrl' })
						.when('/profile', { title: 'Profile', templateUrl: '/sessions/profile', controller: 'ProfileCtrl' })
						.when('/reset-password/:token', {  title: 'Reset Password', templateUrl: '/partials/reset-password', controller: 'ResetCtrl' })
						.when('/reset-password', { title: 'Reset Password', templateUrl: '/partials/reset-password', controller: 'ResetCtrl' })
						.when('/centre/list', { title: 'Donation Centres', templateUrl: '/partials/donation-centres', controller: 'CentresCtrl' })
						.when('/centre/:loc', { title: 'Donation Centres', templateUrl: '/partials/donation-centre-country', controller: 'CentresCountryCtrl' })

						.otherwise({redirectTo:'/'});
					$locationProvider.html5Mode({
						enabled: true,
						requireBase: false
					});
}]);

/* ---------- Dynamic Title ---------- */
/* takes the title from the route scope to bind it to root scope to use as the page title */

reddonor.run(['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll) {
	$rootScope.page = {
		setTitle: function(title) {
			this.title = title + ' | Red Donor';
		}
	};
	$rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
		$rootScope.page.setTitle(current.$$route.title || 'Red Donor');
		//$location.hash('body');
		$anchorScroll();
	});
}]);
;/* Base Controller */
reddonor.controller('BaseCtrl', [
  '$scope',
  function ($scope) {
    $scope.name = 'Samora';
  }
]);/******************************************************************************************************************
 Blood Bank Controller
 - 
******************************************************************************************************************/
reddonor.controller('BloodBankPages', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  'toaster',
  function ($scope, $http, $location, $routeParams, toaster) {
    /**********************************************************
	Appointment Scheduling Modal
**********************************************************/
    $scope.steps = [
      'one',
      'two'
    ];
    $scope.step = 0;
    $scope.maxappt = 4;
    $scope.apptfull = false;
    ///$scope.schedule = {};
    $scope.schedule;
    //$scope.schedule.apptSession;
    //$scope.schedule.apptStatus = 7;
    //$scope.schedule.apptType = 1;
    $scope.bloodtypes = [
      {
        id: 1,
        name: 'O+'
      },
      {
        id: 2,
        name: 'O-'
      },
      {
        id: 3,
        name: 'A+'
      },
      {
        id: 4,
        name: 'A-'
      },
      {
        id: 5,
        name: 'B+'
      },
      {
        id: 6,
        name: 'B-'
      },
      {
        id: 7,
        name: 'AB+'
      },
      {
        id: 8,
        name: 'AB-'
      },
      {
        id: 9,
        name: 'Unknown'
      }
    ];
    $scope.times = [
      {
        time_value: '1',
        time_name: 'Early Morning'
      },
      {
        time_value: '2',
        time_name: 'Mid Morning'
      },
      {
        time_value: '3',
        time_name: 'Early Afternoon'
      },
      {
        time_value: '4',
        time_name: 'Late Afternoon'
      }
    ];
    $scope.isCurrentStep = function (step) {
      return $scope.step === step;
    };
    $scope.setCurrentStep = function (step) {
      $scope.step = step;
    };
    $scope.getCurrentStep = function () {
      return $scope.steps[$scope.step];
    };
    $scope.isFirstStep = function () {
      return $scope.step === 0;
    };
    $scope.isLastStep = function () {
      return $scope.step === $scope.steps.length - 1;
    };
    $scope.getNextLabel = function () {
      return $scope.isLastStep() ? 'Confirm' : 'Next';
    };
    $scope.handlePrevious = function () {
      $scope.step -= $scope.isFirstStep() ? 0 : 1;
    };
    $scope.handleNext = function (dismiss) {
      if ($scope.isLastStep()) {
        $scope.makeAppt();
        dismiss();
        $scope.handlePrevious();
      } else {
        $scope.step += 1;
      }
    };
    $scope.makeAppt = function () {
      $scope.schedule.apptTime = $scope.apptTime;
      $http.post('/api/add-appointment/' + $routeParams.bid, $scope.schedule).success(function (data) {
        console.log('data sent');
      });
      $scope.schedule.apptDate = '';
      $scope.appt = '';
      $scope.checkSchedule();
    };
    $http.get('/api/profile-info/').success(function (data) {
      $scope.profiles = data;  //console.log(data);
    });
    $scope.getAppts = function () {
      //console.log($scope.schedule.apptDate);
      $scope.apptmsg = false;
      $routeParams.date = $scope.schedule.apptDate;
      //console.log("routes " + $routeParams.date);
      $http.post('/api/appointments/' + $routeParams.bid, $scope.schedule).success(function (data) {
        if (data.dategone) {
          console.log('The date has passed!');
          $scope.appt = '';
          $scope.apptend = true;
          $scope.apptmsg = true;
          $scope.appterror = 'Sorry, that date has already passed.';
        } else {
          $scope.appt = data;
          console.log(data);
          $scope.apptend = false;
          $scope.apptmsg = true;
          $scope.maxappt === $scope.appt.time1 ? $scope.apptfull1 = true : $scope.apptfull1 = false;
          $scope.maxappt === $scope.appt.time2 ? $scope.apptfull2 = true : $scope.apptfull2 = false;
          $scope.maxappt === $scope.appt.time3 ? $scope.apptfull3 = true : $scope.apptfull3 = false;
          $scope.maxappt === $scope.appt.time4 ? $scope.apptfull4 = true : $scope.apptfull4 = false;
        }
      });
      console.log($scope.schedule);
    };
    $scope.selectSession = function (time) {
      $scope.apptTime = time;
      if (time === 1)
        $scope.schedule.apptSession = 'early morning';
      if (time === 2)
        $scope.schedule.apptSession = 'mid morning';
      if (time === 3)
        $scope.schedule.apptSession = 'early afternoon';
      if (time === 4)
        $scope.schedule.apptSession = 'late afternoon';
      $scope.handleNext();
    };
    $scope.tooltip = { 'title': 'Select Session' };
    $scope.popup = function () {
      return Flash.pop({
        'title': $scope.error,
        'type': 'error'
      });
    };
    $scope.scheduledtooltip = { 'title': 'You have donation scheduled! </br> Click for your next appointment.' };
    $scope.checksignin = function () {
      $scope.error = 'Please <a href=\'/login\'> Login </a><span> or </span><a href=\'/signup\'> Sign Up </a>';
      $scope.popup();
    };
    $scope.checkSchedule = function () {
      // Get Schedule Donation Button Info
      $http.get('/api/schedule-check').success(function (data) {
        console.log(data);
        if (data.nosession) {
          $scope.donatestatus = 'Donate Blood';
          $scope.notsigned = true;
          $scope.scheduleaction = false;
          $scope.scheduled = false;
        }
        if (data[0].apptexists) {
          $scope.donatestatus = 'Blood Donation Scheduled';
          $scope.scheduled = true;
          $scope.scheduleaction = false;
        }
        if (data[0].apptfree) {
          $scope.donatestatus = 'Donate Blood';
          $scope.scheduleaction = true;
          $scope.scheduled = false;
        }
      });
    };
    /**********************************************************
	Blood Bank Location
**********************************************************/
    //$scope.bvalue = "13.092685,-59.607477";
    //$scope.bbloc = new google.maps.LatLng(16,-70);
    $scope.init = function () {
      $http.get('/api/bloodbank-profile/' + $routeParams.bid).success(function (data) {
        $scope.bankprofile = data;
        //addMarker(data);
        $scope.page.setTitle($scope.bankprofile[0].location_name + ' > ' + $scope.bankprofile[0].country_name);
        //$scope.bbloc = new google.maps.LatLng(data[0].location_lat, data[0].location_lng);
        //console.log(data);
        //console.log(data[0].location_lat);
        $scope.checkSchedule();
      });
    };
    $scope.init();
    /*
	var addMarker = function(object){
			var lat = object[0].location_lat;
			var lng = object[0].location_lng;
			//console.log(lat);
			//console.log(lng);
			var centre = new google.maps.LatLng(lat, lng);
			var pos = new google.maps.LatLng(lat, lng);
			var marker = new google.maps.Marker({
				position: pos,
				title: object[0].location_name,
				map: $scope.myMap
			 
			});
				$scope.myMarkers = [marker, ];
				$scope.bbloc = new google.maps.LatLng(lat, lng);
		
		};

	 
	$scope.mapOptions = {
			center: $scope.bbloc,
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	$scope.openMarkerInfo = function(marker) {
		$scope.currentMarker = marker;
		$scope.currentMarkerLat = marker.getPosition().lat();
		$scope.currentMarkerLng = marker.getPosition().lng();
		//$scope.myInfoWindow.open($scope.myMap, marker);
		};

	

		//Markers should be added after map is loaded
	/*	$scope.onMapIdle = function() {
		if ($scope.myMarkers === undefined){    
			var marker = new google.maps.Marker({
				map: $scope.myMap,
				position: $scope.bbloc,
			});
			$scope.myMarkers = [marker, ];
		}
		}; */
    /**********************************************************
	Blood Bank Blood Levels
**********************************************************/
    $scope.opos = '';
    $scope.oneg = '';
    $scope.apos = '';
    $scope.aneg = '';
    $scope.bpos = '';
    $scope.bneg = '';
    $scope.abpos = '';
    $scope.abneg = '';
    $scope.opostext = '';
    $scope.onegtext = '';
    $scope.apostext = '';
    $scope.anegtext = '';
    $scope.bpostext = '';
    $scope.bnegtext = '';
    $scope.abpostext = '';
    $scope.abnegtext = '';
    $scope.statuses = [
      'danger',
      'warning',
      'info',
      'success'
    ];
    $http.get('/api/bloodbank-levels/' + $routeParams.bid).success(function (data) {
      $scope.banklevels = data;
      if ($scope.banklevels[0].oposlevel < 25) {
        $scope.opos = 'danger';
        $scope.opostext = 'Critical';
      } else if ($scope.banklevels[0].oposlevel < 50) {
        $scope.opos = 'warning';
        $scope.opostext = 'Low';
      } else if ($scope.banklevels[0].oposlevel < 75) {
        $scope.opos = 'info';
        $scope.opostext = 'Moderate';
      } else {
        $scope.opos = 'success';
        $scope.opostext = 'High';
      }
      if ($scope.banklevels[0].oneglevel < 25) {
        $scope.oneg = 'danger';
        $scope.onegtext = 'Critical';
      } else if ($scope.banklevels[0].oneglevel < 50) {
        $scope.oneg = 'warning';
        $scope.onegtext = 'Low';
      } else if ($scope.banklevels[0].oneglevel < 75) {
        $scope.oneg = 'info';
        $scope.onegtext = 'Moderate';
      } else {
        $scope.oneg = 'success';
        $scope.onegtext = 'High';
      }
      if ($scope.banklevels[0].aposlevel < 25) {
        $scope.apos = 'danger';
        $scope.apostext = 'Critical';
      } else if ($scope.banklevels[0].aposlevel < 50) {
        $scope.apos = 'warning';
        $scope.apostext = 'Low';
      } else if ($scope.banklevels[0].aposlevel < 75) {
        $scope.apos = 'info';
        $scope.apostext = 'Moderate';
      } else {
        $scope.apos = 'success';
        $scope.apostext = 'High';
      }
      if ($scope.banklevels[0].aneglevel < 25) {
        $scope.aneg = 'danger';
        $scope.anegtext = 'Critical';
      } else if ($scope.banklevels[0].aneglevel < 50) {
        $scope.aneg = 'warning';
        $scope.anegtext = 'Low';
      } else if ($scope.banklevels[0].aneglevel < 75) {
        $scope.aneg = 'info';
        $scope.anegtext = 'Moderate';
      } else {
        $scope.aneg = 'success';
        $scope.anegtext = 'High';
      }
      if ($scope.banklevels[0].bposlevel < 25) {
        $scope.bpos = 'danger';
        $scope.bpostext = 'Critical';
      } else if ($scope.banklevels[0].bposlevel < 50) {
        $scope.bpos = 'warning';
        $scope.bpostext = 'Low';
      } else if ($scope.banklevels[0].bposlevel < 75) {
        $scope.bpos = 'info';
        $scope.bpostext = 'Moderate';
      } else {
        $scope.bpos = 'success';
        $scope.bpostext = 'High';
      }
      if ($scope.banklevels[0].bneglevel < 25) {
        $scope.bneg = 'danger';
        $scope.bnegtext = 'Critical';
      } else if ($scope.banklevels[0].bneglevel < 50) {
        $scope.bneg = 'warning';
        $scope.bnegtext = 'Low';
      } else if ($scope.banklevels[0].bneglevel < 75) {
        $scope.bneg = 'info';
        $scope.bnegtext = 'Moderate';
      } else {
        $scope.bneg = 'success';
        $scope.bnegtext = 'High';
      }
      if ($scope.banklevels[0].abposlevel < 25) {
        $scope.abpos = 'danger';
        $scope.abpostext = 'Critical';
      } else if ($scope.banklevels[0].abposlevel < 50) {
        $scope.abpos = 'warning';
        $scope.abpostext = 'Low';
      } else if ($scope.banklevels[0].abposlevel < 75) {
        $scope.abpos = 'info';
        $scope.abpostext = 'Moderate';
      } else {
        $scope.abpos = 'success';
        $scope.abpostext = 'High';
      }
      if ($scope.banklevels[0].abneglevel < 25) {
        $scope.abneg = 'danger';
        $scope.abnegtext = 'Critical';
      } else if ($scope.banklevels[0].abneglevel < 50) {
        $scope.abneg = 'warning';
        $scope.abnegtext = 'Low';
      } else if ($scope.banklevels[0].abneglevel < 75) {
        $scope.abneg = 'info';
        $scope.abnegtext = 'Moderate';
      } else {
        $scope.abneg = 'success';
        $scope.abnegtext = 'High';
      }
    });
    /**********************************************************
	Blood Bank Charts

	- Display charts for gender, bloodtype, causes and 
		blood levels
**********************************************************/
    $scope.cdata = [];
    $http.get('/api/loc-gender-chart/' + $routeParams.bid).success(function (cdata) {
      $scope.barData = cdata;
      for (var i = 0; i < $scope.barData.length; i++) {
        //console.log($scope.barData[i]);
        for (var prop in $scope.barData[i]) {
          //console.log(prop);
          $scope.cdata.push([
            prop,
            +$scope.barData[i][prop]
          ]);
        }
      }  //console.log($scope.cdata);
    });
    $scope.ddata = [];
    $http.get('/api/loc-bloodtype-chart/' + $routeParams.bid).success(function (ddata) {
      $scope.chartData = ddata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.ddata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }  //console.log($scope.ddata);
    });
    $scope.edata = [];
    $http.get('/api/loc-causes-chart/' + $routeParams.bid).success(function (edata) {
      $scope.chartData = edata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.edata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }  //console.log($scope.edata);
    });
    $scope.fdata = [];
    $http.get('/api/loc-levels-chart/' + $routeParams.bid).success(function (fdata) {
      $scope.chartData = fdata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.fdata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }  //console.log($scope.fdata);
    });
  }
]);/******************************************************************************************************************
 Donation Centres By Country Controller 
 - Get Donation Centre Info By Country
******************************************************************************************************************/
reddonor.controller('CentresCountryCtrl', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  'ngProgress',
  function ($scope, $http, $location, $routeParams, ngProgress) {
    ngProgress.start();
    // Get Locations Info by country
    $http.get('/api/centres/' + $routeParams.loc).success(function (data) {
      $scope.locations = data;
      $scope.chosenCountry = data[0].country_name;
      ngProgress.complete();
    });
  }
]);/******************************************************************************************************************
 Donation Centres Controller
 - Display donation centre data with filter by country
******************************************************************************************************************/
reddonor.controller('CentresCtrl', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  'countryListData',
  'ngProgress',
  function ($scope, $http, $location, $routeParams, countryListData, ngProgress) {
    ngProgress.start();
    // Get Locations Info (ALL)
    $http.get('/api/centres/').success(function (data) {
      $scope.locations = data;
      $scope.filteredLocations = data;
      ngProgress.complete();
    });
    $scope.countries = countryListData.getCountries();
    $scope.countries.then(function (countries) {
      $scope.countries = countries;
    });
    $scope.chosenCountry = 'All';
    //console.log("countries")
    //console.log($scope.countries);
    $scope.$watch('centreFilter', function () {
      $scope.filteredLocations = [];
      angular.forEach($scope.locations, function (val, key) {
        if (val.location_country == $scope.centreFilter) {
          this.push(val);
          $scope.chosenCountry = val.country_name;  /*console.log("Val");
				console.log(val);
				console.log("Val Country ID");
				console.log(val.location_id);
				console.log("Centre Filter");*/
        } else if ($scope.centreFilter === undefined) {
          //console.log("centre undefined");
          this.push(val);
        }
      }, $scope.filteredLocations);
    });
  }
]);/******************************************************************************************************************
 Blood Donor Controller
 - Sign Up form receiving information for emergency blood donors
******************************************************************************************************************/
reddonor.controller('DonorSignUpCtrl', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  function ($scope, $http, $location, $routeParams) {
    $scope.donorsuccess = false;
    //$scope.showSuccess = function() {
    //	$scope.donorsuccess = true;
    //}
    $scope.onSubmitSignup = function () {
      $http.post('/api/donor-signup/', $scope.user).success(function (data) {
        //console.log(data);
        //console.log(scope.user);
        if (data.error) {
          $scope.error = 'Email Already Exists';  //window.location = "/login";
        }
        if (data.success) {
          $scope.error = '';
          //$scope.showSuccess();
          //console.log("showSuccess called?");
          $scope.donorsuccess = true;  //console.log("$scope.donorsuccess = true");
                                       //window.location = "/";
                                       //console.log("Button clicked!");
        }
      });
    };
    $http.get('/api/countries').success(function (data) {
      $scope.country = data;
      //console.log(data);
      return;
    });
    $scope.bloodtypes = [
      {
        id: 1,
        name: 'O+'
      },
      {
        id: 2,
        name: 'O-'
      },
      {
        id: 3,
        name: 'A+'
      },
      {
        id: 4,
        name: 'A-'
      },
      {
        id: 5,
        name: 'B+'
      },
      {
        id: 6,
        name: 'B-'
      },
      {
        id: 7,
        name: 'AB+'
      },
      {
        id: 8,
        name: 'AB-'
      },
      {
        id: 9,
        name: 'Unknown'
      }
    ];
    $scope.gender = [
      {
        id: 1,
        name: 'Male'
      },
      {
        id: 2,
        name: 'Female'
      }
    ];
    $scope.telephone = /^\d\d\d\d\d\d\d$/;
  }
]);/******************************************************************************************************************
 Add Drive Controller
 - Get country list, uses list, blood types list and errors
******************************************************************************************************************/
/* ---------- Add Drive Controller ---------- */
reddonor.controller('AddDriveCtrl', [
  '$scope',
  '$http',
  '$location',
  function ($scope, $http, $location) {
    // Get Country List
    $http.get('/api/country').success(function (data) {
      $scope.country = data;
      //console.log(data);
      return;
    });
    // Get Uses List
    $http.get('/api/uses').success(function (data) {
      $scope.uses = data;
      //console.log(data);
      return;
    });
    $scope.showForgot = function () {
      if ($scope.forgotpwd === true) {
        $scope.forgotpwd = false;
      } else {
        $scope.forgotpwd = true;
      }
    };
    // Get Locations Info (ALL)
    $http.get('/api/centres/').success(function (data) {
      $scope.location = data;
      $scope.filteredLocations = data;
      return;
    });
    $scope.countryChange = function () {
      $scope.filteredLocations = [];
      angular.forEach($scope.location, function (val, key) {
        if (val.location_country == $scope.recipient.driveCountry) {
          this.push(val);
        } else if ($scope.recipient.driveCountry === undefined) {
          this.push(val);
        }
      }, $scope.filteredLocations);
    };
    // Blood Types for List
    $scope.bloodTypes = [
      {
        id: 1,
        name: 'O+'
      },
      {
        id: 2,
        name: 'O-'
      },
      {
        id: 3,
        name: 'A+'
      },
      {
        id: 4,
        name: 'A-'
      },
      {
        id: 5,
        name: 'B+'
      },
      {
        id: 6,
        name: 'B-'
      },
      {
        id: 7,
        name: 'AB+'
      },
      {
        id: 8,
        name: 'AB-'
      },
      {
        id: 9,
        name: 'Unknown'
      }
    ];
    $scope.poperr = function () {
      toaster.pop('error', '', $scope.error, 5000, true);
    };
    $scope.popsuccess = function () {
      toaster.pop('success', '', $scope.success, 5000, true);
    };
    $scope.onSubmitAddDrive = function () {
      $http.post('/api/create-drive/', $scope.recipient).success(function (data) {
        if (data.aerror) {
          $scope.aerror = true;
          $scope.error = 'Sorry, that Deadline Date has passed';
          $scope.poperr();
        }
        if (data.berror) {
          $scope.error = 'An error occurred. Your Drive was not created :(';
          $scope.poperr();
        }
        if (data.success) {
          $scope.error = '';
          window.location = '/';
        }
      });
    };
  }
]);/******************************************************************************************************************
 Create Drive Controller
 - Get country list, uses list, blood types list and errors
******************************************************************************************************************/
/* ---------- Create Drive Controller ---------- */
reddonor.controller('CreateDriveCtrl', [
  '$scope',
  '$http',
  '$location',
  function ($scope, $http, $location) {
    $scope.go = function (path) {
      $location.path(path);
    };
    $scope.create = function () {
      $scope.decision = false;
    };
    // Get Country List
    $http.get('/api/country').success(function (data) {
      $scope.country = data;
      //console.log(data);
      return;
    });
    // Get Uses List
    $http.get('/api/uses').success(function (data) {
      $scope.uses = data;
      //console.log(data);
      return;
    });
    // Get Locations Info (ALL)
    $http.get('/api/centres/').success(function (data) {
      $scope.location = data;
      $scope.filteredLocations = data;
      return;
    });
    $scope.decision = true;
    $scope.countryChange = function () {
      $scope.filteredLocations = [];
      //$scope.recipient = { driveCentre: $scope.filteredLocations[0] = "-- chose --"};
      angular.forEach($scope.location, function (val, key) {
        if (val.location_country == $scope.recipient.driveCountry) {
          //$scope.recipient = { driveCentre: $scope.filteredLocations[0] = "-- chose --"};
          this.push(val);  //$scope.chosenCountry = val.country_name;
                           /*console.log("Val");
					console.log(val);
					console.log("Val Location ID");
					console.log(val.location_id);
					console.log("Centre Filter");*/
        } else if ($scope.recipient.driveCountry === undefined) {
          //console.log("centre undefined");
          this.push(val);
        }
      }, $scope.filteredLocations);
    };
    // Blood Types for List
    $scope.bloodTypes = [
      {
        id: 1,
        name: 'O+'
      },
      {
        id: 2,
        name: 'O-'
      },
      {
        id: 3,
        name: 'A+'
      },
      {
        id: 4,
        name: 'A-'
      },
      {
        id: 5,
        name: 'B+'
      },
      {
        id: 6,
        name: 'B-'
      },
      {
        id: 7,
        name: 'AB+'
      },
      {
        id: 8,
        name: 'AB-'
      },
      {
        id: 9,
        name: 'Unknown'
      }
    ];
    $scope.poperr = function () {
      toaster.pop('error', '', $scope.error, 5000, true);
    };
    $scope.popsuccess = function () {
      toaster.pop('success', '', $scope.success, 5000, true);
    };
    $scope.onSubmitAddDrive = function () {
      $http.post('/api/create-new-drive/', $scope.recipient).success(function (data) {
        if (data.aerror) {
          $scope.aerror = true;
          $scope.error = 'Sorry, that Deadline Date has passed';
          $scope.poperr();
        }
        if (data.berror) {
          $scope.error = 'An error occurred. Your Drive was not created :(';
          $scope.poperr();
        }
        if (data.success) {
          $scope.error = '';
          window.location = '/';
        }
      });
    };
  }
]);/******************************************************************************************************************
 Drive Details Controller 
 - Display blood drive details, blood type compatibility, pledge/unpledge buttons, share buttons, flash message
******************************************************************************************************************/
reddonor.controller('DriveReadCtrl', [
  '$scope',
  '$http',
  '$timeout',
  '$routeParams',
  '$location',
  '$window',
  'toaster',
  function ($scope, $http, $timeout, $routeParams, $location, $window, toaster) {
    $scope.poperr = function () {
      toaster.pop('error', '', $scope.error, 5000, true);
    };
    $scope.popsuccess = function () {
      toaster.pop('success', '', $scope.success, 5000, true);
    };
    $scope.notlogged = false;
    $scope.orgexist = false;
    $scope.tooltip = { 'title': 'Click to Unpledge Donation' };
    $scope.fbshare = { 'title': 'Share to Facebook' };
    $scope.twitshare = { 'title': 'Share to Twitter' };
    $scope.button = { 'active': false };
    $scope.drivelist = function () {
      $http.get('/api/drivelist/' + $routeParams.did).success(function (data) {
        if (data.org_name) {
          $scope.orgexist = true;
        }
        $scope.drives = data[0];
        $scope.page.setTitle($scope.drives.Drive_For + ' Blood Request');
        $scope.ended = false;
      }).error(function (data) {
        $scope.data = data || 'Request failed';
      });
    };
    $scope.drivelist();
    $http.get('/api/blood-info/' + $routeParams.did).success(function (data) {
      $scope.datediff = data[0][0];
      $scope.bloods = data[1];
    });
    // Get Pledge Button Info
    $http.get('/api/drivecheck/' + $routeParams.did).success(function (data) {
      if (data.driveended) {
        $scope.drivestatus = 'Drive Ended';
        $scope.ended = true;
        $scope.unpledge = false;
      }
      if (data.pledgedalready) {
        $scope.drivestatus = 'Donation Pledged';
        $scope.ended = false;
        $scope.unpledge = true;
        $scope.actionPledge = 'onSubmitUnPledgeDonation';
      }
      if (data.safe) {
        $scope.drivestatus = 'Pledge Donation';
        $scope.ended = false;
        $scope.unpledge = false;
        $scope.actionPledge = 'onSubmitPledgeDonation';
      }
    });
    // Get Locations Info
    $http.get('/api/locations/' + $routeParams.did).success(function (data) {
      $scope.locations = data;
    });
    $scope.onSubmitUnPledgeDonation = function () {
      $http.post('/api/unpledge-donation/' + $routeParams.did, $routeParams).success(function (data) {
        $scope.drivestatus = 'Pledge Donation';
        $scope.ended = false;
        $scope.unpledge = false;
        $scope.actionPledge = 'onSubmitPledgeDonation';
        $scope.success = 'Donation Unpledged';
        $scope.drivelist();
        $scope.popsuccess();
      });
    };
    $scope.onSubmitPledgeDonation = function (location) {
      $http.post('/api/pledge-donation/' + $routeParams.did + '/' + location).success(function (data) {
        if (data.nosession) {
          $scope.notlogged = true;
          console.log('please log in');
          $scope.error = 'Please <a href=\'/login\'> Login </a><span> or </span><a href=\'/signup\'> Sign Up </a>';
          $scope.poperr();
        } else if (data.proceed) {
          $scope.error = '';
          $scope.drivestatus = 'Donation Pledged';
          $scope.ended = false;
          $scope.unpledge = true;
          $scope.actionPledge = 'onSubmitUnPledgeDonation';
          $scope.success = 'Donation Pledged';
          //$scope.drivelist();
          $timeout(function () {
            $scope.drivelist();
          }, 500);
          $scope.popsuccess();
        } else if (data[0].toorecent) {
          $scope.error = 'Sorry, you donated blood less than 3 months ago.';
          $scope.poperr();
        } else if (data[1].wrongLocation) {
          $scope.error = 'Sorry, You must live in the same country to donate.';
          $scope.poperr();
        }
      }).error(function () {
      });
    };
  }
]);/******************************************************************************************************************
 Drives By Blood Type Controller (Paged)
 - Display drives by blood type, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('DriveCatPagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.empty = true;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Count of Number of Drives for Pagination
    $scope.getItemCount = function () {
      $http.get('/api/drives-count-crumb/' + $routeParams.cat).success(function (datas) {
        //$scope.totalItems = datas.drivecount;
        $scope.totalItems = datas[0].Result_Count;
        $scope.breadcrumb = datas[0].bloodtype_name;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
      });
    };
    // Get Previous Drive Completed Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('/drives/' + $routeParams.cat + '?page=' + $scope.currentPage);  //$window.location = '../drives/' + $routeParams.cat + '?page=' + $scope.currentPage;
      }
    };
    // Get Next Drive Completed Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/' + $routeParams.cat + '?page=' + $scope.nextPage);  //$window.location = '../drives/' + $routeParams.cat + '?page=' + $scope.nextPage;
      }
    };
    // Get blood drive information
    $scope.getDrives = function () {
      $http.get('/api/drives/' + $routeParams.cat).success(function (data) {
        $scope.drives = data;
        $scope.empty = false;
        $scope.limit = 8;
        $scope.blood = $routeParams.cat;
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.drives == '') {
          $scope.text = 'No New Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      $scope.getItemCount();
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getItemCount();
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Drives By Location Controller (Paged)
 - Display drives by location, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('DriveLocPagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.empty = true;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Count of Number of Drives for Pagination & Breadcrumb Data
    $scope.getItemCount = function () {
      $http.get('/api/drives-location-count-crumb/' + $routeParams.loc).success(function (datas) {
        $scope.totalItems = datas[0].Result_Count;
        $scope.breadcrumb = datas[0].country_name;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
      });
    };
    // Get Previous Drive by Location Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('/drives/' + $routeParams.loc + '?page=' + $scope.currentPage);  //$window.location = '../drives/' + $routeParams.loc + '?page=' + $scope.currentPage;
      }
    };
    // Get Next Drive by Location Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/' + $routeParams.loc + '?page=' + $scope.nextPage);  //$window.location = '../drives/' + $routeParams.loc + '?page=' + $scope.nextPage;
      }
    };
    // Get blood drive information
    $scope.getDrives = function () {
      $scope.getItemCount();
      $http.get('/api/drives-location/' + $routeParams.loc).success(function (data) {
        $scope.drives = data;
        $scope.empty = false;
        $scope.limit = 8;
        $scope.blood = $routeParams.loc;
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.drives == '') {
          $scope.text = 'No New Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      // Get Soon Ending Drives
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Get Compatible Drives Via User Bloodtype (Paged)
  - Display compatible drives, pagination, no blood drives (empty) set and compatible blood types list
******************************************************************************************************************/
reddonor.controller('CompatPagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.empty = true;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Previous Compatible Drive Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('../drives/compatible?page=' + $scope.currentPage);  //$window.location = '../drives/compatible?page=' + $scope.currentPage;
      }
    };
    // Get Next Compatible Drive Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/compatible?page=' + $scope.nextPage);  //$window.location = '../drives/compatible?page=' + $scope.nextPage;
      }
    };
    // Get Count of Number of Drives for Pagination
    $scope.getItemCount = function () {
      $http.get('/api/drives-compat/' + $routeParams.cat).success(function (datas) {
        $scope.totalItems = datas[0].Result_Count;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
      });
    };
    // Get blood drive information
    $scope.getDrives = function () {
      $scope.getItemCount();
      $http.get('/api/drives-compat/' + $routeParams.page).success(function (data) {
        $scope.drives = data;
        $scope.empty = false;
        $scope.limit = 8;
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.drives == '') {
          $scope.text = 'No New Compatible Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
    // Get Compatible Blood Types For List
    $http.get('/api/blood-compat/').success(function (data) {
      $scope.bloods = data[0];
      $scope.lists = data[1];
    });
  }
]);/******************************************************************************************************************
 Completed Drives Controller (Paged)
  - Display completed drives, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('EndDrivePagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.empty = false;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Previous Drive Completed Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('drives/drive-completed?page=' + $scope.currentPage);  //$window.location = "../drives/drive-completed?page=" + $scope.currentPage;
      }
    };
    // Get Next Drive Completed Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('/drives/drive-completed?page=' + $scope.nextPage);  //$window.location = "../drives/drive-completed?page=" + $scope.nextPage;
      }
    };
    // Get blood drive information
    $scope.getDrives = function () {
      $http.get('/api/drives-nonactive/' + $routeParams.page).success(function (data) {
        $scope.drives = data;
        $scope.empty = false;
        $scope.limit = 8;
        if (data[0])
          $scope.totalItems = data[0].Result_Count;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
        if ($scope.drives == '') {
          $scope.text = 'No Ended Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      // Get Soon Ending Drives
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Drives Ending Soon Controller (Paged)
  - Display drives ending soon, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('SoonEndPagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.empty = true;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Previous Ending Soon Drive Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('../drives/soon-ending?page=' + $scope.currentPage);
      }
    };
    // Get Next Ending Soon Drive Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/soon-ending?page=' + $scope.nextPage);
      }
    };
    // Get blood drive information
    $scope.getDrives = function () {
      $http.get('/api/drives-endingsoon/' + $routeParams.page).success(function (data) {
        $scope.drives = data[0];
        $scope.empty = false;
        $scope.limit = 8;
        if (data[0][0])
          $scope.totalItems = data[0][0].Result_Count;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
        if ($scope.drives == '') {
          $scope.text = 'No New Ending Soon Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      // Get Soon Ending Drives
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Explore Drives Controller (Paged)
 - Display blood drives, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('ListDrivePagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.page.setTitle('Explore Drives');
    $scope.content = [];
    $scope.empty = true;
    $scope.emptydrive = false;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Previous Explore Drive Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('../drives/list?page=' + $scope.currentPage);  //$window.location = '../drives/list?page=' + $scope.currentPage;
      }
    };
    // Get Next Explore Drive Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/list?page=' + $scope.nextPage);  //$window.location = '../drives/list?page=' + $scope.nextPage;
      }
    };
    // Get blood drive information
    $scope.getDrives = function () {
      console.log('--routeParams--');
      console.log($routeParams);
      $http.get('/api/drives-active/' + $routeParams.page).success(function (data) {
        $scope.drivesactive = data;
        $scope.empty = false;
        $scope.limit = 8;
        $scope.totalItems = data[0].Result_Count;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($routeParams.page !== null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
        if ($scope.drives === '') {
          $scope.text = 'No New Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      // Get Soon Ending Drives
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Most Recent Drives Controller (Paged)
 - Display most recent drives, pagination and no blood drives (empty) set
******************************************************************************************************************/
reddonor.controller('MostRecentPagesCtrl', [
  '$scope',
  '$http',
  '$routeParams',
  '$location',
  '$window',
  function ($scope, $http, $routeParams, $location, $window) {
    $scope.drives = [];
    $scope.content = [];
    $scope.emptydrive = false;
    $scope.empty = true;
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    // Get Most Recent Drives Previous Page
    $scope.prevPage = function () {
      if ($scope.currentPage > 0) {
        $location.url('../drives/most-recent?page=' + $scope.currentPage);  //$window.location = '../drives/most-recent?page=' + $scope.currentPage;
      }
    };
    // Get Most Recent Drives Next Page
    $scope.nextPage = function () {
      if ($scope.currentPage < $scope.numPages - 1) {
        $scope.nextPage = $scope.currentPage + 2;
        $location.url('../drives/most-recent?page=' + $scope.nextPage);  //$window.location = '../drives/most-recent?page=' + $scope.nextPage;
      }
    };
    $scope.getDrives = function () {
      $http.get('/api/drives-mostrecent/' + $routeParams.page).success(function (data) {
        $scope.drives = data[0];
        $scope.empty = false;
        $scope.limit = 8;
        $scope.totalItems;
        console.log($scope.drives);
        console.log($scope.drives.length);
        if (data[0][0])
          $scope.totalItems = data[0][0].Result_Count;
        $scope.pageSize = 8;
        $scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
        for (var i = 0; i < $scope.numPages; i++) {
          $scope.content.push(i);
        }
        if ($routeParams.page != null && $routeParams.page > 0) {
          $scope.currentPage = $routeParams.page - 1;
        } else {
          $scope.currentPage = 0;
        }
        if ($scope.currentPage >= 3) {
          $scope.showCurrent = false;
          $scope.hideCurrent = false;
        } else {
          $scope.showCurrent = true;
          $scope.hideCurrent = true;
        }
        if ($scope.drives == '') {
          $scope.text = 'No New Recent Blood Drives Found';
          $scope.emptydrive = true;
          $scope.empty = true;
        }
      });
      $scope.prevPage();
      $scope.nextPage();
    };
    if ($routeParams.page != null) {
      $scope.getDrives();
    } else {
      $routeParams.page = 1;
      $scope.getDrives();
    }
  }
]);/******************************************************************************************************************
 Edit Drive Controller 
 - Get drive detail data, set date options, check errors and save drive details
******************************************************************************************************************/
reddonor.controller('EditDriveCtrl', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  function ($scope, $http, $location, $routeParams) {
    $scope.editabledrive = false;
    $scope.notauth = false;
    $http.get('/api/countries/').success(function (data) {
      $scope.country = data;
      //console.log(data);
      return;
    });
    $http.get('/api/bloodtype/').success(function (data) {
      $scope.bloodtype = data;
      //console.log(data);
      return;
    });
    // Get Uses List
    $http.get('/api/uses/').success(function (data) {
      $scope.uses = data;
      //console.log(data);
      return;
    });
    $http.get('/api/edit-drive/' + $routeParams.did).success(function (data, status) {
      if (data.notauth) {
        //$location.url('/notauth');
        $scope.notauth = true;
        console.log('not auth');
      } else {
        $scope.recipient = data;
        $scope.status = status;
        $scope.editabledrive = true;
      }
    });
    $scope.onSubmitEditDrive = function () {
      $http.put('/api/save-drive/' + $routeParams.did, $scope.recipient).success(function (data) {
        if (data.error) {
          $scope.error = 'Sorry something went wrong :(';  //window.location = "/login";
        } else {
          $scope.error = '';
          //window.location = "/";
          $location.url('/drive/' + $routeParams.did + '/');
        }
      });
    };
  }
]);/******************************************************************************************************************
 Welcome Page Controller
******************************************************************************************************************/
reddonor.controller('WelcomeCtrl', [
  '$scope',
  '$http',
  '$location',
  function ($scope, $http, $location) {
    $scope.page.setTitle('Connecting Blood Donors');
    $http.get('/api/drives-welcome/').success(function (data) {
      $scope.drivesend = data[0];
      $scope.drivesrecent = data[1];
      $scope.alsoempty = false;
      $scope.limit = 4;
      if ($scope.drivesend == '') {
        $scope.textend = 'No New Ending Soon Blood Drives Found';
        $scope.alsoempty = true;
      }
      if ($scope.drivesrecent == '') {
        $scope.emptydrive = true;
        $scope.textrecent = 'No New Recent Blood Drives Found';
      }
    });
  }
]);/******************************************************************************************************************
	Check Logged In Controller 
 - 
******************************************************************************************************************/
reddonor.controller('CheckingMenuCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    // Get Session Info
    $scope.init = function () {
      $http.get('/api/check-logged').success(function (data) {
        console.log(data);
        if (data.loggedin) {
          $scope.loggedin = true;
        }
        if (data.notloggedin) {
          $scope.loggedin = false;
        }
      });
    };
    $scope.init();
  }
]);/******************************************************************************************************************
 Contact Controller
 - 
******************************************************************************************************************/
reddonor.controller('ContactCtrl', [
  '$scope',
  function ($scope) {
  }
]);/******************************************************************************************************************
 Organisation Profile Controller
 - Get Organisation Info, Charts for Causes, Pledges, Gender and Blood Type
******************************************************************************************************************/
reddonor.controller('OrgProfilePages', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  function ($scope, $http, $location, $routeParams) {
    $scope.title = 'Divsion of Causes';
    $scope.number = 'Pledges';
    $scope.data = [];
    $scope.bdata = [];
    $scope.charts = [
      'BarChart',
      'PieChart',
      'ColumnChart',
      'Table',
      'LineChart',
      'ComboChart',
      'AreaChart',
      'ScatterChart'
    ];
    $scope.selectedChart = $scope.charts[1];
    $http.get('/api/org-profile-info/' + $routeParams.oid).success(function (data) {
      $scope.orgprofile = data;
    });
    $http.get('/api/org-drives-cause-chart/' + $routeParams.oid).success(function (data) {
      $scope.pieData = data;
      for (var i = 0; i < $scope.pieData.length; i++) {
        //console.log($scope.pieData[i]);
        for (var prop in $scope.pieData[i]) {
          //console.log(prop);
          $scope.data.push([
            prop,
            +$scope.pieData[i][prop]
          ]);
        }
      }
    });
    $http.get('/api/org-pledges-cause-chart/' + $routeParams.oid).success(function (bdata) {
      $scope.pieData = bdata;
      for (var i = 0; i < $scope.pieData.length; i++) {
        //console.log($scope.pieData[i]);
        for (var prop in $scope.pieData[i]) {
          //console.log(prop);
          $scope.bdata.push([
            prop,
            +$scope.pieData[i][prop]
          ]);
        }
      }  //console.log($scope.bdata);
    });
    $scope.cdata = [];
    $http.get('/api/org-gender-chart/' + $routeParams.oid).success(function (cdata) {
      $scope.chartData = cdata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.cdata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }
    });
    $scope.ddata = [];
    $http.get('/api/org-drives-bloodtype-chart/' + $routeParams.oid).success(function (ddata) {
      $scope.chartData = ddata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.ddata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }
    });
    $scope.edata = [];
    $http.get('/api/org-pledges-bloodtype-chart/' + $routeParams.oid).success(function (edata) {
      $scope.chartData = edata;
      for (var i = 0; i < $scope.chartData.length; i++) {
        //console.log($scope.chartData[i]);
        for (var prop in $scope.chartData[i]) {
          //console.log(prop);
          $scope.edata.push([
            prop,
            +$scope.chartData[i][prop]
          ]);
        }
      }
    });
  }
]);/******************************************************************************************************************
  Profile Appointments Controller 
 - Get user profile info and appointment schedules
******************************************************************************************************************/
reddonor.controller('ApptsCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $http.get('/api/appointment-schedules/').success(function (data) {
      $scope.appts = data;  //console.log(data);
    });
    $http.get('/api/profile-info/').success(function (data) {
      $scope.profiles = data;
    });
  }
]);/******************************************************************************************************************
 Donation History Controller
 - Get user profile info and donation pledges
******************************************************************************************************************/
reddonor.controller('HistoryCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $http.get('/api/profile-info/').success(function (data) {
      $scope.profiles = data;
    });
    $http.get('/api/donation-pledges/').success(function (data) {
      $scope.pledges = data;
    });
  }
]);/******************************************************************************************************************
 My Drives Controller
 - Get user profile info and created drives
******************************************************************************************************************/
reddonor.controller('MyDrivesCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $http.get('/api/profile-info/').success(function (data) {
      $scope.profiles = data;
    });
    $http.get('/api/created-drives/').success(function (data) {
      $scope.drives = data;
    });
  }
]);/******************************************************************************************************************
 Profiles Controller
 - Get user profile info, donation pledges and created drives
******************************************************************************************************************/
reddonor.controller('ProfileCtrl', [
  '$scope',
  '$http',
  function ($scope, $http) {
    $http.get('/api/profile-info/').success(function (data) {
      $scope.profiles = data;
    });
    $http.get('/api/donation-pledges/').success(function (data) {
      $scope.pledges = data;
    });
    $http.get('/api/created-drives/').success(function (data) {
      $scope.drives = data;
    });
  }
]);/******************************************************************************************************************
 Profile Settings Controller
 - 
******************************************************************************************************************/
reddonor.controller('ProfileSettingsCtrl', [
  '$scope',
  '$http',
  '$location',
  '$window',
  function ($scope, $http, $location, $window) {
  }
]);/******************************************************************************************************************
 Guest SignUp Controller
 - 
******************************************************************************************************************/
reddonor.controller('GuestSignUpCtrl', [
  '$scope',
  '$http',
  '$location',
  function ($scope, $http, $location) {
  }
]);/******************************************************************************************************************
 Log In Controller
 - Display 'forgot password' form, success message, errors and send reset password to email address
******************************************************************************************************************/
reddonor.controller('LoginCtrl', [
  '$scope',
  '$http',
  '$location',
  'toaster',
  function ($scope, $http, $location, toaster) {
    $scope.forgotpwd = false;
    $scope.showForgot = function () {
      if ($scope.forgotpwd === true) {
        $scope.forgotpwd = false;
      } else {
        $scope.forgotpwd = true;
      }
    };
    $scope.poperr = function () {
      toaster.pop('error', '', $scope.error, 5000, true);
    };
    $scope.popsuccess = function () {
      toaster.pop('success', '', $scope.success, 5000, true);
    };
    $scope.onSubmitsendEmail = function () {
      $http.post('/api/forgot-password/', $scope.forgot).success(function (data) {
        if (data.noexist) {
          $scope.noexist = 'Sorry, this email address was not found';
          $scope.error = 'Sorry, this email address was not found';
          $scope.poperr();
        } else {
          $scope.noexist = '';
          $scope.success = 'A Reset Password Email has been sent to your address';
          $scope.forgotpwd = false;
          $scope.popsuccess();
        }
      }).error(function (data) {
        $scope.error = 'Something went wrong :(';
        $scope.poperr();
      });
    };
  }
]);/******************************************************************************************************************
 Reset Password Controller 
 - Get reset token, send success email, check errors and change password
******************************************************************************************************************/
reddonor.controller('ResetCtrl', [
  '$scope',
  '$http',
  '$location',
  '$routeParams',
  'toaster',
  function ($scope, $http, $location, $routeParams, toaster) {
    // set reset password form to hidden
    $scope.validUser = false;
    // set forgot password form to hidden
    $scope.forgotpwd = false;
    $scope.poperr = function () {
      toaster.pop('error', '', $scope.error, 5000, true);
    };
    $scope.popsuccess = function () {
      toaster.pop('success', '', $scope.success, 5000, true);
    };
    $http.get('/api/reset-password/' + $routeParams.token).success(function (data, status) {
      if (data.success) {
        $scope.success = 'This token has been authenticated. Please continue.';
        $scope.validUser = true;
        $scope.popsuccess();
      }
      if (data.expired) {
        $scope.error = 'Sorry, this request token has expired. Please request a password reset again.';
        $scope.forgotpwd = true;
        $scope.poperr();
      }
      if (data.error) {
        $scope.error = 'Sorry, this request token is not valid. Please request a password reset again.';
        $scope.forgotpwd = true;
        $scope.poperr();
      }
    });
    $scope.onSubmitsendEmail = function () {
      $http.post('/api/forgot-password/', $scope.forgot).success(function (data) {
        if (data.noexist) {
          $scope.noexist = 'Sorry, this email address was not found';
          $scope.error = 'Sorry, this email address was not found';
          $scope.poperr();
        } else {
          $scope.noexist = '';
          $scope.success = 'A Reset Password Email has been sent to your address';
          $scope.msg = 'A Reset Password Email has been sent to your address';
          $scope.forgotpwd = false;
          $scope.popsuccess();
        }
      }).error(function (data) {
        $scope.error = 'Something went wrong :(';
        $scope.msg = 'Something went wrong :(';
        $scope.poperr();
      });
    };
    $scope.onSubmitNewPassword = function () {
      $http.post('/api/reset-password/' + $routeParams.token, $scope.resetpassword).success(function (data) {
        $location.url('/login');
        window.location = '/login';
      });
    };
  }
]);/******************************************************************************************************************
 Sign Up Controller
 - 
******************************************************************************************************************/
reddonor.controller('SignUpCtrl', [
  '$scope',
  '$http',
  '$location',
  function ($scope, $http, $location) {
  }
]);;/******************************************************************************************************************
 Edit Drive Directive
 - Gets countries, bloodtypes, uses, donations
******************************************************************************************************************/
reddonor.directive('editdriveform', [
  '$http',
  '$location',
  '$routeParams',
  function ($http, $location, $routeParams) {
    var editdriveformFn;
    editdriveformFn = function (scope, element, attrs) {
      $http.get('/api/countries/').success(function (data) {
        scope.country = data;
        //console.log(data);
        return;
      });
      $http.get('/api/bloodtype/').success(function (data) {
        scope.bloodtype = data;
        //console.log(data);
        return;
      });
      // Get Uses List
      $http.get('/api/uses/').success(function (data) {
        scope.uses = data;
        //console.log(data);
        return;
      });
      scope.donations = [
        {
          id: 1,
          nos: '1'
        },
        {
          id: 2,
          nos: '2'
        },
        {
          id: 3,
          nos: '3'
        },
        {
          id: 4,
          nos: '4'
        },
        {
          id: 5,
          nos: '5'
        },
        {
          id: 6,
          nos: '6'
        },
        {
          id: 7,
          nos: '7'
        },
        {
          id: 8,
          nos: '8'
        },
        {
          id: 9,
          nos: '9'
        },
        {
          id: 10,
          nos: '10'
        },
        {
          id: 11,
          nos: '11'
        },
        {
          id: 12,
          nos: '12'
        }
      ];
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/editDrive-form.jade',
      link: editdriveformFn
    };
  }
]);/******************************************************************************************************************
 Guest Sign Up Fom Directive
 - Gets countries, bloodtypes, gender, add user
******************************************************************************************************************/
reddonor.directive('guestsignupform', [
  '$http',
  '$location',
  'toaster',
  function ($http, $location, toaster) {
    var signupGuestformFn;
    signupGuestformFn = function (scope, element, attrs) {
      scope.poperr = function () {
        toaster.pop('error', '', scope.error, 5000, true);
      };
      scope.popsuccess = function () {
        toaster.pop('success', '', scope.success, 5000, true);
      };
      scope.onSubmitSignup = function () {
        $http.post('/api/guest-signup/', scope.user).success(function (data) {
          if (data.eerror) {
            scope.eerror = true;
            scope.aerror = false;
            scope.error = 'Email address matches an existing account';
            scope.poperr();
          } else if (data.aerror) {
            scope.aerror = true;
            scope.eerror = false;
            scope.error = 'You must be over 12 years old to sign up';
          } else {
            scope.error = '';
            window.location = '/';
          }
        });
      };
      $http.get('/api/countries').success(function (data) {
        scope.country = data;
        return;
      });
      scope.bloodtypes = [
        {
          id: 1,
          name: 'O+'
        },
        {
          id: 2,
          name: 'O-'
        },
        {
          id: 3,
          name: 'A+'
        },
        {
          id: 4,
          name: 'A-'
        },
        {
          id: 5,
          name: 'B+'
        },
        {
          id: 6,
          name: 'B-'
        },
        {
          id: 7,
          name: 'AB+'
        },
        {
          id: 8,
          name: 'AB-'
        },
        {
          id: 9,
          name: 'Unknown'
        }
      ];
      scope.gender = [
        {
          id: 1,
          name: 'Male'
        },
        {
          id: 2,
          name: 'Female'
        }
      ];
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/guestsignup-form.jade',
      link: signupGuestformFn
    };
  }
]);/******************************************************************************************************************
 Log In Form Directive
 - Performs log in
******************************************************************************************************************/
reddonor.directive('loginform', [
  '$http',
  '$location',
  '$window',
  'toaster',
  function ($http, $location, $window, toaster) {
    var loginformFn;
    loginformFn = function (scope, element, attrs) {
      scope.loginerror = false;
      scope.poperr = function () {
        toaster.pop('error', '', scope.error, 5000, true);
      };
      scope.popsuccess = function () {
        toaster.pop('success', '', scope.success, 5000, true);
      };
      scope.onSubmitLogin = function () {
        $http.post('/api/login/', scope.user).success(function (data) {
          if (data.noemail) {
            scope.error = 'Email Address Was Not Found';
            scope.incorrect = 'Email address was not found';
            scope.poperr();
          } else if (data.incorrect) {
            scope.error = 'Incorrect Password Entered';
            scope.incorrect = 'Incorrect password enetered';
            scope.poperr();
          } else {
            scope.incorrect = '';
            scope.success = 'Login Successful!';
            scope.popsuccess();
            //window.history.back();
            window.location = '/';  //$location.path('/profile');
          }
        }).error(function (data, status, headers, config) {
          //console.log("Data for login POST encountered an error.");
          window.location = '/';
        });
      };
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/login-form.jade',
      link: loginformFn
    };
  }
]);/******************************************************************************************************************
 Account Settings Directive
 - Save account info, change password, flash notification, country list and user settings
******************************************************************************************************************/
reddonor.directive('settingsform', [
  '$http',
  '$location',
  '$window',
  'toaster',
  function ($http, $location, $window, toaster) {
    var settingsformFn;
    settingsformFn = function (scope, element, attrs) {
      scope.onSubmitSaveAccount = function () {
        $http.put('/api/save-account/', scope.settings).success(function (data) {
          if (data.error) {
            scope.error = ':( An error occurred. Changes were not saved';
            scope.poperr();
          } else {
            scope.error = '';
            scope.success = 'Your changes have been saved to your account';
            scope.popsuccess();  //$window.location = "/profile";
                                 //$location.url('/profile');
          }
        });
      };
      scope.popsuccess = function () {
        toaster.pop('success', '', scope.success, 5000, true);
        console.log('success');
      };
      scope.poperr = function () {
        toaster.pop('error', '', scope.error, 5000, true);
      };
      scope.onSubmitChangePassword = function () {
        $http.put('/api/save-password/', scope.pwd).success(function (data) {
          if (data.incorrect) {
            scope.error = 'Current password is incorrect';
            scope.poperr();
          }
          if (data.successful) {
            scope.incorrect = '';
            scope.success = 'Your password has been changed';
            scope.popsuccess();  //$location.url('/profile');
                                 //$window.location = "/profile";
          }
        });
      };
      $http.get('/api/countries/').success(function (data) {
        scope.country = data;
        //console.log(data);
        return;
      });
      $http.get('/api/user-settings/').success(function (data, status) {
        scope.settings = data;
        scope.status = status;
        return;
      }).error(function (data, status) {
        scope.settings = data || 'Request failed';
        scope.status = status;
      });
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/settings-form.jade',
      link: settingsformFn
    };
  }
]);/******************************************************************************************************************
 Sign Up Fom Directive
 - Gets countries, bloodtypes, gender, add user
******************************************************************************************************************/
reddonor.directive('signupform', [
  '$http',
  '$location',
  'toaster',
  function ($http, $location, toaster) {
    var signupformFn;
    signupformFn = function (scope, element, attrs) {
      scope.poperr = function () {
        toaster.pop('error', '', scope.error, 5000, true);
      };
      scope.popsuccess = function () {
        toaster.pop('success', '', scope.success, 5000, true);
      };
      scope.onSubmitSignup = function () {
        $http.post('/api/signup/', scope.user).success(function (data) {
          if (data.eerror) {
            scope.eerror = true;
            scope.aerror = false;
            scope.error = 'Email address matches an existing account';
            scope.poperr();
          } else if (data.aerror) {
            scope.aerror = true;
            scope.eerror = false;
            scope.error = 'You must be over 12 years old to sign up';
          } else {
            scope.error = '';
            window.location = '/';
          }
        });
      };
      $http.get('/api/countries').success(function (data) {
        scope.country = data;
        return;
      });
      scope.bloodtypes = [
        {
          id: 1,
          name: 'O+'
        },
        {
          id: 2,
          name: 'O-'
        },
        {
          id: 3,
          name: 'A+'
        },
        {
          id: 4,
          name: 'A-'
        },
        {
          id: 5,
          name: 'B+'
        },
        {
          id: 6,
          name: 'B-'
        },
        {
          id: 7,
          name: 'AB+'
        },
        {
          id: 8,
          name: 'AB-'
        },
        {
          id: 9,
          name: 'Unknown'
        }
      ];
      scope.gender = [
        {
          id: 1,
          name: 'Male'
        },
        {
          id: 2,
          name: 'Female'
        }
      ];
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/signup-form.jade',
      link: signupformFn
    };
  }
]);/******************************************************************************************************************
 Navigation Menu Directive (Logged In)
 - Get username, donation centre locations, countries and logout
******************************************************************************************************************/
reddonor.directive('rednav', [
  '$http',
  '$location',
  function ($http, $location) {
    var navLogout;
    navLogout = function (scope, element, attrs) {
      scope.onlogOut = function () {
        $http.get('/session/logout').success(function () {
          //console.log("Logout clicked!");
          //window.location = "/login";
          window.location = '/';
        }).error(function () {
        });
      };
      $http.get('/api/countries/').success(function (data) {
        scope.country = data;
        //console.log(data);
        return;
      });
      $http.get('/api/username/').success(function (data) {
        scope.username = data[0].user_name;
        return;
      });
      $http.get('/api/centre-locations/').success(function (data) {
        scope.userlocations = data;
        //console.log(data);
        return;
      });
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/loggedin-menu.jade',
      link: navLogout
    };
  }
]);/******************************************************************************************************************
 Navigation Menu Directive
 - Get username, donation centre locations, countries and logout
******************************************************************************************************************/
reddonor.directive('onenav', [
  '$http',
  '$location',
  function ($http, $location) {
    var navOriginal;
    navOriginal = function (scope, element, attrs) {
      scope.init = function () {
        $http.get('/api/check-logged/').success(function (data) {
          //console.log(data);
          if (data.loggedin) {
            //scope.loggedin = true;
            scope.loggedin = 'logged';
            scope.getUsername();
            scope.getLocations();
          }
          if (data.notloggedin) {
            //scope.loggedin = false;
            scope.loggedin = 'notlogged';
          }
        });
      };
      scope.init();
      scope.onlogOut = function () {
        $http.get('/session/logout').success(function () {
          //console.log("Logout clicked!");
          //window.location = "/login";
          window.location = '/';
        }).error(function () {
        });
      };
      scope.getUsername = function () {
        $http.get('/api/profile-info/').success(function (data) {
          scope.profiles = data;
          scope.username = data[0].user_name;
        });
      };
      scope.getLocations = function () {
        $http.get('/api/centre-locations/').success(function (data) {
          scope.userlocations = data;
          //console.log(data);
          return;
        });
      };
      scope.onlogOut = function () {
        $http.get('/session/logout').success(function () {
          //console.log("Logout clicked!");
          //window.location = "/login";
          window.location = '/';
        }).error(function () {
        });
      };
      $http.get('/api/countries/').success(function (data) {
        scope.country = data;
        //console.log(data);
        return;
      });
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/not-loggedin-menu.jade',
      link: navOriginal
    };
  }
]);reddonor.directive('sidenav', [
  '$http',
  function ($http) {
    var bloodtypeFn;
    bloodtypeFn = function (scope, element, attrs) {
      scope.bloodtype = [
        {
          bloodtype_url: 'o-pos',
          bloodtype_name: 'O+'
        },
        {
          bloodtype_url: 'o-neg',
          bloodtype_name: 'O-'
        },
        {
          bloodtype_url: 'a-pos',
          bloodtype_name: 'A+'
        },
        {
          bloodtype_url: 'a-neg',
          bloodtype_name: 'A-'
        },
        {
          bloodtype_url: 'b-pos',
          bloodtype_name: 'B+'
        },
        {
          bloodtype_url: 'b-neg',
          bloodtype_name: 'B-'
        },
        {
          bloodtype_url: 'ab-pos',
          bloodtype_name: 'AB+'
        },
        {
          bloodtype_url: 'ab-neg',
          bloodtype_name: 'AB-'
        }
      ];
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/bloodtype-menu.jade',
      link: bloodtypeFn
    };
  }
]);/******************************************************************************************************************
 Share Social Directive
******************************************************************************************************************/
reddonor.directive('emptysocial', [
  '$http',
  function ($http) {
    var sharesocialFn;
    sharesocialFn = function (scope, element, attrs) {
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/empty-drives.jade',
      link: sharesocialFn
    };
  }
]);/******************************************************************************************************************
 Social Login Directive
 - Authenticate Login with Facebook & Twitter
******************************************************************************************************************/
reddonor.directive('sociallogin', [
  '$location',
  function ($location) {
    var socialloginFn;
    socialloginFn = function (scope, element, attrs) {
      scope.onTwitterLogin = function () {
        // to overcome Angular intercepting function call
        window.location = '../auth/twitter';
      };
      scope.onFacebookLogin = function () {
        // to overcome Angular intercepting function call
        window.location = '../auth/facebook';
      };
    };
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/social-login.jade',
      link: socialloginFn
    };
  }
]);/******************************************************************************************************************
 Charts Directive
 - Display Charts
******************************************************************************************************************/
reddonor.directive('chart', function () {
  return {
    scope: {
      'title': '@',
      'data': '=',
      'type': '@',
      'number': '@'
    },
    restrict: 'EA',
    link: function (scope, element, attrs) {
      scope.$watch('data', function (a) {
        if (scope.data[0]) {
          if (a == [])
            console.log('Watch ' + scope.data == [], 'data');
          var id = 'chart' + Math.floor(Math.random() * 1000 + 1);
          element[0].id = id;
          var chart = new google.visualization.ChartWrapper();
          chart.setContainerId(id);
          chart.setOptions({
            'title': attrs.title || 'No Title',
            'width': attrs.width || 400,
            'height': attrs.height || 300,
            'backgroundColor': { fill: 'transparent' }
          });
          //chart.setChartType('BarChart');
          scope.$watch('type', function (a) {
            chart.setChartType(a || 'BarChart');
            chart.draw();
          });
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Causes');
          data.addColumn('number', attrs.value);
          //console.log(scope.number);
          data.addRows(scope.data);
          chart.setDataTable(data);
          chart.draw();
        }
      }, true);
    }
  };
});/******************************************************************************************************************
 Datepicker Directive
 - 
******************************************************************************************************************/
reddonor.directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModelCtrl) {
      $(function () {
        element.datepicker({
          dateFormat: 'mm/dd/yy',
          changeMonth: true,
          changeYear: true,
          onSelect: function (date) {
            ngModelCtrl.$setViewValue(date);
            scope.$apply();
          }
        });
      });
    }
  };
});/******************************************************************************************************************
 Validation Equals Directive
 - used for the same password check
******************************************************************************************************************/
reddonor.directive('uiValidateEquals', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {
      function validateEqual(myValue, otherValue) {
        if (myValue === otherValue) {
          ctrl.$setValidity('equal', true);
          return myValue;
        } else {
          ctrl.$setValidity('equal', false);
          return undefined;
        }
      }
      scope.$watch(attrs.uiValidateEquals, function (otherModelValue) {
        validateEqual(ctrl.$viewValue, otherModelValue);
      });
      ctrl.$parsers.unshift(function (viewValue) {
        return validateEqual(viewValue, scope.$eval(attrs.uiValidateEquals));
      });
      ctrl.$formatters.unshift(function (modelValue) {
        return validateEqual(modelValue, scope.$eval(attrs.uiValidateEquals));
      });
    }
  };
});;'use strict';

/* Filters */

reddonor.filter('interpolate', ['version', function(version) {
		return function(text) {
			return String(text).replace(/\%VERSION\%/mg, version);
		};
	}])
	.filter('fromNow', function() {
		return function(dateString) {
			return moment(new Date(dateString)).fromNow();
		};
	})
	.filter('startFrom', function() {
		return function(input, start) {
			if (!_.isArray(input) && !_.isString(input)) {
				return input;
			}
			start = +start || 0;
			return input.slice(start);
		};
	})
	.filter('isCentre', function() {
			return function(input, centre) {
					if (typeof centre == 'undefined' || centre == null) {
							return input;
					} else {
							var out = [];
							angular.forEach($scope.countries, function(val, key){
								if(val !== $scope.selectedCountry){
									this.push(val);
									}
								},filteredCountries);
							console.log(input);
							console.log("Centre " + centre);
							return out;
					}
			};
	});


;/******************************************************************************************************************
 Country List Data Factory
 - Gets list of countries data
******************************************************************************************************************/
reddonor.factory('countryListData', ['$http','$q', function($http, $q) {
		return {
			getCountries: function() {
			var deferred = $q.defer();
			$http.get('/api/countries/')
					.success(function(data) {
						deferred.resolve(data);
						console.log("getCountries");
						console.log(data);
					})
					.error(function() {
						deferred.reject();
					});
				return deferred.promise;
		}
	};
}]);