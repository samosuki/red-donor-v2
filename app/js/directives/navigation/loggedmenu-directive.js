/******************************************************************************************************************
 Navigation Menu Directive (Logged In)
 - Get username, donation centre locations, countries and logout
******************************************************************************************************************/

reddonor.directive('rednav', ['$http', '$location', function ($http, $location) {
    var navLogout;
    navLogout = function(scope, element, attrs) {
      scope.onlogOut = function() {
        $http.get('/session/logout').
          success(function() {
          //console.log("Logout clicked!");
          //window.location = "/login";
          window.location = "/";
				}).error(function() {
					//console.log("Did not work?");
				});
      };
      $http.get('/api/countries/').
        success(function(data) {
          scope.country = data;
          //console.log(data);
          return;
        });
      $http.get('/api/username/').
        success(function(data) {
          scope.username = data[0].user_name;
          return;
        });
      $http.get('/api/centre-locations/').
        success(function(data) {
          scope.userlocations = data;
          //console.log(data);
          return;
        });
      }
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/partials/loggedin-menu.jade',
      link: navLogout

    };
  }]);