###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore") 
moment = require("moment")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()

###******************************************************************************************************************
GET BLOOD TYPE INFO
Usage: gets all the blood type information
******************************************************************************************************************###
exports.bloodtype = (req, res) ->
  connection = database.getConnection()  
  # returns all the blood type info (check if used or reduce the content call)
  q = "SELECT * from bloodtype"
  connection.connect()
  connection.query q, (err, results) ->
    if err      
      console.log err
    else
      res.json results
      connection.end()

###******************************************************************************************************************
GET COMPATIBLE BLOOD BREADCRUMBS [COMPATIBLE DRIVES PAGE]
Usage: i. Gets Bloodtype Name for Compatible Drive Breadcrumb
      ii. Gets Bloodtypes Compatible with User
******************************************************************************************************************###
exports.headingcompatlist = (req, res) ->
  connection = database.getConnection()
  connection.connect()  
  async.parallel [
    (callback) ->
      
      # returns the blood type name from the user bloodtype id stored in the session variable
      userbloodtype = req.session.user.user_bloodtype
      q1 = "SELECT b.`bloodtype_name` FROM users u, bloodtype b WHERE u.user_bloodtype=b.bloodtype_id AND u.`user_bloodtype` = ? LIMIT 1;"
      connection.query q1, [userbloodtype], (err, results) ->
        if err
          console.log err
        else
          console.log "--- Blood Compatibility List Loaded ---"
          callback null, results

    (callback) ->
      
      # returns compatible blood type 
      drivebloodtype = req.session.user.user_bloodtype
      if drivebloodtype
        compat = "b.`o-pos-compat`"  if drivebloodtype is 1
        compat = "b.`o-neg-compat`"  if drivebloodtype is 2
        compat = "b.`a-pos-compat`"  if drivebloodtype is 3
        compat = "b.`a-neg-compat`"  if drivebloodtype is 4
        compat = "b.`b-pos-compat`"  if drivebloodtype is 5
        compat = "b.`b-neg-compat`"  if drivebloodtype is 6
        compat = "b.`ab-pos-compat`"  if drivebloodtype is 7
        compat = "b.`ab-neg-compat`"  if drivebloodtype is 8
        compat = "b.`ab-pos-compat`"  if drivebloodtype is 9
        q2 = "SELECT b.`bloodtype_name` FROM bloodtype b WHERE " + compat + "  = 1"
        connection.query q2, (err, results, fields) ->
          if err
            console.log err
          else
            console.log "--- Compatible Blood Types Loaded ---"            
            callback null, results

  ], (err, results) ->
    res.json results
    connection.end()



