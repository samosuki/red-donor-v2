/******************************************************************************************************************
 Drives By Location Controller (Paged)
 - Display drives by location, pagination and no blood drives (empty) set
******************************************************************************************************************/
	
reddonor.controller('DriveLocPagesCtrl', ['$scope', '$http', '$routeParams', '$location', '$window', 
	function($scope, $http, $routeParams, $location, $window) {
			$scope.drives = [];
	$scope.content = [];
	$scope.empty = true;
	$scope.emptydrive = false;
	$scope.currentPage = 0;
	$scope.pageSize = 8;

	// Get Count of Number of Drives for Pagination & Breadcrumb Data
	$scope.getItemCount = function () {
		$http.get('/api/drives-location-count-crumb/' + $routeParams.loc).
			success(function(datas) {
				$scope.totalItems = datas[0].Result_Count;
				$scope.breadcrumb = datas[0].country_name;
				$scope.pageSize = 8;
				$scope.numPages = Math.ceil($scope.totalItems / $scope.pageSize);
				for(var i = 0; i<$scope.numPages; i++) {
					$scope.content.push(i);
				}

				if($scope.currentPage >= 3) {
					$scope.showCurrent = false;
					$scope.hideCurrent = false;
				} else {
					$scope.showCurrent = true;
					$scope.hideCurrent = true;
				}
			});

	};
	// Get Previous Drive by Location Page
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$location.url('/drives/' + $routeParams.loc + '?page=' + $scope.currentPage);
			//$window.location = '../drives/' + $routeParams.loc + '?page=' + $scope.currentPage;
		}
	};
	// Get Next Drive by Location Page
	$scope.nextPage = function () {
		if ($scope.currentPage <  $scope.numPages-1) {
			$scope.nextPage = $scope.currentPage + 2;
			$location.url('../drives/' + $routeParams.loc + '?page=' + $scope.nextPage);
			//$window.location = '../drives/' + $routeParams.loc + '?page=' + $scope.nextPage;
		}
	};

	// Get blood drive information
	$scope.getDrives = function() {
		$scope.getItemCount();

		$http.get('/api/drives-location/' + $routeParams.loc).
				success(function(data) {
					$scope.drives = data;
					$scope.empty = false;
					$scope.limit = 8;
					$scope.blood = $routeParams.loc

					if(($routeParams.page != null) && ($routeParams.page > 0)) {
						$scope.currentPage = $routeParams.page - 1;
					} else {
						$scope.currentPage = 0;
					}

					if($scope.drives == "") {
						$scope.text = "No New Blood Drives Found";
						$scope.emptydrive = true;
						$scope.empty = true;
					}
					
				});
				$scope.prevPage();
				$scope.nextPage();
			};

	if($routeParams.page != null) {
			// Get Soon Ending Drives
			$scope.getDrives();
	} else {
			$routeParams.page = 1;
			$scope.getDrives();
		}
}]);