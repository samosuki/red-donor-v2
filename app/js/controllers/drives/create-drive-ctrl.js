/******************************************************************************************************************
 Create Drive Controller
 - Get country list, uses list, blood types list and errors
******************************************************************************************************************/
/* ---------- Create Drive Controller ---------- */
reddonor.controller('CreateDriveCtrl', ['$scope', '$http', '$location',
	function($scope, $http, $location) {
		
		$scope.go = function ( path ) {
  		$location.path( path );
		};

		$scope.create = function() {
			$scope.decision = false;
		};

		// Get Country List
		$http.get('/api/country').
				success(function(data) {
					$scope.country = data;
					//console.log(data);
					return;
		});

		// Get Uses List
		$http.get('/api/uses').
				success(function(data) {
					$scope.uses = data;
					//console.log(data);
					return;
		});

		// Get Locations Info (ALL)
		$http.get('/api/centres/').
			success(function(data) {
				$scope.location = data;
				$scope.filteredLocations = data;
				return;
		});

		$scope.decision = true;

		$scope.countryChange = function() {
			$scope.filteredLocations = [];
			//$scope.recipient = { driveCentre: $scope.filteredLocations[0] = "-- chose --"};
			angular.forEach($scope.location, function(val, key){
				if(val.location_country == $scope.recipient.driveCountry){
					//$scope.recipient = { driveCentre: $scope.filteredLocations[0] = "-- chose --"};
					this.push(val);
					//$scope.chosenCountry = val.country_name;
					/*console.log("Val");
					console.log(val);
					console.log("Val Location ID");
					console.log(val.location_id);
					console.log("Centre Filter");*/
				}
				else if($scope.recipient.driveCountry === undefined) {
					//console.log("centre undefined");
					this.push(val);
				}
			},$scope.filteredLocations);
		};


		// Blood Types for List
		$scope.bloodTypes = [
				{ id: 1, name:"O+" },
				{ id: 2, name:"O-" },
				{ id: 3, name:"A+" },
				{ id: 4, name:"A-" },
				{ id: 5, name:"B+" },
				{ id: 6, name:"B-" },
				{ id: 7, name:"AB+"},
				{ id: 8, name:"AB-"},
				{ id: 9, name:"Unknown"}
			];

			$scope.poperr = function() {
				toaster.pop('error', "", $scope.error, 5000, true);
			};

			$scope.popsuccess = function() {
				toaster.pop('success', "", $scope.success, 5000, true);
			};

			$scope.onSubmitAddDrive = function() {
				$http.post('/api/create-new-drive/', $scope.recipient).
					success(function(data) {
						if(data.aerror) {
							$scope.aerror = true;
							$scope.error = "Sorry, that Deadline Date has passed";
							$scope.poperr();
						}
						if(data.berror) {
							$scope.error = "An error occurred. Your Drive was not created :(";
							$scope.poperr();
						}
						if(data.success) {
							$scope.error = "";
							window.location = "/";
						}
					}); 
				};
	}]);