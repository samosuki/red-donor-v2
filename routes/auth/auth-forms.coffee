###***********************************************************
Required Modules
***********************************************************###

path = require('path')
mysql = require('mysql')
_ = require("underscore")
bcrypt = require("bcrypt")
nodemailer = require("nodemailer")
async = require("async") # Async Functions
database = require(path.join(__dirname, "../database"))
#connection = database.getConnection()
mandrill = require('mandrill-api/mandrill')
mandrill_client = new mandrill.Mandrill('46FSXMG25M4aHtpUVeoPew')

# Require Login
# Checks for User Session Data and redirections to login form if not found
exports.requireLogin = (req, res, next) ->
  if req.session.user
    res.locals.userid = req.session.user.users_id
    res.locals.user_name = req.session.user.user_name
    console.log "User Session Exists"
    next()
  else    
    #res.redirect('/partials/login?redir=' + req.url);
    res.redirect "partials/login"    
    console.log "Redirect - No Session Found"


# Authenticate Login
# Checks that the user name and password exist in the DB
exports.authenticateLogin = (loginEmail, loginPassword, callback) ->
  connection = database.getConnection()
  q = "SELECT * FROM users u WHERE u.`user_email` = ?;"
  params = [loginEmail]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      callback null
    else
      if bcrypt.compareSync(loginPassword, results[0].user_password)
        user = results[0]
        console.log "User Authenticated"
        callback user
        return
      else
        callback null
        #console.log user
        console.log "User not Authenticated."
  connection.end()

# Check for Login Email Signup
# Checks for existing email address
exports.existingLoginEmail = (loginEmail, callback) ->
  connection = database.getConnection()
  q = "SELECT * FROM users u WHERE u.`user_email` = ?"
  connection.query q, [loginEmail], (err, results) ->
    if err
      console.log err
      callback false
      return
    if results.length > 0
      taken = true
      console.log "Exists..."
      callback taken
      return
    callback false
  connection.end()

# Check for Email Signup
# Checks for existing email address
exports.existingEmail = (signupEmail, callback) ->
  connection = database.getConnection()
  q = "SELECT * FROM users u WHERE u.`user_email` = ?"
  connection.query q, [signupEmail], (err, results) ->
    if err
      console.log err
      callback false
      return
    if results.length > 0
      taken = true
      console.log "Exists..."
      callback taken
      return
    callback false
  connection.end()



###
AUTHENTICATE SIGNUP
Usage:  encrypts the password from the Sign Up form, saves User Info to the database, login user
###
exports.authenticateSignup = (signupName, signupEmail, signupPassword, signupbloodTypes, signupGender, signupDOB, signupLocation, callback) ->
  connection = database.getConnection()
  # encrypts password using bcrypt module
  salt = bcrypt.genSaltSync(12, 32)
  pass = bcrypt.hashSync(signupPassword, salt)
  dateJoined = new Date()
  dob = new Date(signupDOB)
  #var dateJoined = new time.Date();
  #dateJoined.setTimezone("America/New_York");
  q = "INSERT INTO users SET user_name = ?, user_email = ?, user_password = ?, user_bloodtype = ?, user_gender = ?, user_dob = ?, user_country = ?, date_joined = ?"
  params = [
    signupName
    signupEmail
    pass
    signupbloodTypes
    signupGender
    dob
    signupLocation
    dateJoined
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      #console.log results
      console.log "Error inserting user"
    else
      console.log "New User Signed Up!"
      #console.log results  
  # Login After Signing in User
  q2 = "SELECT * FROM users u WHERE u.`user_email` = ? AND u.`user_password` = ?"
  params2 = [
    signupEmail
    pass
  ]
  connection.query q2, params2, (err, results) ->
    if err
      console.log err
      callback null
      console.log "Error signing in user"
      return
    if results.length > 0
      user = results[0]
      console.log "User_ID authenticated: " + user.users_id
      callback user
      console.log "Logging in New User!"
      return
    callback null
  
  # Prepare Mandrill Email
  message =
    html: "<html>" + "<title>Welcome to Red Donor</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Welcome to the Red Donor Community!</h3>" + "<p style='font-size: 14px;'>Hi " + signupName + ",</p>" + "<p style='font-size: 14px;'>You are the newest member of the Red Donor Emergency Blood Donors. </p>" + "<p style='font-size: 14px;'>Thank you for your deep generosity for pledging to respond should the need arise for your blood group. </p>" + "<p style='font-size: 14px;'>You will be contacted should there be a sudden need for your blood type. </p>" + "<p style='font-size: 14px;'>Thank you for signing up. You can help save a life!</p>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Team</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
    subject: "Welcome to Red Donor"
    from_email: "support@reddonor.com"
    from_name: "Red Donor"
    to: [
      email: signupEmail
      name: signupName
      type: "to"
    ]
    headers:
      "Reply-To": "support@reddonor.com"

    important: false
    inline_css: true
  mandrill_client.messages.send
    message: message
  , ((result) ->
    console.log "Message sent"
    #console.log result
  ), (e) ->
    console.log "A mandrill error occurred: " + e.name + " - " + e.message
  
  #console.log signupEmail  
  connection.end()

###***********************************************************
GUEST USER SIGNUP
Usage:  Saves User Info to the database
***********************************************************###
exports.guestSignup = (userEmail, callback) ->
  connection = database.getConnection()
  dateJoined = new Date()
  userName = "Guest"
  q = "INSERT INTO users SET user_name = ?, user_email = ?, date_joined = ?"
  q1 = "SELECT users_id FROM users u WHERE u.`user_email` = ?;"
  params = [
    userName
    userEmail
    dateJoined
  ]
  params1 = [
    userEmail
  ]
  connection.connect()
  connection.query q, params, (err, results) ->
    if err
      console.log err
      console.log "Error inserting Guest!"
    else
      console.log "Guest Registered!"
  connection.query q1, params1, (err, results) ->
    if err
      console.log err
      callback false
    else
      console.log "Get UserID!"
      userid = results[0].users_id
      callback userid
  connection.end()

###
AUTHENTICATE GUEST SIGNUP
Usage:  encrypts the password from the Sign Up form, saves User Info to the database, login user
###
exports.authenticateGuestSignup = (signupName, signupEmail, signupPassword, signupbloodTypes, signupGender, signupDOB, signupLocation, callback) ->
  connection = database.getConnection()
  # encrypts password using bcrypt module
  salt = bcrypt.genSaltSync(12, 32)
  pass = bcrypt.hashSync(signupPassword, salt)
  dateJoined = new Date()
  dob = new Date(signupDOB)
  #var dateJoined = new time.Date();
  #dateJoined.setTimezone("America/New_York");
  q = "UPDATE users SET user_name = ?, user_email = ?, user_password = ?, user_bloodtype = ?, user_gender = ?, user_country = ?, date_joined = ? WHERE user_email = ?"
  params = [
    signupName
    signupEmail
    pass
    signupbloodTypes
    signupGender
    signupLocation
    dateJoined
    signupEmail
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      #console.log results
      console.log "Error inserting user"
    else
      console.log "New User Signed Up!"
      #console.log results  
  # Login After Signing in User
  q2 = "SELECT * FROM users u WHERE u.`user_email` = ? AND u.`user_password` = ?"
  params2 = [
    signupEmail
    pass
  ]
  connection.query q2, params2, (err, results) ->
    if err
      console.log err
      callback null
      console.log "Error signing in user"
      return
    if results.length > 0
      user = results[0]
      console.log "User_ID authenticated: " + user.users_id
      callback user
      console.log "Logging in New User!"
      return
    callback null
  
  # Prepare Mandrill Email
  message =
    html: "<html>" + "<title>Welcome to Red Donor</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/assets/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Welcome to the Red Donor Community!</h3>" + "<p style='font-size: 14px;'>Hi " + signupName + ",</p>" + "<p style='font-size: 14px;'>You are the newest member of the Red Donor Emergency Blood Donors. </p>" + "<p style='font-size: 14px;'>Thank you for your deep generosity for pledging to respond should the need arise for your blood group. </p>" + "<p style='font-size: 14px;'>You will be contacted should there be a sudden need for your blood type. </p>" + "<p style='font-size: 14px;'>Thank you for signing up. You can help save a life!</p>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Team</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/assets/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"
    subject: "Welcome to Red Donor"
    from_email: "support@reddonor.com"
    from_name: "Red Donor"
    to: [
      email: signupEmail
      name: signupName
      type: "to"
    ]
    headers:
      "Reply-To": "support@reddonor.com"

    important: false
    inline_css: true
  mandrill_client.messages.send
    message: message
  , ((result) ->
    console.log "Message sent"
    #console.log result
  ), (e) ->
    console.log "A mandrill error occurred: " + e.name + " - " + e.message
  
  #console.log signupEmail  
  connection.end()

###
AUTHENTICATE DONOR SIGNUP
Usage:  Saves donor info to the database and sends
###
exports.authenticateDonorSignup = (signupName, signupEmail, signupTel, signupPassword, signupbloodTypes, signupGender, signupLocation, callback) ->
  
  # encrypts password using bcrypt module
  salt = bcrypt.genSaltSync(12, 32)
  pass = bcrypt.hashSync(signupPassword, salt)
  dateJoined = new Date()
  donorType = 2
  
  #var dateJoined = new time.Date();
  #dateJoined.setTimezone("America/New_York");
  q = "UPDATE users SET user_name = ?, user_email = ?, user_tel = ?, user_password = ?, user_bloodtype = ?, user_gender = ?, user_country = ?, user_donor_type = ?, date_joined = ? WHERE user_email = ?"
  params = [
    signupName
    signupEmail
    signupTel
    pass
    signupbloodTypes
    signupGender
    signupLocation
    donorType
    dateJoined
    signupEmail
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
      #console.log results
      console.log "Error inserting new Emergency Donor"
    else
      console.log "New Donor Registered!"
      #console.log results
  
  # Check that Donor Exists
  q2 = "SELECT * FROM users u WHERE u.`user_email` = ? AND u.`user_tel` = ?"
  params2 = [
    signupEmail
    signupTel
  ]
  connection.query q2, params2, (err, results) ->
    if err
      console.log err
      callback null
      console.log "Error signing in user"
      return
    if results.length > 0
      user = results[0]
      console.log "User Authenticated"
      callback user
      return
    callback null
  
  # Send Email To New Donor
  
  # Prepare Nodemailer Transport
  transport = nodemailer.createTransport("SMTP",
    
    #service: 'Gmail',
    host: "smtp.webfaction.com"
    port: 587
    auth:
      user: "red_donor"
      pass: "m1sty_MTN" # m1sty_MTN / mOg33S4m
  )
  
  # Setup Mail Options
  mailOptions =
    forceEmbeddedImages: true
    from: "Red Donor <support@reddonor.com>" # sender address
    to: signupEmail #"samosuki@gmail.com", //,  //signupEmail, // list of receivers
    replyTo: "support@reddonor.com"
    subject: "Welcome to Red Donor Emergency Blood Donors" # Subject line
    generateTextFromHTML: true # generates text from HTML
    html: "<html>" + "<title>Welcome to Red Donor</title>" + "<body style='margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; padding: 0px 0px 0px 0px; font-family: Arial, sans-serif;'>" + "<table width='560px' height='350px' align='center' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; bgcolor: #ECECEC; border: 1px solid #CCCCCC;'>" + "<tr align='center'>" + "<td>" + "<table width='560px' height='110px' bgcolor='#ECECEC' cellpadding='0' cellspacing='0' style='margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; border-bottom: 1px solid #CCCCCC'>" + "<tr><td><span>" + "<img src='http://reddonor.com/images/red-donor-email-header-clean.jpg' alt='Red Donor'></img>" + "</span></td></tr></table>" + "<table width='560px' height='240px' bgcolor='#FFFFFF' cellpadding='0' cellspacing='0'>" + "<tr><td valign='top' style='padding: 20px 16px 20px 16px;'>" + "<h3 style='font-size: 18px; font-weight: bold; color: #141414;'>Welcome to the Red Donor Community!</h3>" + "<p style='font-size: 14px;'>Hi " + signupName + ",</p>" + "<p style='font-size: 14px;'>You are the newest member of the Red Donor Emergency Blood Donors. </p>" + "<p style='font-size: 14px;'>Thank you for your deep generosity for pledging to respond should the need arise for your blood group. </p>" + "<p style='font-size: 14px;'>You will be contacted should there be a sudden need for your blood type. </p>" + "<p style='font-size: 14px;'>Thank you for signing up. You can help save a life!</p>" + "<p style='color:#979797; font-size: 13px;'>Red Donor Team</p>" + "<hr style='border: none; background-color:#E2E2E2; color:#E2E2E2; height:1px; margin: 20px 0px 0px 0px'>" + "</td></tr></table>" + "<table width='560px' height='40px' align='center' cellpadding='0' cellspacing='0' style='padding: 6px 16px 6px 16px;'><tr><td>" + "<span style='font-size: 13px; color:#6b6b6b;'>&copy; 2013 Red Donor</span></td>" + "<td style='text-align: right; padding-top: 3px'>" + "<span style='padding-right: 4px; padding-left: 2px;'><a href='https://www.facebook.com/RedDonor' target='_blank'><img src='http://reddonor.com/images/facebook.jpg' alt='Red Donor Facebook'></img></a></span>" + "<span style='padding-right: 8px; padding-left: 2px;'><a href='https://twitter.com/RedDonor' target='_blank'><img src='http://reddonor.com/images/twitter.jpg' alt='Red Donor Twitter'></img></a></span>" + "</td></tr></table>" + "</td></tr></table>" + "</html>"

  console.log signupEmail
  transport.sendMail mailOptions, (error, response) ->
    if error
      console.log error
    else
      console.log "Message sent"


# Email Exists
###
Usage:  Checks to see if an email address already exists in the database
###
exports.emailExists = (forgotEmail, callback) ->
  connection = database.getConnection()
  # if user email matches one in the database return user id 
  q = "SELECT u.`user_name` FROM users u WHERE u.`user_email` = ?"
  connection.query q, [forgotEmail], (err, results) ->
    if err
      console.log err
      callback false
      return
    if results.length > 0
      exists = results[0]
      console.log "Email Address Exists"
      callback exists
      return
    callback false
  connection.end()


exports.createtoken = (forgotEmail, callback) ->
  connection = database.getConnection()
  salt = bcrypt.genSaltSync(12, 32)
  unique_str = Math.random().toString(36).substr(2, 36)
  token = bcrypt.hashSync(unique_str, salt).substr(8, 28)
  tokenRequested = new Date()
  
  #var tokenRequested = new time.Date();
  #tokenRequested.setTimezone("America/New_York");
  q = "UPDATE users SET recover_token = ?, recover_datetime = ? WHERE user_email = ?"
  params = [
    token
    tokenRequested
    forgotEmail
  ]
  connection.query q, params, (err, results) ->
    if err
      console.log err
    else
      console.log "Password Reset Tokens Added to DB"
      callback token
  connection.end()


exports.checkToken = (recoverToken, callback) ->
  connection = database.getConnection()
  # Check Token
  q = "SELECT u.`recover_datetime` FROM users u WHERE u.`recover_token` = ?"
  connection.query q, [recoverToken], (err, results) ->
    if err
      console.log err
      callback false
      return
    if results.length > 0
      tokenTime = results[0]
      callback tokenTime
      return
    callback false
  connection.end()


exports.checkTokenTime = (tokenTime, callback) ->
  connection = database.getConnection()
  # Check Token Time
  tokenTime = tokenTime.recover_datetime
  q = "SELECT `users_id` FROM `users` WHERE TIMESTAMPDIFF(HOUR, ?, NOW()) < 2"
  connection.query q, [tokenTime], (err, results) ->
    if err
      console.log err
      callback false
      return
    if results.length > 0
      userID = results[0]
      currentDate = new Date()      
      #var currentDate = new time.Date();
      #currentDate.setTimezone("America/New_York");
      console.log "Token Time Less than 2 hrs..."
      console.log "Token Time: " + tokenTime
      console.log "Now: " + currentDate
      callback userID
      return
    callback false
  connection.end()
