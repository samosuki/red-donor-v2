/******************************************************************************************************************
 Country List Data Factory
 - Gets list of countries data
******************************************************************************************************************/
reddonor.factory('countryListData', ['$http','$q', function($http, $q) {
		return {
			getCountries: function() {
			var deferred = $q.defer();
			$http.get('/api/countries/')
					.success(function(data) {
						deferred.resolve(data);
						console.log("getCountries");
						console.log(data);
					})
					.error(function() {
						deferred.reject();
					});
				return deferred.promise;
		}
	};
}]);