/******************************************************************************************************************
 Profiles Controller
 - Get user profile info, donation pledges and created drives
******************************************************************************************************************/
reddonor.controller('ProfileCtrl', ['$scope', '$http',
	function($scope, $http) {
		$http.get('/api/profile-info/').
      success(function(data) {
        $scope.profiles = data;
      });
  $http.get('/api/donation-pledges/').
      success(function(data) {
        $scope.pledges = data;
      });
  $http.get('/api/created-drives/').
      success(function(data) {
        $scope.drives = data;
      });
	}]);